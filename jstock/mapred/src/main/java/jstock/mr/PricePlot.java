package jstock.mr;

import java.io.IOException;

import knapp.common.util.ByteUtils;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.Tool;

public class PricePlot extends Configured implements Tool {
	
	
	public static void main(String[] args) {
		
	}

	class MyMapper extends Mapper<Key, Value, IntRangeWritable, IntWritable> {
		private IntRangeWritable range = new IntRangeWritable();
		private IntWritable price = new IntWritable();

		public void map(Key k, Value v, Context c) throws IOException,
				InterruptedException {
			int pricePennies = ByteUtils.bytesToInt(v.get());
			price.set((int) Math.round((double) pricePennies / (double) 100));
			// figure out what bucket to emit
			for (int i = 0; i < 195; i += 5) {
				range.setStart(i);
				range.setStop(i + 5);
				if (range.contains(price.get())) {
					c.write(range, price);
				}
			}
		}
	}

	class MyReducer extends
			Reducer<IntRangeWritable, IntWritable, IntWritable, Text> {
		private IntWritable start = new IntWritable();
		private Text val = new Text();

		@Override
		public void reduce(IntRangeWritable range,Iterable<IntWritable> values,Context c) throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable v : values) {
				sum+=v.get();
			}
			start.set(range.getStart());
			val.set(String.format("%d\t%d",range.getStop(),sum));
			c.write(start,val);
		}
	}

	@Override
	public int run(String[] arg0) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}
}
