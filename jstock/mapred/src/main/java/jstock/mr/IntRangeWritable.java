package jstock.mr;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

public class IntRangeWritable implements WritableComparable<IntRangeWritable> {

	private int start;
	private int stop;
	
	public IntRangeWritable() {
	
	}
	
	public IntRangeWritable(int start,int stop) {
		this.start = start;
		this.stop = stop;
	}
	
	public IntRangeWritable(IntRangeWritable other) {
		this.start = other.start;
		this.stop = other.stop;
	}
	
	@Override
	public void readFields(DataInput arg0) throws IOException {
		this.start = arg0.readInt();
		this.stop = arg0.readInt();
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		arg0.writeInt(start);
		arg0.writeInt(stop);
	}

	@Override
	public int compareTo(IntRangeWritable o) {
		return this.start-o.start;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getStop() {
		return stop;
	}

	public void setStop(int stop) {
		this.stop = stop;
	}

	public boolean contains(int priceDollars) {
		return start<=priceDollars && priceDollars<stop;
	}

}
