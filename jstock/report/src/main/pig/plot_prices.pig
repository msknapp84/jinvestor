/* The goal of this is to plot:
 on the x-axis: price buckets in five dollar increments
 on the y-axis: the logarithm of the number of companies in that price range. 
 */

-- I'm grabbing the adjusted closing price, and the date.  The row id has the ticker.
PH = LOAD 'accumulo://jstock.priceHistory?instance=michael&user=jstock&password=jstock&zookeepers=localhost'
      USING org.apache.pig.backend.hadoop.accumulo.AccumuloStorage(
      'f:a,f:d', '-a public') AS
      (ticker_time:chararray, adj_close_pennies:int, date:date);


/* unfortunately the row id contains the ticker and we would have to split it 
 to get that.
 */
 
PH_WITH_TICKER = FOREACH PH GENERATE TOKENIZE(ticker_time,'-') AS (ticker:chararray,inverse_time:chararray) adj_close_pennies date; 

/* I have a massive dataset, it's best to filter things down first.
I want to just use the last known price, so let's group by the ticker
*/
