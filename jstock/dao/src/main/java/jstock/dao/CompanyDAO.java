package jstock.dao;

import jstock.domain.Company;
import knapp.common.data.dao.DAO;
import knapp.common.data.exceptions.DataException;

public interface CompanyDAO extends DAO<Company> {
	Company load(String ticker) throws DataException;
	void save(Company company) throws DataException;
	void delete(String ticker) throws DataException;
}