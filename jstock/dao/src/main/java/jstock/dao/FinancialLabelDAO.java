package jstock.dao;

import java.util.Map;

import jstock.domain.fh.FinancialLabel;
import knapp.common.data.dao.DAO;
import knapp.common.data.exceptions.DataException;

public interface FinancialLabelDAO extends DAO<FinancialLabel> {
	Map<Integer,String> loadLabels() throws DataException;
	Integer getOrdinalForName(String name) throws DataException;
	String getNameForOrdinal(Integer ordinal) throws DataException;
}
