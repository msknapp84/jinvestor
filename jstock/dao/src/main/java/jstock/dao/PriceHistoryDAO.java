package jstock.dao;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;

import jstock.domain.ph.PHTablet;
import knapp.common.data.dao.DAO;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;
import knapp.common.util.TimeSpan;

public interface PriceHistoryDAO extends DAO<PHTablet> {
	PHTablet load(ByteArray ticker,LocalDate from,LocalDate to) throws DataException;
	PHTablet loadTablet(ByteArray ticker) throws DataException;
	Map<ByteArray,PHTablet> loadTablets(Collection<ByteArray> ticker) throws DataException;
	// probably indexed by company and then date.
	Map<ByteArray,PHTablet> loadRecords(Collection<ByteArray> ticker,LocalDate start,LocalDate end) throws DataException;
	Collection<TimeSpan> getMissingTimespans(ByteArray tickerSymbol,
			TimeSpan within) throws DataException;
}