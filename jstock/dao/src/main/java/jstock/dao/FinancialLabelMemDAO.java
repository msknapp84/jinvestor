package jstock.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import jstock.domain.fh.FinancialLabel;
import knapp.common.data.dao.DAOSkeleton;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;

public class FinancialLabelMemDAO extends DAOSkeleton<FinancialLabel> implements FinancialLabelDAO {
	
	private final FinancialLabelDAO core;
	private final Map<Integer,FinancialLabel> ordinalIndex = new HashMap<>();
	private final Map<String,FinancialLabel> nameIndex = new HashMap<>();
	
	public FinancialLabelMemDAO() {
		this.core = null;
	}

	public FinancialLabelMemDAO(final FinancialLabelDAO core) {
		this.core = core;
	}

	@Override
	public Map<ByteArray, FinancialLabel> get(Collection<ByteArray> ids)
			throws DataException {
		return getByIds(ids);
	}

	@Override
	public Map<ByteArray, FinancialLabel> getByIds(Collection<ByteArray> ids)
			throws DataException {
		Map<ByteArray,FinancialLabel> results = new HashMap<>();
		Set<ByteArray> missing = new HashSet<>();
		for (ByteArray id : ids) {
			Integer ordinal = Integer.parseInt(id.toString());
			FinancialLabel label = ordinalIndex.get(ordinal);
			if (label == null) {
				missing.add(id);
			} else {
				results.put(id, label);
			}
		}
		if (core!=null) {
			Map<ByteArray,FinancialLabel> coreResults = core.getByIds(missing);
			results.putAll(coreResults);
		}
		return results;
	}

	@Override
	public Map<Integer, String> loadLabels() throws DataException {
		Map<Integer,String> labels = new HashMap<>();
		for (Integer ordinal : ordinalIndex.keySet()) {
			labels.put(ordinal,ordinalIndex.get(ordinal).getName());
		}
		return labels;
	}

	@Override
	public int countAll() throws DataException {
		if (core!=null) {
			return core.countAll();
		}
		return ordinalIndex.size();
	}

	@Override
	public Integer getOrdinalForName(String name) throws DataException {
		FinancialLabel label = this.nameIndex.get(name);
		if (label == null && core!=null) {
			return core.getOrdinalForName(name);
		}
		return label == null ? -1 : label.getOrdinal();
	}

	@Override
	public String getNameForOrdinal(Integer ordinal) throws DataException {
		FinancialLabel label = this.ordinalIndex.get(ordinal);
		if (label == null && core!=null) {
			return core.getNameForOrdinal(ordinal);
		}
		return label == null ? null : label.getName();
	}

	@Override
	public void save(Collection<FinancialLabel> entity) throws DataException {
		if (core!=null) {
			core.save(entity);
		}
		for (FinancialLabel label : entity) {
			this.ordinalIndex.put(label.getOrdinal(),label);
			this.nameIndex.put(label.getName(),label);
		}
	}

	@Override
	public void delete(Collection<ByteArray> ids) throws DataException {
		if (core!=null){ 
			core.delete(ids);
		}
		for (ByteArray id : ids) {
			Integer ordinal = Integer.parseInt(id.toString());
			FinancialLabel label = this.ordinalIndex.get(ordinal);
			if (label !=null) {
				this.ordinalIndex.remove(ordinal);
				this.nameIndex.remove(label.getName());
			}
		}
	}
}
