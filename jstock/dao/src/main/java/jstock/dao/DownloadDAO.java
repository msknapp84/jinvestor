package jstock.dao;

/*
 * #%L
 * jinvestor.parent
 * %%
 * Copyright (C) 2014 Michael Scott Knapp
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import jstock.domain.Download;
import jstock.domain.DownloadRecordQuery;
import knapp.common.data.dao.DAO;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;

public interface DownloadDAO extends DAO<Download> {
	void create(Download record) throws DataException;
	void create(List<Download> records) throws DataException;
	Download getLatest(String uri) throws DataException;
	Download getLatestSuccess(String uri) throws DataException;
	Download getLatestByForeignId(String host,String path, String foreignTable,ByteArray foreignId) throws DataException;
	Download getLatestSuccessByForeignId(String host,String path, String foreignTable,ByteArray foreignId) throws DataException;
	List<Download> get(DownloadRecordQuery query) throws DataException;
	void update(Download record) throws DataException;
	void delete(Download record) throws DataException;
	void update(List<Download> records) throws DataException;
	void delete(List<Download> records) throws DataException;
	int count() throws DataException;
	int count(DownloadRecordQuery query) throws DataException;
}