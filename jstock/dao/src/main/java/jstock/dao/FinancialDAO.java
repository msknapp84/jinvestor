package jstock.dao;

import jstock.domain.fh.Financial;
import knapp.common.data.dao.DAO;

public interface FinancialDAO extends DAO<Financial> {

}
