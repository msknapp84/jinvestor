package jstock.dao;

import java.time.LocalDate;
import java.util.Collection;

import jstock.domain.er.EconomicRecord;
import knapp.common.data.dao.DAO;
import knapp.common.data.exceptions.DataException;

public interface EconomicRecordDAO extends DAO<EconomicRecord> {

	void save(EconomicRecord ER) throws DataException;

	EconomicRecord load(String ticker) throws DataException;

	void delete(String ticker) throws DataException;

	// probably indexed by series id and then date.
	EconomicRecord loadRecords(String seriesId,
			LocalDate start, LocalDate end) throws DataException;

	void saveRecords(Collection<EconomicRecord> records) throws DataException;
}