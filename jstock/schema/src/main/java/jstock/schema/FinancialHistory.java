//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.01.18 at 04:37:05 PM EST 
//


package jstock.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for financialHistory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="financialHistory">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="quarterlyBalance" type="{http://www.example.org/stock}financial" minOccurs="0"/>
 *         &lt;element name="quarterlyIncome" type="{http://www.example.org/stock}financial" minOccurs="0"/>
 *         &lt;element name="quarterlyCashFlow" type="{http://www.example.org/stock}financial" minOccurs="0"/>
 *         &lt;element name="annualBalance" type="{http://www.example.org/stock}financial" minOccurs="0"/>
 *         &lt;element name="annualIncome" type="{http://www.example.org/stock}financial" minOccurs="0"/>
 *         &lt;element name="annualCashFlow" type="{http://www.example.org/stock}financial" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "financialHistory", propOrder = {

})
public class FinancialHistory {

    protected Financial quarterlyBalance;
    protected Financial quarterlyIncome;
    protected Financial quarterlyCashFlow;
    protected Financial annualBalance;
    protected Financial annualIncome;
    protected Financial annualCashFlow;

    /**
     * Gets the value of the quarterlyBalance property.
     * 
     * @return
     *     possible object is
     *     {@link Financial }
     *     
     */
    public Financial getQuarterlyBalance() {
        return quarterlyBalance;
    }

    /**
     * Sets the value of the quarterlyBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Financial }
     *     
     */
    public void setQuarterlyBalance(Financial value) {
        this.quarterlyBalance = value;
    }

    /**
     * Gets the value of the quarterlyIncome property.
     * 
     * @return
     *     possible object is
     *     {@link Financial }
     *     
     */
    public Financial getQuarterlyIncome() {
        return quarterlyIncome;
    }

    /**
     * Sets the value of the quarterlyIncome property.
     * 
     * @param value
     *     allowed object is
     *     {@link Financial }
     *     
     */
    public void setQuarterlyIncome(Financial value) {
        this.quarterlyIncome = value;
    }

    /**
     * Gets the value of the quarterlyCashFlow property.
     * 
     * @return
     *     possible object is
     *     {@link Financial }
     *     
     */
    public Financial getQuarterlyCashFlow() {
        return quarterlyCashFlow;
    }

    /**
     * Sets the value of the quarterlyCashFlow property.
     * 
     * @param value
     *     allowed object is
     *     {@link Financial }
     *     
     */
    public void setQuarterlyCashFlow(Financial value) {
        this.quarterlyCashFlow = value;
    }

    /**
     * Gets the value of the annualBalance property.
     * 
     * @return
     *     possible object is
     *     {@link Financial }
     *     
     */
    public Financial getAnnualBalance() {
        return annualBalance;
    }

    /**
     * Sets the value of the annualBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Financial }
     *     
     */
    public void setAnnualBalance(Financial value) {
        this.annualBalance = value;
    }

    /**
     * Gets the value of the annualIncome property.
     * 
     * @return
     *     possible object is
     *     {@link Financial }
     *     
     */
    public Financial getAnnualIncome() {
        return annualIncome;
    }

    /**
     * Sets the value of the annualIncome property.
     * 
     * @param value
     *     allowed object is
     *     {@link Financial }
     *     
     */
    public void setAnnualIncome(Financial value) {
        this.annualIncome = value;
    }

    /**
     * Gets the value of the annualCashFlow property.
     * 
     * @return
     *     possible object is
     *     {@link Financial }
     *     
     */
    public Financial getAnnualCashFlow() {
        return annualCashFlow;
    }

    /**
     * Sets the value of the annualCashFlow property.
     * 
     * @param value
     *     allowed object is
     *     {@link Financial }
     *     
     */
    public void setAnnualCashFlow(Financial value) {
        this.annualCashFlow = value;
    }

}
