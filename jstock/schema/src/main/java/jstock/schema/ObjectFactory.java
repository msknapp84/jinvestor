//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.01.18 at 04:37:05 PM EST 
//


package jstock.schema;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the jstock.schema package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: jstock.schema
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DateValue }
     * 
     */
    public DateValue createDateValue() {
        return new DateValue();
    }

    /**
     * Create an instance of {@link PriceHistoryCell }
     * 
     */
    public PriceHistoryCell createPriceHistoryCell() {
        return new PriceHistoryCell();
    }

    /**
     * Create an instance of {@link FinancialHistory }
     * 
     */
    public FinancialHistory createFinancialHistory() {
        return new FinancialHistory();
    }

    /**
     * Create an instance of {@link Economics }
     * 
     */
    public Economics createEconomics() {
        return new Economics();
    }

    /**
     * Create an instance of {@link DateValues }
     * 
     */
    public DateValues createDateValues() {
        return new DateValues();
    }

    /**
     * Create an instance of {@link Financial }
     * 
     */
    public Financial createFinancial() {
        return new Financial();
    }

    /**
     * Create an instance of {@link FinancialCell }
     * 
     */
    public FinancialCell createFinancialCell() {
        return new FinancialCell();
    }

    /**
     * Create an instance of {@link Company }
     * 
     */
    public Company createCompany() {
        return new Company();
    }

    /**
     * Create an instance of {@link PriceHistory }
     * 
     */
    public PriceHistory createPriceHistory() {
        return new PriceHistory();
    }

}
