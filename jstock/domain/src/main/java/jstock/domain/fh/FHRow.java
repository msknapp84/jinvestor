package jstock.domain.fh;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FHRow {
	private final String name;
	private final Map<Date,Double> values;
	
	public static class FHRowBuilder {
		private String name;
		private Map<Date,Double> values = new HashMap<>();
		
		public FHRowBuilder name(String name) {
			this.name = name;
			return this;
		}

		public FHRowBuilder add(Date date,Double value) {
			this.values.put(date, value);
			return this;
		}	
		
		public FHRow build() {
			return new FHRow(this);
		}
	}
	
	public FHRow(FHRowBuilder fhRowBuilder) {
		this.name = fhRowBuilder.name;
		this.values = Collections.unmodifiableMap(new HashMap<>(fhRowBuilder.values));
	}
	
	public String getName() {
		return name;
	}
	
	public Map<Date, Double> getValues() {
		return values;
	}
}