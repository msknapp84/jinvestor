package jstock.domain.fh;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class FHCells {
	private final Map<String,FHRow> cells;
	
	private FHCells(FHCellsBuilder b) {
		this.cells = Collections.unmodifiableMap(b.cells);
	}
	
	public Map<String,FHRow> getCells() {
		return cells;
	}

	public static class FHCellsBuilder {
		private final Map<String,FHRow> cells = new HashMap<>();
		
		public FHCellsBuilder add(FHRow row) {
			this.cells.put(row.getName(), row);
			return this;
		}
		
		public FHCells build() {
			return new FHCells(this);
		}
	}
	
}
