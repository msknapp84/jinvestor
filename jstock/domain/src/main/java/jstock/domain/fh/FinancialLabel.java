package jstock.domain.fh;

import knapp.common.data.domain.Entity;

public class FinancialLabel extends Entity {

	private int ordinal;
	private String name;

	public FinancialLabel() {

	}

	public FinancialLabel(String name, int ordinal) {
		this.name = name;
		this.ordinal = ordinal;
	}

	public int getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(int ordinal) {
		this.ordinal = ordinal;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
