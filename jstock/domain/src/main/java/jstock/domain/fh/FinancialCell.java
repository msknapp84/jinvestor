package jstock.domain.fh;

public class FinancialCell {
	private String ticker;
	private StatementType type;
	private boolean annual;
	private byte[] date;
	private int label;
	private double value;
	public String getTicker() {
		return ticker;
	}
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
	public StatementType getType() {
		return type;
	}
	public void setType(StatementType type) {
		this.type = type;
	}
	public boolean isAnnual() {
		return annual;
	}
	public void setAnnual(boolean annual) {
		this.annual = annual;
	}
	public byte[] getDate() {
		return date;
	}
	public void setDate(byte[] date) {
		this.date = date;
	}
	public int getLabel() {
		return label;
	}
	public void setLabel(int label) {
		this.label = label;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	
	

}
