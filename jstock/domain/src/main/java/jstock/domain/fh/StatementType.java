package jstock.domain.fh;

public enum StatementType {
	INCOME,
	CASHFLOW,
	BALANCE;
}