package jstock.domain.fh;

import java.time.LocalDate;
import java.util.function.Function;

import knapp.common.collections.time.DoubleTable;
import knapp.common.collections.time.DoubleTableImmutable;

public class FinancialTable extends DoubleTableImmutable {
	private final String ticker;
	private final StatementType type;
	private final boolean annual;
	
	// this is just for convenience, consider it transient.
	private Function<Integer,String> labelNameResolver;
	private Function<String,Integer> labelOrdinalResolver;
	
	public static String determineId(String ticker,StatementType type,boolean annual) {
		return String.format("%s-%s%s",ticker,type.name().toLowerCase().charAt(0),annual?"a":"q");
	}
	
	public static String determineTicker(String rowId) {
		return rowId.split("-")[0];
	}
	
	public static StatementType determineType(String rowId) {
		char c = rowId.charAt(rowId.length()-2);
		if ('i'==c) {
			return StatementType.INCOME;
		} else if ('c'==c) {
			return StatementType.CASHFLOW;
		}
		return StatementType.BALANCE;
	}
	
	public static boolean determineAnnual(String rowId) {
		return rowId.charAt(rowId.length()-1) == 'a';
	}
	
	public FinancialTable(DoubleTable table,String ticker, StatementType type,boolean annual) {
		super(table);
		this.annual = annual;
		this.ticker = ticker;
		this.type = type;
	}
	
	public String calculateId() {
		return determineId(ticker, type, annual);
	}
	
	public String getTicker() {
		return ticker;
	}

	public StatementType getType() {
		return type;
	}

	public boolean isAnnual() {
		return annual;
	}

	public String toPrintString() {
		return toPrintString(labelNameResolver);
	}

	public String toPrintString(Function<Integer,String> labelNameResolver) {
		StringBuilder sb = new StringBuilder();
		sb.append("Ticker: ").append(ticker).append('\n');
		sb.append("Type: ").append(type.name()).append('\n');
		sb.append("Annual: ").append(Boolean.toString(annual)).append('\n').append('\n');
		super.toPrintString(sb,labelNameResolver);
		return sb.toString();
	}

	public Function<Integer, String> getLabelNameResolver() {
		return labelNameResolver;
	}
	
	public void setLabelNameResolver(Function<Integer, String> labelNameResolver) {
		this.labelNameResolver = labelNameResolver;
	}
	
	public double get(String labelName,LocalDate date,boolean latest) {
		return super.get(date, this.labelOrdinalResolver.apply(labelName), latest);
	}
	
	public double get(String labelName,String date,boolean latest) {
		if (labelName==null) {
			return super.getUnsetValue();
		}
		return super.get(LocalDate.parse(date), this.labelOrdinalResolver.apply(labelName), latest);
	}

	public Function<String,Integer> getLabelOrdinalResolver() {
		return labelOrdinalResolver;
	}

	public void setLabelOrdinalResolver(Function<String,Integer> labelOrdinalResolver) {
		this.labelOrdinalResolver = labelOrdinalResolver;
	}
}