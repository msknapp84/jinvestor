package jstock.domain.fh;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import knapp.common.collections.time.DoubleTable;
import knapp.common.collections.time.DoubleTableCell;
import knapp.common.data.domain.Entity;

public class Financial extends Entity implements Iterable<FinancialCell> {
	private FinancialTable quarterlyBalance;
	private FinancialTable quarterlyIncome;
	private FinancialTable quarterlyCashFlow;
	private FinancialTable annualBalance;
	private FinancialTable annualIncome;
	private FinancialTable annualCashFlow;
	private String ticker;

	public Financial(FinancialBuilder b) {
		this.ticker = b.ticker;
		if (b.quarterlyBalance != null) {
			this.quarterlyBalance = new FinancialTable(b.quarterlyBalance,
					b.ticker, StatementType.BALANCE, false);
		}
		if (b.quarterlyCashFlow != null) {
			this.quarterlyCashFlow = new FinancialTable(b.quarterlyCashFlow,
					b.ticker, StatementType.CASHFLOW, false);
		}
		if (b.quarterlyIncome != null) {
			this.quarterlyIncome = new FinancialTable(b.quarterlyIncome,
					b.ticker, StatementType.INCOME, false);
		}
		if (b.annualBalance != null) {

			this.annualBalance = new FinancialTable(b.annualBalance, b.ticker,
					StatementType.BALANCE, true);
		}
		if (b.annualCashFlow != null) {
			this.annualCashFlow = new FinancialTable(b.annualCashFlow,
					b.ticker, StatementType.CASHFLOW, true);
		}
		if (b.annualIncome != null) {
			this.annualIncome = new FinancialTable(b.annualIncome, b.ticker,
					StatementType.INCOME, true);
		}
	}

	public Financial() {

	}

	public FinancialTable getQuarterlyBalance() {
		return quarterlyBalance;
	}

	public FinancialTable getQuarterlyIncome() {
		return quarterlyIncome;
	}

	public FinancialTable getQuarterlyCashFlow() {
		return quarterlyCashFlow;
	}

	public FinancialTable getAnnualBalance() {
		return annualBalance;
	}

	public FinancialTable getAnnualIncome() {
		return annualIncome;
	}

	public FinancialTable getAnnualCashFlow() {
		return annualCashFlow;
	}

	public String getTicker() {
		return ticker;
	}

	public List<FinancialTable> tablesList() {
		List<FinancialTable> tables = new ArrayList<>();
		if (annualBalance != null) {
			tables.add(annualBalance);
		}
		if (annualCashFlow != null) {
			tables.add(annualCashFlow);
		}
		if (annualIncome != null) {
			tables.add(annualIncome);
		}
		if (quarterlyBalance != null) {
			tables.add(quarterlyBalance);
		}
		if (quarterlyCashFlow != null) {
			tables.add(quarterlyCashFlow);
		}
		if (quarterlyIncome != null) {
			tables.add(quarterlyIncome);
		}
		return tables;
	}

	public static class FinancialBuilder {
		private String ticker;
		private DoubleTable quarterlyBalance;
		private DoubleTable quarterlyIncome;
		private DoubleTable quarterlyCashFlow;
		private DoubleTable annualBalance;
		private DoubleTable annualIncome;
		private DoubleTable annualCashFlow;

		public FinancialBuilder ticker(String ticker) {
			this.ticker = ticker;
			return this;
		}

		public FinancialBuilder quarterlyBalance(DoubleTable quarterlyBalance) {
			this.quarterlyBalance = quarterlyBalance;
			return this;
		}

		public FinancialBuilder quarterlyIncome(DoubleTable quarterlyIncome) {
			this.quarterlyIncome = quarterlyIncome;
			return this;
		}

		public FinancialBuilder quarterlyCashFlow(DoubleTable quarterlyCashFlow) {
			this.quarterlyCashFlow = quarterlyCashFlow;
			return this;
		}

		public FinancialBuilder annualBalance(DoubleTable annualBalance) {
			this.annualBalance = annualBalance;
			return this;
		}

		public FinancialBuilder annualIncome(DoubleTable annualIncome) {
			this.annualIncome = annualIncome;
			return this;
		}

		public FinancialBuilder annualCashFlow(DoubleTable annualCashFlow) {
			this.annualCashFlow = annualCashFlow;
			return this;
		}

		public FinancialBuilder add(StatementType type, boolean annual,
				LocalDate date, int label, double value) {
			getTable(type, annual).add(date, label, value);
			return this;
		}

		public FinancialBuilder add(StatementType type, boolean annual,
				byte[] date, int label, double value) {
			getTable(type, annual).add(date, label, value);
			return this;
		}

		public DoubleTable getTable(StatementType type, boolean annual) {
			if (type == StatementType.BALANCE) {
				return annual ? getAnnualBalance() : getQuarterlyBalance();
			} else if (type == StatementType.INCOME) {
				return annual ? getAnnualIncome() : getQuarterlyIncome();
			} else {
				return annual ? getAnnualCashFlow() : getQuarterlyCashFlow();
			}
		}

		public Financial build() {
			return new Financial(this);
		}

		public String getTicker() {
			return ticker;
		}

		public void setTicker(String ticker) {
			this.ticker = ticker;
		}

		public DoubleTable getQuarterlyBalance() {
			if (quarterlyBalance == null) {
				quarterlyBalance = new DoubleTable();
			}
			return quarterlyBalance;
		}

		public void setQuarterlyBalance(DoubleTable quarterlyBalance) {
			this.quarterlyBalance = quarterlyBalance;
		}

		public DoubleTable getQuarterlyIncome() {
			if (quarterlyIncome == null) {
				this.quarterlyIncome = new DoubleTable();
			}
			return quarterlyIncome;
		}

		public void setQuarterlyIncome(DoubleTable quarterlyIncome) {
			this.quarterlyIncome = quarterlyIncome;
		}

		public DoubleTable getQuarterlyCashFlow() {
			if (quarterlyCashFlow == null) {
				this.quarterlyCashFlow = new DoubleTable();
			}
			return quarterlyCashFlow;
		}

		public void setQuarterlyCashFlow(DoubleTable quarterlyCashFlow) {
			this.quarterlyCashFlow = quarterlyCashFlow;
		}

		public DoubleTable getAnnualBalance() {
			if (annualBalance == null) {
				annualBalance = new DoubleTable();
			}
			return annualBalance;
		}

		public void setAnnualBalance(DoubleTable annualBalance) {
			this.annualBalance = annualBalance;
		}

		public DoubleTable getAnnualIncome() {
			if (annualIncome == null) {
				annualIncome = new DoubleTable();
			}
			return annualIncome;
		}

		public void setAnnualIncome(DoubleTable annualIncome) {
			this.annualIncome = annualIncome;
		}

		public DoubleTable getAnnualCashFlow() {
			if (annualCashFlow == null) {
				annualCashFlow = new DoubleTable();
			}
			return annualCashFlow;
		}

		public void setAnnualCashFlow(DoubleTable annualCashFlow) {
			this.annualCashFlow = annualCashFlow;
		}
	}

	@Override
	public Iterator<FinancialCell> iterator() {
		return new FinancialIterator(this);
	}

	private static class FinancialIterator implements Iterator<FinancialCell> {
		private final List<FinancialTable> tables;

		private int i = 0;
		private FinancialCell cell = new FinancialCell();
		private Iterator<DoubleTableCell> coreIterator;

		public FinancialIterator(final Financial source) {
			this.tables = source.tablesList();
		}

		@Override
		public boolean hasNext() {
			return i < tables.size()
					|| (coreIterator != null && coreIterator.hasNext());
		}

		@Override
		public FinancialCell next() {
			boolean found = false;
			do {
				if (coreIterator == null) {
					coreIterator = this.tables.get(i).iterator();
				}
				if (coreIterator.hasNext()) {
					DoubleTableCell coreCell = coreIterator.next();
					cell.setAnnual(tables.get(i).isAnnual());
					cell.setDate(coreCell.getDate());
					cell.setLabel(coreCell.getLabel());
					cell.setTicker(tables.get(i).getTicker());
					cell.setType(tables.get(i).getType());
					cell.setValue(coreCell.getValue());
				} else {
					i++;
					if (i >= tables.size()) {
						break;
					}
					coreIterator = this.tables.get(i).iterator();
				}
			} while (!found);
			return found ? cell : null;
		}
	}

	public void setQuarterlyBalance(FinancialTable quarterlyBalance) {
		this.quarterlyBalance = quarterlyBalance;
	}

	public void setQuarterlyIncome(FinancialTable quarterlyIncome) {
		this.quarterlyIncome = quarterlyIncome;
	}

	public void setQuarterlyCashFlow(FinancialTable quarterlyCashFlow) {
		this.quarterlyCashFlow = quarterlyCashFlow;
	}

	public void setAnnualBalance(FinancialTable annualBalance) {
		this.annualBalance = annualBalance;
	}

	public void setAnnualIncome(FinancialTable annualIncome) {
		this.annualIncome = annualIncome;
	}

	public void setAnnualCashFlow(FinancialTable annualCashFlow) {
		this.annualCashFlow = annualCashFlow;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public void set(FinancialTable table) {
		if (table == null) {
			throw new IllegalArgumentException("Can't set a null table.");
		}
		if (this.ticker == null) {
			this.ticker = table.getTicker();
		} else if (this.ticker.equals(table.getTicker())) {
			throw new IllegalArgumentException("The tickers don't agree");
		}
		if (table.getType() == StatementType.BALANCE) {
			if (table.isAnnual()) {
				this.annualBalance = table;
			} else {
				this.quarterlyBalance = table;
			}
		}
		if (table.getType() == StatementType.INCOME) {
			if (table.isAnnual()) {
				this.annualIncome = table;
			} else {
				this.quarterlyIncome = table;
			}
		}
		if (table.getType() == StatementType.CASHFLOW) {
			if (table.isAnnual()) {
				this.annualCashFlow = table;
			} else {
				this.quarterlyCashFlow = table;
			}
		}
	}

	public FinancialTable getTable(StatementType type, boolean annual) {
		if (type == StatementType.BALANCE) {
			if (annual) {
				return this.annualBalance;
			} else {
				return this.quarterlyBalance;
			}
		}
		if (type == StatementType.INCOME) {
			if (annual) {
				return this.annualIncome;
			} else {
				return this.quarterlyIncome;
			}
		}
		if (type == StatementType.CASHFLOW) {
			if (annual) {
				return this.annualCashFlow;
			} else {
				return this.quarterlyCashFlow;
			}
		}
		return null;
	}
}