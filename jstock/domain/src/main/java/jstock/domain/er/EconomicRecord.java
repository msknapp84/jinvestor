package jstock.domain.er;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Iterator;

import knapp.common.collections.time.DoublesOverTimeImmutable;
import knapp.common.collections.time.ValueOnDay;
import knapp.common.data.domain.Entity;

public class EconomicRecord extends Entity implements Iterable<ValueOnDay<Double>> {
	private String seriesId;
	private DoublesOverTimeImmutable values;
	private String title;
	private String source;
	private String release;
	private boolean seasonallyAdjusted;
	private ChronoUnit frequency;
	private int periodicity = 1;
	private String units;
	
	/**
	 * This information is available on SLF
Title:               Consumer Price Index for All Urban Consumers: All Items
Series ID:           CPIAUCSL
Source:              US. Bureau of Labor Statistics
Release:             Consumer Price Index
Seasonal Adjustment: Seasonally Adjusted
Frequency:           Monthly
Units:               Index 1982-84=100
	 */
	
	
	public EconomicRecord() {
		
	}
	
	public EconomicRecord(String seriesId) {
		this.seriesId = seriesId;
	}
	
	@Override
	public Iterator<ValueOnDay<Double>> iterator() {
		return values.iterator();
	}

	public String getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(String seriesId) {
		this.seriesId = seriesId;
	}

	public DoublesOverTimeImmutable getValues() {
		return values;
	}

	public void setValues(DoublesOverTimeImmutable values) {
		this.values = values;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getRelease() {
		return release;
	}

	public void setRelease(String release) {
		this.release = release;
	}

	public boolean isSeasonallyAdjusted() {
		return seasonallyAdjusted;
	}

	public void setSeasonallyAdjusted(boolean seasonallyAdjusted) {
		this.seasonallyAdjusted = seasonallyAdjusted;
	}

	public ChronoUnit getFrequency() {
		return frequency;
	}

	public void setFrequency(ChronoUnit frequency) {
		this.frequency = frequency;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public int getPeriodicity() {
		return periodicity;
	}

	public void setPeriodicity(int periodicity) {
		this.periodicity = periodicity;
	}
	
	public double getValue(String date) {
		return this.values.get(date);
	}

	public double getValue(LocalDate date) {
		return this.values.get(date);
	}
	
	public double getValue(String date,boolean latest) {
		return this.values.get(date,latest);
	}

	public double getValue(LocalDate date,boolean latest) {
		return this.values.get(date,latest);
	}
}