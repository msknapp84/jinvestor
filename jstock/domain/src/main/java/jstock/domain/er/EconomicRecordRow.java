package jstock.domain.er;

import java.time.LocalDate;

import knapp.common.collections.time.ValueOnDay;
import knapp.common.util.StringHelp;

public class EconomicRecordRow extends ValueOnDay<Double> {
	
	private String seriesId;

	public String getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(String seriesId) {
		this.seriesId = seriesId;
	}
	
	public String rowId() {
		return determineRowId(getSeriesId(), getDate());
	}
	
	public static String determineRowId(String seriesId,LocalDate date) {
		String inverseDate = StringHelp.createInverseDateString(date);
		return String.format("%s-%s",seriesId,inverseDate);
	}
	
	public static String determineSeriesIdFromRowId(String currentRowId) {
		if (currentRowId==null || !currentRowId.contains("-")) {
			return currentRowId;
		}
		String[] parts = currentRowId.split("-");
		return parts[0];
	}
	
	public static Object[] splitRowId(String currentRowId) {
		String[] parts = currentRowId.split("-");
		return new Object[]{parts[0],StringHelp.fromInverseDateString(parts[1])};
	}
	
	public static LocalDate determineDateFromRowId(String currentRowId) {
		String[] parts = currentRowId.split("-");
		return StringHelp.fromInverseDateString(parts[1]);
	}
}