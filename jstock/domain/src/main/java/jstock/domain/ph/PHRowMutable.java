package jstock.domain.ph;

import java.time.LocalDate;

public class PHRowMutable {
	private LocalDate date;
	private int openPennies;
	private int closePennies;
	private int highPennies;
	private int lowPennies;
	private int adjClosePennies;
	private int volume;
	
	public PHRowMutable date(LocalDate date) {
		this.date = date;
		return this;
	}
	public PHRowMutable openPennies(Integer openPennies) {
		this.openPennies = openPennies;
		return this;
	}
	public PHRowMutable closePennies(Integer closePennies) {
		this.closePennies = closePennies;
		return this;
	}
	public PHRowMutable adjClosePennies(Integer adjClosePennies) {
		this.adjClosePennies = adjClosePennies;
		return this;
	}
	public PHRowMutable lowPennies(Integer lowPennies) {
		this.lowPennies = lowPennies;
		return this;
	}
	public PHRowMutable volume(Integer volume) {
		this.volume = volume;
		return this;
	}

	public PHRowMutable highPennies(Integer highPennies) {
		this.highPennies = highPennies;
		return this;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public Integer getOpenPennies() {
		return openPennies;
	}
	public void setOpenPennies(Integer openPennies) {
		this.openPennies = openPennies;
	}
	public Integer getClosePennies() {
		return closePennies;
	}
	public void setClosePennies(Integer closePennies) {
		this.closePennies = closePennies;
	}
	public Integer getHighPennies() {
		return highPennies;
	}
	public void setHighPennies(Integer highPennies) {
		this.highPennies = highPennies;
	}
	public Integer getLowPennies() {
		return lowPennies;
	}
	public void setLowPennies(Integer lowPennies) {
		this.lowPennies = lowPennies;
	}
	public Integer getAdjClosePennies() {
		return adjClosePennies;
	}
	public void setAdjClosePennies(Integer adjClosePennies) {
		this.adjClosePennies = adjClosePennies;
	}
	public Integer getVolume() {
		return volume;
	}
	public void setVolume(Integer volume) {
		this.volume = volume;
	}
}
