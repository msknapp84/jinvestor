package jstock.domain.ph;

import java.util.ArrayList;
import java.util.List;

import jstock.avro.PriceHistory;
import jstock.domain.time.StockTimeUtil;
import knapp.common.data.domain.Entity;

public class PH extends Entity {
	private final List<PHRow> cells;
	
	private PH(PHBuilder b) {
		this.cells = new ArrayList<>(b.cells);
	}
	
	public static class PHBuilder {
		private List<PHRow> cells = new ArrayList<>();
		
		public PHBuilder add(PHRow cell) {
			this.cells.add(cell);
			return this;
		}
		
		public PH build() {
			return new PH(this);
		}
	}
	
	public List<PriceHistory> toPriceHistories(String ticker) {
		List<PriceHistory> phs = new ArrayList<>();
		for (PHRow phc : cells) {
			PriceHistory ph = toPriceHistory(ticker, phc);
			phs.add(ph);
		}
		return phs;
	}
	
	public static PriceHistory toPriceHistory(String ticker,PHRow cell) {
		PriceHistory ph = new PriceHistory();
		ph.setTicker(ticker);
		ph.setAdjClosePennies(cell.getAdjClosePennies());
		ph.setClosePennies(cell.getClosePennies());
		int daysSince1970 = StockTimeUtil.daysSince1970(cell.getDate());
		ph.setDaysSinceEpoch(daysSince1970);
		ph.setHighPennies(cell.getHighPennies());
		ph.setLowPennies(cell.getLowPennies());
		ph.setOpenPennies(cell.getOpenPennies());
		return ph;
	}
}
