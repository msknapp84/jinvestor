package jstock.domain.ph;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import knapp.common.data.domain.Entity;
import knapp.common.util.ByteArray;
import knapp.common.util.StringHelp;

public class PHTablet extends Entity implements Iterable<PHRowMutable> {
	private final ByteArray ticker;
	private final LocalDate[] dates;
	private final int[] openPennies;
	private final int[] highPennies;
	private final int[] lowPennies;
	private final int[] closePennies;
	private final int[] volume;
	private final int[] adjClosePennies;
	
	public static String determineId(ByteArray ticker,LocalDate date) {
		return determineId(ticker.toString(), date);
	}
	
	public static String determineId(String ticker,LocalDate date) {
		// let's use the real ticker symbol string, but  
		// anything that streams a bunch of ticker symbols 
		// should stream them in a random order.
		
		// we want the date to be inverted and readable
		String inverseDateString = StringHelp.createInverseDateString(date);
		return String.format("%s-%s",ticker,inverseDateString);
	}
	
	public static LocalDate determineDate(String rowId) {
		String datePortion = rowId.split("-")[1];
		return StringHelp.fromInverseDateString(datePortion);
	}
	
	public static String determineTicker(String id) {
		if (id == null) {
			return null;
		}
		if (id.contains("-")) {
			return StringUtils.substringBefore(id, "-");
		} else {
			return id;
		}
	}

	private PHTablet(PHTabletBuilder b) {
		this.setId(new ByteArray(b.ticker));
		this.ticker = b.ticker;
		this.dates = b.dates;
		this.openPennies = b.openPennies;
		this.highPennies = b.highPennies;
		this.lowPennies = b.lowPennies;
		this.closePennies = b.closePennies;
		this.volume = b.volume;
		this.adjClosePennies = b.adjClosePennies;
		this.setId(ticker);
	}

	public PHTablet(PHTabletGrowableBuilder b) {
		this.setId(new ByteArray(b.ticker));
		this.ticker = b.ticker;
		int sz = b.dates.size();
		this.dates = b.dates.toArray(new LocalDate[sz]);
		this.openPennies = toArr(b.openPennies);
		this.highPennies = toArr(b.highPennies);
		this.lowPennies = toArr(b.lowPennies);
		this.closePennies = toArr(b.closePennies);
		this.volume = toArr(b.volume);
		this.adjClosePennies = toArr(b.adjClosePennies);
	}
	
	private static int[] toArr(List<Integer> vals) {
		int[] r = new int[vals.size()];
		for (int i = 0;i<vals.size();i++) {
			r[i] = vals.get(i);
		}
		return r;
	}

	@Override
	public Iterator<PHRowMutable> iterator() {
		return new RowIterator(this);
	}

	public ByteArray getTicker() {
		return ticker;
	}

	private static class RowIterator implements Iterator<PHRowMutable> {
		private int index = 0;
		private final PHRowMutable row;
		private final PHTablet source;

		public RowIterator(PHTablet source) {
			this.source = source;
			this.row = new PHRowMutable();
		}

		@Override
		public boolean hasNext() {
			return this.index < source.dates.length;
		}

		@Override
		public PHRowMutable next() {
//			if (index == source.adjClosePennies.length-1) {
//				System.out.println("hi");
//			}
			// re-use the row for little garbage clean-up.
			row.adjClosePennies(source.adjClosePennies[index])
					.date(source.dates[index])
					.openPennies(source.openPennies[index])
					.lowPennies(source.lowPennies[index])
					.highPennies(source.highPennies[index])
					.closePennies(source.closePennies[index])
					.volume(source.volume[index]);
			index++;
			return row;
		}
	}

	public static final PHTabletBuilder start(String ticker, int size) {
		return new PHTabletBuilder(ticker, size);
	}

	public static final PHTabletGrowableBuilder growable(String ticker) {
		return new PHTabletGrowableBuilder(ticker, 100);
	}

	public static final PHTabletGrowableBuilder growable(ByteArray ticker) {
		return new PHTabletGrowableBuilder(ticker, 100);
	}
	
	public static final PHTabletGrowableBuilder growable(String ticker, int size) {
		return new PHTabletGrowableBuilder(ticker, size);
	}

	public static final class PHTabletBuilder {
		private final ByteArray ticker;
		private final LocalDate[] dates;
		private final int[] openPennies;
		private final int[] highPennies;
		private final int[] lowPennies;
		private final int[] closePennies;
		private final int[] volume;
		private final int[] adjClosePennies;
		private int index = 0;
		private boolean closed = false;

		public PHTabletBuilder(String ticker, int size) {
			this(new ByteArray(ticker),size);
		}

		public PHTabletBuilder(ByteArray ticker, int size) {
			this.ticker = ticker;
			this.dates = new LocalDate[size];
			this.openPennies = new int[size];
			this.highPennies = new int[size];
			this.lowPennies = new int[size];
			this.closePennies = new int[size];
			this.volume = new int[size];
			this.adjClosePennies = new int[size];
		}

		public void add(LocalDate date, int openPenny, int highPenny,
				int lowPenny, int closePenny, int vol, int adjClosePenny) {
			if (closed) {
				throw new IllegalStateException(
						"This builder is already closed.");
			}
			if (index >= dates.length) {
				throw new IllegalStateException(
						"We have already filled this tablet.");
			}
			dates[index] = date;
			openPennies[index] = openPenny;
			highPennies[index] = highPenny;
			lowPennies[index] = lowPenny;
			closePennies[index] = closePenny;
			volume[index] = vol;
			adjClosePennies[index] = adjClosePenny;
			index++;
		}

		public PHTablet build() {
			this.closed = true;
			return new PHTablet(this);
		}

		public ByteArray getTicker() {
			return ticker;
		}
	}

	public static final class PHTabletGrowableBuilder {
		private final ByteArray ticker;
		private final List<LocalDate> dates;
		private final List<Integer> openPennies;
		private final List<Integer> highPennies;
		private final List<Integer> lowPennies;
		private final List<Integer> closePennies;
		private final List<Integer> volume;
		private final List<Integer> adjClosePennies;
		private boolean closed = false;

		public PHTabletGrowableBuilder(String ticker, int size) {
			this(new ByteArray(ticker),size);
		}

		public PHTabletGrowableBuilder(ByteArray ticker, int size) {
			this.ticker = ticker;
			this.dates = new ArrayList<>(size);
			this.openPennies = new ArrayList<>(size);
			this.highPennies = new ArrayList<>();
			this.lowPennies = new ArrayList<>();
			this.closePennies = new ArrayList<>();
			this.volume = new ArrayList<>();
			this.adjClosePennies = new ArrayList<>();
		}

		public void add(PHRowMutable row) {
			add(row.getDate(), row.getOpenPennies(), row.getHighPennies(),
					row.getLowPennies(), row.getClosePennies(),
					row.getVolume(), row.getAdjClosePennies());
		}

		public void add(LocalDate date, int openPenny, int highPenny,
				int lowPenny, int closePenny, int vol, int adjClosePenny) {
			if (closed) {
				throw new IllegalStateException(
						"This builder is already closed.");
			}
			dates.add(date);
			openPennies.add(openPenny);
			highPennies.add(highPenny);
			lowPennies.add(lowPenny);
			closePennies.add(closePenny);
			volume.add(vol);
			adjClosePennies.add(adjClosePenny);
		}

		public PHTablet build() {
			this.closed = true;
			return new PHTablet(this);
		}

		public ByteArray getTicker() {
			return ticker;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(adjClosePennies);
		result = prime * result + Arrays.hashCode(closePennies);
		result = prime * result + Arrays.hashCode(dates);
		result = prime * result + Arrays.hashCode(highPennies);
		result = prime * result + Arrays.hashCode(lowPennies);
		result = prime * result + Arrays.hashCode(openPennies);
		result = prime * result + ((ticker == null) ? 0 : ticker.hashCode());
		result = prime * result + Arrays.hashCode(volume);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PHTablet other = (PHTablet) obj;
		if (!Arrays.equals(adjClosePennies, other.adjClosePennies))
			return false;
		if (!Arrays.equals(closePennies, other.closePennies))
			return false;
		if (!Arrays.equals(dates, other.dates))
			return false;
		if (!Arrays.equals(highPennies, other.highPennies))
			return false;
		if (!Arrays.equals(lowPennies, other.lowPennies))
			return false;
		if (!Arrays.equals(openPennies, other.openPennies))
			return false;
		if (ticker == null) {
			if (other.ticker != null)
				return false;
		} else if (!ticker.equals(other.ticker))
			return false;
		if (!Arrays.equals(volume, other.volume))
			return false;
		return true;
	}
}