package jstock.domain.ph;

import java.time.LocalDate;

public class PHRow {
	
	private final LocalDate date;
	private final int openPennies;
	private final int closePennies;
	private final int highPennies;
	private final int lowPennies;
	private final int adjClosePennies;
	private final int volume;

	private PHRow(PHRowMutable b) {
		this.date = b.getDate();
		this.openPennies = b.getOpenPennies();
		this.closePennies = b.getClosePennies();
		this.highPennies = b.getHighPennies();
		this.lowPennies = b.getLowPennies();
		this.adjClosePennies = b.getAdjClosePennies();
		this.volume = b.getVolume();
	}
	
	
	public LocalDate getDate() {
		return date;
	}



	public Integer getOpenPennies() {
		return openPennies;
	}



	public Integer getClosePennies() {
		return closePennies;
	}



	public Integer getHighPennies() {
		return highPennies;
	}



	public Integer getLowPennies() {
		return lowPennies;
	}



	public Integer getAdjClosePennies() {
		return adjClosePennies;
	}



	public Integer getVolume() {
		return volume;
	}

}