package jstock.domain;

/*
 * #%L
 * jinvestor.parent
 * %%
 * Copyright (C) 2014 Michael Scott Knapp
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import knapp.common.data.domain.Entity;
import knapp.common.util.ByteArray;
import knapp.common.util.ByteUtils;
import knapp.common.util.TimeSpan;

public class Download extends Entity {
	private Calendar attemptedOn;
	private URI uri;
	private boolean wasSuccessful;
	private String foreignTable;
	private ByteArray foreignId;
	private int responseCode;
	private String responseMessage;
	private String system;
	private TimeSpan queryTimeSpan;
	private TimeSpan dataTimeSpan;
	private String saveFilePath;
	private String md5;
	private String appVersion;
	private boolean serviceDenied;

	public Download() {

	}

	public Download(Download original) {
		this(original, original.getAttemptedOn());
	}

	public Download(Download original, Calendar time) {
		super();
		this.attemptedOn = time;
		this.attemptedOn.setTime(original.getAttemptedOn().getTime());
		this.uri = original.getUri();
		this.setId(determineId(this.uri, time));
		this.responseCode = original.getResponseCode();
		this.responseMessage = original.getResponseMessage();
		this.system = original.getSystem();
		this.queryTimeSpan = original.getQueryTimeSpan();
		this.dataTimeSpan = original.getDataTimeSpan();
		this.saveFilePath = original.getSaveFilePath();
		this.md5 = original.getMd5();
		this.appVersion = original.getAppVersion();
		this.serviceDenied = original.isServiceDenied();
		this.foreignId = original.getForeignId();
		this.wasSuccessful = original.isWasSuccessful();
		this.foreignTable = original.getForeignTable();
	}

	public Download(final URL url, final ByteArray tickerSymbol) {
		try {
			this.uri = url.toURI();
		} catch (URISyntaxException e) {
			throw new IllegalArgumentException("The given uri is invalid: "
					+ url.toString(), e);
		}
		this.attemptedOn = Calendar.getInstance();
		this.setId(determineId(this.uri, attemptedOn));
	}

	public Download(final String uri, final ByteArray tickerSymbol) {
		try {
			this.uri = new URI(uri);
		} catch (URISyntaxException e) {
			throw new IllegalArgumentException("The given uri is invalid: "
					+ uri, e);
		}
		this.attemptedOn = Calendar.getInstance();
		this.setId(determineId(this.uri, attemptedOn));
	}

	public Download(final String uri, final String tickerSymbol,
			final Calendar attemptedOn) {
		super();
		try {
			this.uri = new URI(uri);
			setId(determineId(this.uri, attemptedOn));
		} catch (URISyntaxException e) {
			throw new IllegalArgumentException("The given uri is invalid: "
					+ uri, e);
		}
		this.attemptedOn = attemptedOn;
	}

	public Download(DownloadMutable original) {
		super();
		this.attemptedOn = Calendar.getInstance();
		this.attemptedOn.setTime(original.getAttemptedOn().getTime());
		this.uri = original.getUri();
		this.responseCode = original.getResponseCode();
		this.responseMessage = original.getResponseMessage();
		this.system = original.getSystem();
		this.queryTimeSpan = original.getQueryTimeSpan();
		this.dataTimeSpan = original.getDataTimeSpan();
		this.saveFilePath = original.getSaveFilePath();
		this.md5 = original.getMd5();
		this.appVersion = original.getAppVersion();
		this.serviceDenied = original.isServiceDenied();
		this.setId(determineId(uri, attemptedOn));
	}

	public void setUriString(String uri) {
		try {
			this.uri = new URI(uri);
		} catch (Exception e) {
			throw new IllegalArgumentException("The given uri is invalid. "
					+ uri, e);
		}
		this.setId(determineId(this.uri, attemptedOn));
	}

	public void updateSuccess() {
		this.wasSuccessful = !serviceDenied && 200 < this.responseCode
				&& this.responseCode < 300;
	}

	public static ByteArray determineId(URI uri, Calendar attemptedOn) {
		return new ByteArray(String.format("%s-%s", determineIdForUri(uri),
				determineIdForTime(attemptedOn)));
	}

	public static String determineIdForTime(Calendar attemptedOn) {
		if (attemptedOn != null) {
			Long inverseTime = Long.MAX_VALUE - attemptedOn.getTimeInMillis();
			return inverseTime.toString();
		}
		return "";
	}

	public static String determineIdForUri(URI uri) {
		if (uri == null) {
			return "";
		}
		// base 64 portions of the uri + inverse of time.
		String host = uri.getHost();
		String path = uri.getPath();
		String parms = uri.getQuery();
		return determineIdForUri(host, path, parms);
	}

	public static String determineIdForUri(String host, String path,
			String parms) {
		// base 64 portions of the uri + inverse of time.
		List<Byte> bytes = new ArrayList<>(22);
		ByteUtils.addBytes(bytes, ByteUtils.hashTruncateAndBase64(host, 2));
		bytes.add((byte) '-');
		if (path != null) {
			ByteUtils.addBytes(bytes, ByteUtils.hashTruncateAndBase64(path, 3));
			bytes.add((byte) '-');
			if (parms != null) {
				ByteUtils.addBytes(bytes,
						ByteUtils.hashTruncateAndBase64(parms, 3));
			}
		}
		return listOfBytesToString(bytes);
	}

	public static String determineIdPrefix(String host, String path) {
		return determineIdForUri(host, path, null);
	}

	public static String determineIdPrefix(String host) {
		return determineIdForUri(host, null, null);
	}

	private static String listOfBytesToString(List<Byte> bytes) {
		byte[] forString = new byte[bytes.size()];
		for (int i = 0; i < bytes.size(); i++) {
			forString[i] = bytes.get(i);
		}
		return new String(forString);
	}

	public Calendar getAttemptedOn() {
		return attemptedOn;
	}

	public URI getUri() {
		return uri;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((attemptedOn == null) ? 0 : attemptedOn.hashCode());
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Download other = (Download) obj;
		if (attemptedOn == null) {
			if (other.attemptedOn != null)
				return false;
		} else if (!attemptedOn.equals(other.attemptedOn))
			return false;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}

	public String getSaveFilePath() {
		return saveFilePath;
	}

	public void setSaveFilePath(String saveFilePath) {
		this.saveFilePath = saveFilePath;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public boolean isServiceDenied() {
		return serviceDenied;
	}

	public void setServiceDenied(boolean serviceDenied) {
		this.serviceDenied = serviceDenied;
	}

	public TimeSpan getQueryTimeSpan() {
		return queryTimeSpan;
	}

	public void setQueryTimeSpan(TimeSpan queryTimeSpan) {
		this.queryTimeSpan = queryTimeSpan;
	}

	public TimeSpan getDataTimeSpan() {
		return dataTimeSpan;
	}

	public void setDataTimeSpan(TimeSpan dataTimeSpan) {
		this.dataTimeSpan = dataTimeSpan;
	}

	public int age(TimeUnit timeUnit) {
		Calendar now = Calendar.getInstance();
		long nowTime = now.getTimeInMillis();
		long attemptTime = attemptedOn.getTimeInMillis();
		long delta = nowTime - attemptTime;
		int age = 0;
		if (timeUnit == TimeUnit.HOURS) {
			age = (int) Math.floor(delta / (1000L * 60L * 60L));
		} else if (timeUnit == TimeUnit.SECONDS) {
			age = (int) Math.floor(delta / (1000L));
		} else if (timeUnit == TimeUnit.MINUTES) {
			age = (int) Math.floor(delta / (1000L * 60L));
		} else if (timeUnit == TimeUnit.DAYS) {
			age = (int) Math.floor(delta / (1000L * 60L * 60L * 24L));
		} else {
			throw new IllegalArgumentException("We do not support age in "
					+ timeUnit);
		}
		return age;
	}

	public boolean wasSuccessful() {
		return !isServiceDenied() && getResponseCode() >= 200
				&& getResponseCode() < 300;
	}

	public static final class ChronComparator implements Comparator<Download> {
		private final boolean ascending;

		public ChronComparator(final boolean ascending) {
			this.ascending = ascending;
		}

		@Override
		public int compare(Download arg0, Download arg1) {
			return (ascending ? 1 : -1)
					* (arg0.getAttemptedOn().compareTo(arg1.getAttemptedOn()));
		}
	}

	public static String determineIdForUri(String uri2) {
		String id = null;
		try {
			URI u = new URI(uri2);
			id = determineIdForUri(u);
		} catch (Exception e) {
			throw new IllegalArgumentException("The given uri is not valid. "
					+ uri2, e);
		}
		return id;
	}

	public boolean isWasSuccessful() {
		return wasSuccessful;
	}

	public void setWasSuccessful(boolean wasSuccessful) {
		this.wasSuccessful = wasSuccessful;
	}

	public String getForeignTable() {
		return foreignTable;
	}

	public void setForeignTable(String foreignTable) {
		this.foreignTable = foreignTable;
	}

	public ByteArray getForeignId() {
		return foreignId;
	}

	public void setForeignId(ByteArray foreignId) {
		this.foreignId = foreignId;
	}

	public void setAttemptedOn(Calendar attemptedOn) {
		this.attemptedOn = attemptedOn;
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

	// public static final class SuccessfulPredicate implements
	// Predicate<Download> {
	// private final boolean successful;
	// public SuccessfulPredicate(final boolean successful) {
	// this.successful = successful;
	// }
	// @Override
	// public boolean evaluate(Download arg0) {
	// boolean wasSuccess = arg0.responseCode>=200 && arg0.responseCode<400;
	// return successful ? wasSuccess : !wasSuccess;
	// }
	// }
}