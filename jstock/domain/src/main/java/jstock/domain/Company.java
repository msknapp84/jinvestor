package jstock.domain;

import jstock.domain.fh.Financial;
import jstock.domain.ph.PH;
import knapp.common.data.domain.Entity;

public class Company extends Entity {
	private PH priceHistory;
	private Financial financialHistory;
	private String ticker;
	private String name;
	private byte ipoYearAfterEpoch;
	private String sector;
	private String industry;
	
	public PH getPriceHistory() {
		return priceHistory;
	}

	public Financial getFinancialHistory() {
		return financialHistory;
	}

	public Company() {
		
	}
	
	public Company(CompanyBuilder companyBuilder) {
		this.priceHistory = companyBuilder.priceHistory;
		this.financialHistory = companyBuilder.financialHistory;
	}
	
	public static class CompanyBuilder {
		private PH priceHistory;
		private Financial financialHistory;
		
		
		public CompanyBuilder ph(PH priceHistory) {
			this.priceHistory = priceHistory;
			return this;
		}
		public CompanyBuilder fh(Financial financialHistory) {
			this.financialHistory = financialHistory;
			return this;
		}
		public Company build() {
			return new Company(this);
		}
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte getIpoYearAfterEpoch() {
		return ipoYearAfterEpoch;
	}

	public void setIpoYearAfterEpoch(byte ipoYearAfterEpoch) {
		this.ipoYearAfterEpoch = ipoYearAfterEpoch;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public void setPriceHistory(PH priceHistory) {
		this.priceHistory = priceHistory;
	}

	public void setFinancialHistory(Financial financialHistory) {
		this.financialHistory = financialHistory;
	}

	public void setIpoYear(Integer year) {
		this.ipoYearAfterEpoch = (byte) (year - 1970);
	}
	public int getIpoYear() {
		return this.ipoYearAfterEpoch+1970;
	}
}