package jstock.domain;

/*
 * #%L
 * jinvestor.parent
 * %%
 * Copyright (C) 2014 Michael Scott Knapp
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import knapp.common.data.domain.Entity;
import knapp.common.util.ByteArray;
import knapp.common.util.TimeSpan;

public class DownloadMutable extends Entity {
	private Calendar attemptedOn;
	private URI uri;
	private int responseCode;
	private String responseMessage;
	private String system;
	private TimeSpan queryTimeSpan;
	private TimeSpan dataTimeSpan;
	private String saveFilePath;
	private String md5;
	private String appVersion;
	private boolean serviceDenied;

	public DownloadMutable() {
		this.attemptedOn = Calendar.getInstance();
		responseCode = 200;
		appVersion = "1";
	}

	public DownloadMutable(DownloadMutable original) {
		this(original, original.getAttemptedOn());
		this.setId(determineId(uri, attemptedOn));
	}

	public DownloadMutable(DownloadMutable original, Calendar time) {
		super(original.getId());
		this.attemptedOn = time;
		this.attemptedOn.setTime(original.getAttemptedOn().getTime());
		this.uri = original.getUri();
		this.responseCode = original.getResponseCode();
		this.responseMessage = original.getResponseMessage();
		this.system = original.getSystem();
		this.queryTimeSpan = original.getQueryTimeSpan();
		this.dataTimeSpan = original.getDataTimeSpan();
		this.saveFilePath = original.getSaveFilePath();
		this.md5 = original.getMd5();
		this.appVersion = original.getAppVersion();
		this.serviceDenied = original.isServiceDenied();
		this.setId(determineId(uri, attemptedOn));
	}

	public DownloadMutable(final String uri, final String tickerSymbol) {
		this.attemptedOn = Calendar.getInstance();
		setUriString(uri);
	}

	public DownloadMutable(final String uri, final String tickerSymbol,
			final Calendar attemptedOn) {
		super();
		this.attemptedOn = attemptedOn;
		setUriString(uri);
	}

	public Download toDownload() {
		if (this.attemptedOn == null) {
			this.attemptedOn = Calendar.getInstance();
		}
		if (uri == null) {
			throw new IllegalArgumentException("The uri is null");
		}
		return new Download(this);
	}

	public static ByteArray determineId(URI uri, Calendar attemptedOn) {
		if (uri == null) {
			return new ByteArray("");
		}
		return Download.determineId(uri, attemptedOn);
//		// base 64 portions of the uri + inverse of time.
//		List<Byte> bytes = new ArrayList<>(22);
//		String host = uri.getHost();
//		String path = uri.getPath();
//		String parms = uri.getQuery();
//		ByteUtils.addBytes(bytes, ByteUtils.hashTruncateAndBase64(host, 2));
//		bytes.add((byte) '-');
//		ByteUtils.addBytes(bytes, ByteUtils.hashTruncateAndBase64(path, 3));
//		bytes.add((byte) '-');
//		ByteUtils.addBytes(bytes, ByteUtils.hashTruncateAndBase64(parms, 3));
//		bytes.add((byte) '-');
//		// I want it in descending order by time
//		if (attemptedOn != null) {
//			long inverseTime = Long.MAX_VALUE - attemptedOn.getTimeInMillis();
//			// can't just base64 this since the order won't be maintained.
//			// so convert it to a string and use its bytes
//			ByteUtils.addBytes(bytes, String.valueOf(inverseTime).getBytes());
//		}
//		// now we have something where all urls go to the same place, and
//		// are
//		// kept in order, with time descending, and all
//		// row ids are composed of readable characters.
//		byte[] forString = new byte[bytes.size()];
//		for (int i = 0; i < bytes.size(); i++) {
//			forString[i] = bytes.get(i);
//		}
//		return String.valueOf(forString);
	}

	public Calendar getAttemptedOn() {
		return attemptedOn;
	}

	public String getUriString() {
		return uri.toString();
	}

	public URI getUri() {
		return uri;
	}
	
	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((attemptedOn == null) ? 0 : attemptedOn.hashCode());
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DownloadMutable other = (DownloadMutable) obj;
		if (attemptedOn == null) {
			if (other.attemptedOn != null)
				return false;
		} else if (!attemptedOn.equals(other.attemptedOn))
			return false;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}

	public String getSaveFilePath() {
		return saveFilePath;
	}

	public void setSaveFilePath(String saveFilePath) {
		this.saveFilePath = saveFilePath;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public boolean isServiceDenied() {
		return serviceDenied;
	}

	public void setServiceDenied(boolean serviceDenied) {
		this.serviceDenied = serviceDenied;
	}

	public TimeSpan getQueryTimeSpan() {
		return queryTimeSpan;
	}

	public void setQueryTimeSpan(TimeSpan queryTimeSpan) {
		this.queryTimeSpan = queryTimeSpan;
	}

	public TimeSpan getDataTimeSpan() {
		return dataTimeSpan;
	}

	public void setDataTimeSpan(TimeSpan dataTimeSpan) {
		this.dataTimeSpan = dataTimeSpan;
	}

	public int age(TimeUnit timeUnit) {
		Calendar now = Calendar.getInstance();
		long nowTime = now.getTimeInMillis();
		long attemptTime = attemptedOn.getTimeInMillis();
		long delta = nowTime - attemptTime;
		int age = 0;
		if (timeUnit == TimeUnit.HOURS) {
			age = (int) Math.floor(delta / (1000L * 60L * 60L));
		} else if (timeUnit == TimeUnit.SECONDS) {
			age = (int) Math.floor(delta / (1000L));
		} else if (timeUnit == TimeUnit.MINUTES) {
			age = (int) Math.floor(delta / (1000L * 60L));
		} else if (timeUnit == TimeUnit.DAYS) {
			age = (int) Math.floor(delta / (1000L * 60L * 60L * 24L));
		} else {
			throw new IllegalArgumentException("We do not support age in "
					+ timeUnit);
		}
		return age;
	}

	public static final class ChronComparator implements
			Comparator<DownloadMutable> {
		private final boolean ascending;

		public ChronComparator(final boolean ascending) {
			this.ascending = ascending;
		}

		@Override
		public int compare(DownloadMutable arg0, DownloadMutable arg1) {
			return (ascending ? 1 : -1)
					* (arg0.getAttemptedOn().compareTo(arg1.getAttemptedOn()));
		}
	}

	public void setAttemptedOn(Calendar attemptedOn) {
		this.attemptedOn = attemptedOn;
		this.setId(determineId(uri, attemptedOn));
	}

	public void setUriString(String uri) {
		try {
			this.uri = new URI(uri);
		} catch (Exception e) {
			throw new IllegalArgumentException("The given uri is invalid. "+uri,e);
		}
		this.setId(determineId(this.uri, attemptedOn));
	}

	public DownloadMutable attemptedOn(Calendar attemptedOn) {
		this.attemptedOn = attemptedOn;
		this.setId(determineId(uri, attemptedOn));
		return this;
	}

	public DownloadMutable uriString(String uri) {
		setUriString(uri);
		return this;
	}

	public DownloadMutable saveFilePath(String saveFilePath) {
		this.saveFilePath = saveFilePath;
		return this;
	}

	public DownloadMutable md5(String md5) {
		this.md5 = md5;
		return this;
	}

	public DownloadMutable appVersion(String appVersion) {
		this.appVersion = appVersion;
		return this;
	}

	public DownloadMutable appVersion(Number appVersion) {
		this.appVersion = appVersion.toString();
		return this;
	}

	public DownloadMutable serviceDenied(boolean serviceDenied) {
		this.serviceDenied = serviceDenied;
		return this;
	}

	public DownloadMutable queryTimeSpan(TimeSpan queryTimeSpan) {
		this.queryTimeSpan = queryTimeSpan;
		return this;
	}

	public DownloadMutable dataTimeSpan(TimeSpan dataTimeSpan) {
		this.dataTimeSpan = dataTimeSpan;
		return this;
	}

	public DownloadMutable responseCode(int responseCode) {
		this.responseCode = responseCode;
		return this;
	}

	public DownloadMutable responseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
		return this;
	}

	public DownloadMutable system(String system) {
		this.system = system;
		return this;
	}

	public DownloadMutable on(String d) {
		this.attemptedOn = Calendar.getInstance();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			Date dt = sdf.parse(d);
			this.attemptedOn.setTime(dt);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return this;
	}
}