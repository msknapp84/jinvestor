package jstock.domain.time;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import knapp.common.data.domain.Validatable;

public class TimeSeries extends Validatable {

	private int baseYear;
	private final double[][][] ymd;

	public TimeSeries(int baseYear, double[][][] ymd) {
		this.ymd=ymd;
		this.baseYear = baseYear;
	}

	public Double getValue(Date date) {
		return getValue(date,null);
	}

	public Double getValue(Date date,Double defaultValue) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);
		int y=year-baseYear;
		if (y<0 || y >= ymd.length) {
			return defaultValue;
		}
		return ymd[y][month][day];
	}

	public int getIntValue(Date date) {
		return getIntValue(date,null);
	}

	public int getIntValue(Date date,Double defaultValue) {
		return getValue(date,null).intValue();
	}


	public boolean isSet(Date date) {
		return getValue(date,null) == null;
	}


	public List<String> getValidationProblems() {
		List<String> problems = new ArrayList<>();
		if (ymd==null) {
			problems.add("There are no values.");
		}
		return problems;
	}

	
	public static class TimeSeriesBuilder {
		private Map<Integer,Map<Integer,Map<Integer,Double>>> values;
		
		public TimeSeriesBuilder add(Date d,Double v) {
			if (d == null || v == null) {
				// TODO log
				return this;
			}
			Calendar c = Calendar.getInstance();
			c.setTime(d);
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			Map<Integer,Map<Integer,Double>> monthMap = values.get(year);
			if (monthMap == null) {
				monthMap = new HashMap<Integer,Map<Integer,Double>>();
				values.put(year, monthMap);
			}
			Map<Integer,Double> dayMap = monthMap.get(month);
			if (dayMap == null) {
				dayMap = new HashMap<Integer,Double>();
				monthMap.put(month, dayMap);
			}
			dayMap.put(day, v);
			return this;
		}
		
		public void clear() {
			values.clear();
		}
		
		public TimeSeries build() {
			// convert it to arrays.
			int years = values.size();
			int baseYear = Integer.MAX_VALUE;
			for (int year : values.keySet()) {
				if (year < baseYear) {
					baseYear = year;
				}
			}
			double[][][] ymd = new double[years][12][31];
			for (int year : values.keySet()) {
				Map<Integer,Map<Integer,Double>> monthMap = values.get(year);
				for (int month : values.get(year).keySet()) {
					Map<Integer,Double> dayMap = monthMap.get(month);
					for (int day : dayMap.keySet()) {
						ymd[year-baseYear][month][day] = dayMap.get(day);
					}
				}
			}
			return new TimeSeries(baseYear,ymd);
		}
		
	}
}
