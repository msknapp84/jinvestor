package jstock.domain.time;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;

import knapp.common.data.domain.Validatable;

public class ArrayPair extends Validatable {

	private final int[] dates;
	private final double[] values;

	public ArrayPair(int[] dates, double[] values) {
		this.dates = dates;
		this.values = values;
		this.validate();
	}

	public double getValue(Date date) {
		return getValue(dateToInt(date));
	}

	public Double getValue(int date) {
		int index = Arrays.binarySearch(dates, date);
		if (index == -1) {
			return Double.NaN;
		} else {
			return values[index];
		}
	}

	public int getIntValue(Date date) {
		return getIntValue(dateToInt(date));
	}

	public int getIntValue(int date) {
		return (int) Math.round(getValue(date));
	}

	public boolean isSet(Date date) {
		return isSet(dateToInt(date));
	}

	public boolean isSet(int date) {
		return Arrays.binarySearch(dates, date)!=-1;
	}

	private int dateToInt(Date date) {
		date = DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
		Long days = Math.round(Math.floor(new Double(date.getTime())
				/ DateUtils.MILLIS_PER_DAY));
		return days.intValue();
	}
	
	private Date intToDate(int d) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(0);
		return DateUtils.addDays(c.getTime(), d);
	}

	public List<String> getValidationProblems() {
		List<String> problems = new ArrayList<>();
		if (dates == null || values == null) {
			problems.add("Dates or values are null.");
		}
		if (dates.length != values.length) {
			problems.add("The length of the two arrays are not equal.");
		}
		int last = -1;
		for (int i = 0; i < dates.length; i++) {
			int d = dates[i];
			if (d <= last) {
				problems.add("The dates are not in order.");
				break;
			}
		}
		return problems;
	}

	public int[] getDates() {
		return dates;
	}

	public double[] getValues() {
		return values;
	}

}
