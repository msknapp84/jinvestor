package jstock.domain.time;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.Date;

public final class StockTimeUtil {
	
	private static Temporal epoch;
	
	static {
		epoch = new Date(0).toInstant();
	}
	private StockTimeUtil() {
		
	}

	public static int daysSince1970(LocalDate date) {
		return (int) ChronoUnit.DAYS.between(epoch, date);
	}
}
