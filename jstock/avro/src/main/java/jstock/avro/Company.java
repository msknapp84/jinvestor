/**
 * Autogenerated by Avro
 * 
 * DO NOT EDIT DIRECTLY
 */
package jstock.avro;  
@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class Company extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"Company\",\"namespace\":\"jstock.avro\",\"fields\":[{\"name\":\"tickerSymbol\",\"type\":\"string\"},{\"name\":\"sector\",\"type\":[\"null\",\"string\"]},{\"name\":\"description\",\"type\":[\"null\",\"string\"]}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  @Deprecated public java.lang.CharSequence tickerSymbol;
  @Deprecated public java.lang.CharSequence sector;
  @Deprecated public java.lang.CharSequence description;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>. 
   */
  public Company() {}

  /**
   * All-args constructor.
   */
  public Company(java.lang.CharSequence tickerSymbol, java.lang.CharSequence sector, java.lang.CharSequence description) {
    this.tickerSymbol = tickerSymbol;
    this.sector = sector;
    this.description = description;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call. 
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return tickerSymbol;
    case 1: return sector;
    case 2: return description;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }
  // Used by DatumReader.  Applications should not call. 
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: tickerSymbol = (java.lang.CharSequence)value$; break;
    case 1: sector = (java.lang.CharSequence)value$; break;
    case 2: description = (java.lang.CharSequence)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'tickerSymbol' field.
   */
  public java.lang.CharSequence getTickerSymbol() {
    return tickerSymbol;
  }

  /**
   * Sets the value of the 'tickerSymbol' field.
   * @param value the value to set.
   */
  public void setTickerSymbol(java.lang.CharSequence value) {
    this.tickerSymbol = value;
  }

  /**
   * Gets the value of the 'sector' field.
   */
  public java.lang.CharSequence getSector() {
    return sector;
  }

  /**
   * Sets the value of the 'sector' field.
   * @param value the value to set.
   */
  public void setSector(java.lang.CharSequence value) {
    this.sector = value;
  }

  /**
   * Gets the value of the 'description' field.
   */
  public java.lang.CharSequence getDescription() {
    return description;
  }

  /**
   * Sets the value of the 'description' field.
   * @param value the value to set.
   */
  public void setDescription(java.lang.CharSequence value) {
    this.description = value;
  }

  /** Creates a new Company RecordBuilder */
  public static jstock.avro.Company.Builder newBuilder() {
    return new jstock.avro.Company.Builder();
  }
  
  /** Creates a new Company RecordBuilder by copying an existing Builder */
  public static jstock.avro.Company.Builder newBuilder(jstock.avro.Company.Builder other) {
    return new jstock.avro.Company.Builder(other);
  }
  
  /** Creates a new Company RecordBuilder by copying an existing Company instance */
  public static jstock.avro.Company.Builder newBuilder(jstock.avro.Company other) {
    return new jstock.avro.Company.Builder(other);
  }
  
  /**
   * RecordBuilder for Company instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<Company>
    implements org.apache.avro.data.RecordBuilder<Company> {

    private java.lang.CharSequence tickerSymbol;
    private java.lang.CharSequence sector;
    private java.lang.CharSequence description;

    /** Creates a new Builder */
    private Builder() {
      super(jstock.avro.Company.SCHEMA$);
    }
    
    /** Creates a Builder by copying an existing Builder */
    private Builder(jstock.avro.Company.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.tickerSymbol)) {
        this.tickerSymbol = data().deepCopy(fields()[0].schema(), other.tickerSymbol);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.sector)) {
        this.sector = data().deepCopy(fields()[1].schema(), other.sector);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.description)) {
        this.description = data().deepCopy(fields()[2].schema(), other.description);
        fieldSetFlags()[2] = true;
      }
    }
    
    /** Creates a Builder by copying an existing Company instance */
    private Builder(jstock.avro.Company other) {
            super(jstock.avro.Company.SCHEMA$);
      if (isValidValue(fields()[0], other.tickerSymbol)) {
        this.tickerSymbol = data().deepCopy(fields()[0].schema(), other.tickerSymbol);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.sector)) {
        this.sector = data().deepCopy(fields()[1].schema(), other.sector);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.description)) {
        this.description = data().deepCopy(fields()[2].schema(), other.description);
        fieldSetFlags()[2] = true;
      }
    }

    /** Gets the value of the 'tickerSymbol' field */
    public java.lang.CharSequence getTickerSymbol() {
      return tickerSymbol;
    }
    
    /** Sets the value of the 'tickerSymbol' field */
    public jstock.avro.Company.Builder setTickerSymbol(java.lang.CharSequence value) {
      validate(fields()[0], value);
      this.tickerSymbol = value;
      fieldSetFlags()[0] = true;
      return this; 
    }
    
    /** Checks whether the 'tickerSymbol' field has been set */
    public boolean hasTickerSymbol() {
      return fieldSetFlags()[0];
    }
    
    /** Clears the value of the 'tickerSymbol' field */
    public jstock.avro.Company.Builder clearTickerSymbol() {
      tickerSymbol = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /** Gets the value of the 'sector' field */
    public java.lang.CharSequence getSector() {
      return sector;
    }
    
    /** Sets the value of the 'sector' field */
    public jstock.avro.Company.Builder setSector(java.lang.CharSequence value) {
      validate(fields()[1], value);
      this.sector = value;
      fieldSetFlags()[1] = true;
      return this; 
    }
    
    /** Checks whether the 'sector' field has been set */
    public boolean hasSector() {
      return fieldSetFlags()[1];
    }
    
    /** Clears the value of the 'sector' field */
    public jstock.avro.Company.Builder clearSector() {
      sector = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /** Gets the value of the 'description' field */
    public java.lang.CharSequence getDescription() {
      return description;
    }
    
    /** Sets the value of the 'description' field */
    public jstock.avro.Company.Builder setDescription(java.lang.CharSequence value) {
      validate(fields()[2], value);
      this.description = value;
      fieldSetFlags()[2] = true;
      return this; 
    }
    
    /** Checks whether the 'description' field has been set */
    public boolean hasDescription() {
      return fieldSetFlags()[2];
    }
    
    /** Clears the value of the 'description' field */
    public jstock.avro.Company.Builder clearDescription() {
      description = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    @Override
    public Company build() {
      try {
        Company record = new Company();
        record.tickerSymbol = fieldSetFlags()[0] ? this.tickerSymbol : (java.lang.CharSequence) defaultValue(fields()[0]);
        record.sector = fieldSetFlags()[1] ? this.sector : (java.lang.CharSequence) defaultValue(fields()[1]);
        record.description = fieldSetFlags()[2] ? this.description : (java.lang.CharSequence) defaultValue(fields()[2]);
        return record;
      } catch (Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }
}
