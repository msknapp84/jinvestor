/**
 * Autogenerated by Avro
 * 
 * DO NOT EDIT DIRECTLY
 */
package jstock.avro;  
@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class PriceHistory extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"PriceHistory\",\"namespace\":\"jstock.avro\",\"fields\":[{\"name\":\"ticker\",\"type\":\"string\"},{\"name\":\"sector\",\"type\":[\"string\",\"null\"]},{\"name\":\"daysSinceEpoch\",\"type\":\"int\"},{\"name\":\"volume\",\"type\":\"int\"},{\"name\":\"openPennies\",\"type\":\"int\"},{\"name\":\"highPennies\",\"type\":\"int\"},{\"name\":\"lowPennies\",\"type\":\"int\"},{\"name\":\"closePennies\",\"type\":\"int\"},{\"name\":\"adjClosePennies\",\"type\":\"int\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  @Deprecated public java.lang.CharSequence ticker;
  @Deprecated public java.lang.CharSequence sector;
  @Deprecated public int daysSinceEpoch;
  @Deprecated public int volume;
  @Deprecated public int openPennies;
  @Deprecated public int highPennies;
  @Deprecated public int lowPennies;
  @Deprecated public int closePennies;
  @Deprecated public int adjClosePennies;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>. 
   */
  public PriceHistory() {}

  /**
   * All-args constructor.
   */
  public PriceHistory(java.lang.CharSequence ticker, java.lang.CharSequence sector, java.lang.Integer daysSinceEpoch, java.lang.Integer volume, java.lang.Integer openPennies, java.lang.Integer highPennies, java.lang.Integer lowPennies, java.lang.Integer closePennies, java.lang.Integer adjClosePennies) {
    this.ticker = ticker;
    this.sector = sector;
    this.daysSinceEpoch = daysSinceEpoch;
    this.volume = volume;
    this.openPennies = openPennies;
    this.highPennies = highPennies;
    this.lowPennies = lowPennies;
    this.closePennies = closePennies;
    this.adjClosePennies = adjClosePennies;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call. 
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return ticker;
    case 1: return sector;
    case 2: return daysSinceEpoch;
    case 3: return volume;
    case 4: return openPennies;
    case 5: return highPennies;
    case 6: return lowPennies;
    case 7: return closePennies;
    case 8: return adjClosePennies;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }
  // Used by DatumReader.  Applications should not call. 
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: ticker = (java.lang.CharSequence)value$; break;
    case 1: sector = (java.lang.CharSequence)value$; break;
    case 2: daysSinceEpoch = (java.lang.Integer)value$; break;
    case 3: volume = (java.lang.Integer)value$; break;
    case 4: openPennies = (java.lang.Integer)value$; break;
    case 5: highPennies = (java.lang.Integer)value$; break;
    case 6: lowPennies = (java.lang.Integer)value$; break;
    case 7: closePennies = (java.lang.Integer)value$; break;
    case 8: adjClosePennies = (java.lang.Integer)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'ticker' field.
   */
  public java.lang.CharSequence getTicker() {
    return ticker;
  }

  /**
   * Sets the value of the 'ticker' field.
   * @param value the value to set.
   */
  public void setTicker(java.lang.CharSequence value) {
    this.ticker = value;
  }

  /**
   * Gets the value of the 'sector' field.
   */
  public java.lang.CharSequence getSector() {
    return sector;
  }

  /**
   * Sets the value of the 'sector' field.
   * @param value the value to set.
   */
  public void setSector(java.lang.CharSequence value) {
    this.sector = value;
  }

  /**
   * Gets the value of the 'daysSinceEpoch' field.
   */
  public java.lang.Integer getDaysSinceEpoch() {
    return daysSinceEpoch;
  }

  /**
   * Sets the value of the 'daysSinceEpoch' field.
   * @param value the value to set.
   */
  public void setDaysSinceEpoch(java.lang.Integer value) {
    this.daysSinceEpoch = value;
  }

  /**
   * Gets the value of the 'volume' field.
   */
  public java.lang.Integer getVolume() {
    return volume;
  }

  /**
   * Sets the value of the 'volume' field.
   * @param value the value to set.
   */
  public void setVolume(java.lang.Integer value) {
    this.volume = value;
  }

  /**
   * Gets the value of the 'openPennies' field.
   */
  public java.lang.Integer getOpenPennies() {
    return openPennies;
  }

  /**
   * Sets the value of the 'openPennies' field.
   * @param value the value to set.
   */
  public void setOpenPennies(java.lang.Integer value) {
    this.openPennies = value;
  }

  /**
   * Gets the value of the 'highPennies' field.
   */
  public java.lang.Integer getHighPennies() {
    return highPennies;
  }

  /**
   * Sets the value of the 'highPennies' field.
   * @param value the value to set.
   */
  public void setHighPennies(java.lang.Integer value) {
    this.highPennies = value;
  }

  /**
   * Gets the value of the 'lowPennies' field.
   */
  public java.lang.Integer getLowPennies() {
    return lowPennies;
  }

  /**
   * Sets the value of the 'lowPennies' field.
   * @param value the value to set.
   */
  public void setLowPennies(java.lang.Integer value) {
    this.lowPennies = value;
  }

  /**
   * Gets the value of the 'closePennies' field.
   */
  public java.lang.Integer getClosePennies() {
    return closePennies;
  }

  /**
   * Sets the value of the 'closePennies' field.
   * @param value the value to set.
   */
  public void setClosePennies(java.lang.Integer value) {
    this.closePennies = value;
  }

  /**
   * Gets the value of the 'adjClosePennies' field.
   */
  public java.lang.Integer getAdjClosePennies() {
    return adjClosePennies;
  }

  /**
   * Sets the value of the 'adjClosePennies' field.
   * @param value the value to set.
   */
  public void setAdjClosePennies(java.lang.Integer value) {
    this.adjClosePennies = value;
  }

  /** Creates a new PriceHistory RecordBuilder */
  public static jstock.avro.PriceHistory.Builder newBuilder() {
    return new jstock.avro.PriceHistory.Builder();
  }
  
  /** Creates a new PriceHistory RecordBuilder by copying an existing Builder */
  public static jstock.avro.PriceHistory.Builder newBuilder(jstock.avro.PriceHistory.Builder other) {
    return new jstock.avro.PriceHistory.Builder(other);
  }
  
  /** Creates a new PriceHistory RecordBuilder by copying an existing PriceHistory instance */
  public static jstock.avro.PriceHistory.Builder newBuilder(jstock.avro.PriceHistory other) {
    return new jstock.avro.PriceHistory.Builder(other);
  }
  
  /**
   * RecordBuilder for PriceHistory instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<PriceHistory>
    implements org.apache.avro.data.RecordBuilder<PriceHistory> {

    private java.lang.CharSequence ticker;
    private java.lang.CharSequence sector;
    private int daysSinceEpoch;
    private int volume;
    private int openPennies;
    private int highPennies;
    private int lowPennies;
    private int closePennies;
    private int adjClosePennies;

    /** Creates a new Builder */
    private Builder() {
      super(jstock.avro.PriceHistory.SCHEMA$);
    }
    
    /** Creates a Builder by copying an existing Builder */
    private Builder(jstock.avro.PriceHistory.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.ticker)) {
        this.ticker = data().deepCopy(fields()[0].schema(), other.ticker);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.sector)) {
        this.sector = data().deepCopy(fields()[1].schema(), other.sector);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.daysSinceEpoch)) {
        this.daysSinceEpoch = data().deepCopy(fields()[2].schema(), other.daysSinceEpoch);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.volume)) {
        this.volume = data().deepCopy(fields()[3].schema(), other.volume);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.openPennies)) {
        this.openPennies = data().deepCopy(fields()[4].schema(), other.openPennies);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.highPennies)) {
        this.highPennies = data().deepCopy(fields()[5].schema(), other.highPennies);
        fieldSetFlags()[5] = true;
      }
      if (isValidValue(fields()[6], other.lowPennies)) {
        this.lowPennies = data().deepCopy(fields()[6].schema(), other.lowPennies);
        fieldSetFlags()[6] = true;
      }
      if (isValidValue(fields()[7], other.closePennies)) {
        this.closePennies = data().deepCopy(fields()[7].schema(), other.closePennies);
        fieldSetFlags()[7] = true;
      }
      if (isValidValue(fields()[8], other.adjClosePennies)) {
        this.adjClosePennies = data().deepCopy(fields()[8].schema(), other.adjClosePennies);
        fieldSetFlags()[8] = true;
      }
    }
    
    /** Creates a Builder by copying an existing PriceHistory instance */
    private Builder(jstock.avro.PriceHistory other) {
            super(jstock.avro.PriceHistory.SCHEMA$);
      if (isValidValue(fields()[0], other.ticker)) {
        this.ticker = data().deepCopy(fields()[0].schema(), other.ticker);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.sector)) {
        this.sector = data().deepCopy(fields()[1].schema(), other.sector);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.daysSinceEpoch)) {
        this.daysSinceEpoch = data().deepCopy(fields()[2].schema(), other.daysSinceEpoch);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.volume)) {
        this.volume = data().deepCopy(fields()[3].schema(), other.volume);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.openPennies)) {
        this.openPennies = data().deepCopy(fields()[4].schema(), other.openPennies);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.highPennies)) {
        this.highPennies = data().deepCopy(fields()[5].schema(), other.highPennies);
        fieldSetFlags()[5] = true;
      }
      if (isValidValue(fields()[6], other.lowPennies)) {
        this.lowPennies = data().deepCopy(fields()[6].schema(), other.lowPennies);
        fieldSetFlags()[6] = true;
      }
      if (isValidValue(fields()[7], other.closePennies)) {
        this.closePennies = data().deepCopy(fields()[7].schema(), other.closePennies);
        fieldSetFlags()[7] = true;
      }
      if (isValidValue(fields()[8], other.adjClosePennies)) {
        this.adjClosePennies = data().deepCopy(fields()[8].schema(), other.adjClosePennies);
        fieldSetFlags()[8] = true;
      }
    }

    /** Gets the value of the 'ticker' field */
    public java.lang.CharSequence getTicker() {
      return ticker;
    }
    
    /** Sets the value of the 'ticker' field */
    public jstock.avro.PriceHistory.Builder setTicker(java.lang.CharSequence value) {
      validate(fields()[0], value);
      this.ticker = value;
      fieldSetFlags()[0] = true;
      return this; 
    }
    
    /** Checks whether the 'ticker' field has been set */
    public boolean hasTicker() {
      return fieldSetFlags()[0];
    }
    
    /** Clears the value of the 'ticker' field */
    public jstock.avro.PriceHistory.Builder clearTicker() {
      ticker = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /** Gets the value of the 'sector' field */
    public java.lang.CharSequence getSector() {
      return sector;
    }
    
    /** Sets the value of the 'sector' field */
    public jstock.avro.PriceHistory.Builder setSector(java.lang.CharSequence value) {
      validate(fields()[1], value);
      this.sector = value;
      fieldSetFlags()[1] = true;
      return this; 
    }
    
    /** Checks whether the 'sector' field has been set */
    public boolean hasSector() {
      return fieldSetFlags()[1];
    }
    
    /** Clears the value of the 'sector' field */
    public jstock.avro.PriceHistory.Builder clearSector() {
      sector = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /** Gets the value of the 'daysSinceEpoch' field */
    public java.lang.Integer getDaysSinceEpoch() {
      return daysSinceEpoch;
    }
    
    /** Sets the value of the 'daysSinceEpoch' field */
    public jstock.avro.PriceHistory.Builder setDaysSinceEpoch(int value) {
      validate(fields()[2], value);
      this.daysSinceEpoch = value;
      fieldSetFlags()[2] = true;
      return this; 
    }
    
    /** Checks whether the 'daysSinceEpoch' field has been set */
    public boolean hasDaysSinceEpoch() {
      return fieldSetFlags()[2];
    }
    
    /** Clears the value of the 'daysSinceEpoch' field */
    public jstock.avro.PriceHistory.Builder clearDaysSinceEpoch() {
      fieldSetFlags()[2] = false;
      return this;
    }

    /** Gets the value of the 'volume' field */
    public java.lang.Integer getVolume() {
      return volume;
    }
    
    /** Sets the value of the 'volume' field */
    public jstock.avro.PriceHistory.Builder setVolume(int value) {
      validate(fields()[3], value);
      this.volume = value;
      fieldSetFlags()[3] = true;
      return this; 
    }
    
    /** Checks whether the 'volume' field has been set */
    public boolean hasVolume() {
      return fieldSetFlags()[3];
    }
    
    /** Clears the value of the 'volume' field */
    public jstock.avro.PriceHistory.Builder clearVolume() {
      fieldSetFlags()[3] = false;
      return this;
    }

    /** Gets the value of the 'openPennies' field */
    public java.lang.Integer getOpenPennies() {
      return openPennies;
    }
    
    /** Sets the value of the 'openPennies' field */
    public jstock.avro.PriceHistory.Builder setOpenPennies(int value) {
      validate(fields()[4], value);
      this.openPennies = value;
      fieldSetFlags()[4] = true;
      return this; 
    }
    
    /** Checks whether the 'openPennies' field has been set */
    public boolean hasOpenPennies() {
      return fieldSetFlags()[4];
    }
    
    /** Clears the value of the 'openPennies' field */
    public jstock.avro.PriceHistory.Builder clearOpenPennies() {
      fieldSetFlags()[4] = false;
      return this;
    }

    /** Gets the value of the 'highPennies' field */
    public java.lang.Integer getHighPennies() {
      return highPennies;
    }
    
    /** Sets the value of the 'highPennies' field */
    public jstock.avro.PriceHistory.Builder setHighPennies(int value) {
      validate(fields()[5], value);
      this.highPennies = value;
      fieldSetFlags()[5] = true;
      return this; 
    }
    
    /** Checks whether the 'highPennies' field has been set */
    public boolean hasHighPennies() {
      return fieldSetFlags()[5];
    }
    
    /** Clears the value of the 'highPennies' field */
    public jstock.avro.PriceHistory.Builder clearHighPennies() {
      fieldSetFlags()[5] = false;
      return this;
    }

    /** Gets the value of the 'lowPennies' field */
    public java.lang.Integer getLowPennies() {
      return lowPennies;
    }
    
    /** Sets the value of the 'lowPennies' field */
    public jstock.avro.PriceHistory.Builder setLowPennies(int value) {
      validate(fields()[6], value);
      this.lowPennies = value;
      fieldSetFlags()[6] = true;
      return this; 
    }
    
    /** Checks whether the 'lowPennies' field has been set */
    public boolean hasLowPennies() {
      return fieldSetFlags()[6];
    }
    
    /** Clears the value of the 'lowPennies' field */
    public jstock.avro.PriceHistory.Builder clearLowPennies() {
      fieldSetFlags()[6] = false;
      return this;
    }

    /** Gets the value of the 'closePennies' field */
    public java.lang.Integer getClosePennies() {
      return closePennies;
    }
    
    /** Sets the value of the 'closePennies' field */
    public jstock.avro.PriceHistory.Builder setClosePennies(int value) {
      validate(fields()[7], value);
      this.closePennies = value;
      fieldSetFlags()[7] = true;
      return this; 
    }
    
    /** Checks whether the 'closePennies' field has been set */
    public boolean hasClosePennies() {
      return fieldSetFlags()[7];
    }
    
    /** Clears the value of the 'closePennies' field */
    public jstock.avro.PriceHistory.Builder clearClosePennies() {
      fieldSetFlags()[7] = false;
      return this;
    }

    /** Gets the value of the 'adjClosePennies' field */
    public java.lang.Integer getAdjClosePennies() {
      return adjClosePennies;
    }
    
    /** Sets the value of the 'adjClosePennies' field */
    public jstock.avro.PriceHistory.Builder setAdjClosePennies(int value) {
      validate(fields()[8], value);
      this.adjClosePennies = value;
      fieldSetFlags()[8] = true;
      return this; 
    }
    
    /** Checks whether the 'adjClosePennies' field has been set */
    public boolean hasAdjClosePennies() {
      return fieldSetFlags()[8];
    }
    
    /** Clears the value of the 'adjClosePennies' field */
    public jstock.avro.PriceHistory.Builder clearAdjClosePennies() {
      fieldSetFlags()[8] = false;
      return this;
    }

    @Override
    public PriceHistory build() {
      try {
        PriceHistory record = new PriceHistory();
        record.ticker = fieldSetFlags()[0] ? this.ticker : (java.lang.CharSequence) defaultValue(fields()[0]);
        record.sector = fieldSetFlags()[1] ? this.sector : (java.lang.CharSequence) defaultValue(fields()[1]);
        record.daysSinceEpoch = fieldSetFlags()[2] ? this.daysSinceEpoch : (java.lang.Integer) defaultValue(fields()[2]);
        record.volume = fieldSetFlags()[3] ? this.volume : (java.lang.Integer) defaultValue(fields()[3]);
        record.openPennies = fieldSetFlags()[4] ? this.openPennies : (java.lang.Integer) defaultValue(fields()[4]);
        record.highPennies = fieldSetFlags()[5] ? this.highPennies : (java.lang.Integer) defaultValue(fields()[5]);
        record.lowPennies = fieldSetFlags()[6] ? this.lowPennies : (java.lang.Integer) defaultValue(fields()[6]);
        record.closePennies = fieldSetFlags()[7] ? this.closePennies : (java.lang.Integer) defaultValue(fields()[7]);
        record.adjClosePennies = fieldSetFlags()[8] ? this.adjClosePennies : (java.lang.Integer) defaultValue(fields()[8]);
        return record;
      } catch (Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }
}
