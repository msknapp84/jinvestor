package jstock.parser.fh;

import java.io.InputStream;
import java.util.Map;

import org.junit.Test;
import static org.junit.Assert.*;

import jstock.dao.FinancialLabelMemDAO;
import jstock.domain.fh.FinancialTable;
import knapp.common.data.exceptions.DataException;

public class FinancialTableParserTest {

	@Test
	public void testParse() throws DataException {
		InputStream in = FinancialTableParser.class
				.getResourceAsStream("/facebook_cashflow_sample.html");
		final FinancialLabelMemDAO flmd = new FinancialLabelMemDAO();
		FinancialTable table = new FinancialTableParser(flmd).apply(in);
		table.setLabelNameResolver((i)->{
			String s = null;
			try {
				s = flmd.getNameForOrdinal(i);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return s;
		});
		table.setLabelOrdinalResolver(name->{
			int i = 0;
			if (name == null) {
				return -1;
			}
			try {
				Integer j = flmd.getOrdinalForName(name);
				i = j!=null ? j : -1;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return i;
		});
//		System.out.println(table.toPrintString());
		Map<Integer,String> labels = flmd.loadLabels();
//		System.out.println("Here are the labels:");
//		for (Integer lbl : labels.keySet()) {
//			System.out.format("%d\t%s%n",lbl,labels.get(lbl));
//		}
		assertEquals(264000000,table.get("Depreciation", "2014-03-31", false),1e-6);
		assertEquals(433000000,table.get("Depreciation", "2014-12-31", false),1e-6);
		assertEquals(-3000000,table.get("Effect Of Exchange Rate Changes", "2014-06-30", false),1e-6);
		assertEquals(-68000000,table.get("Effect Of Exchange Rate Changes", "2014-09-30", false),1e-6);
		assertEquals(-52000000,table.get("Effect Of Exchange Rate Changes", "2014-12-31", false),1e-6);
		assertEquals(215000000,table.get("Changes In Liabilities", "2014-09-30", false),1e-6);
		assertEquals(-70000000,table.get("Other Cash Flows from Financing Activities", "2014-12-31", false),1e-6);
		assertEquals(-3000000,table.get("Other Cash Flows from Financing Activities", "2014-03-31", false),1e-6);
		assertEquals(table.getUnsetValue(),table.get("Effect Of Exchange Rate Changes", "2014-03-31", false),1e-9);
	}
}
