package jstock.parser.er;

import java.io.InputStream;
import java.time.temporal.ChronoUnit;

import jstock.domain.er.EconomicRecord;

import org.junit.Test;

import static org.junit.Assert.*;

public class SLFTextParserTest {
	
	@Test
	public void testParse() {
		InputStream in = SLFTextParserTest.class.getResourceAsStream("/slf_cpi_sample.txt");
		SLFTextParser parser = new SLFTextParser();
		EconomicRecord er = parser.apply(in);
		assertNotNull(er);
		assertEquals(ChronoUnit.MONTHS,er.getFrequency());
		assertEquals(1,er.getPeriodicity());
		assertEquals("Consumer Price Index",er.getRelease());
		assertTrue(er.isSeasonallyAdjusted());
		assertEquals("CPIAUCSL",er.getSeriesId());
		assertEquals("US. Bureau of Labor Statistics",er.getSource());
		assertEquals("Consumer Price Index for All Urban Consumers: All Items",er.getTitle());
		assertEquals("Index 1982-84=100",er.getUnits());
		assertEquals(37.9,er.getValue("1970-01-01"),1e-3);
		assertEquals(38.1,er.getValue("1970-02-01"),1e-3);
		assertEquals(140.5,er.getValue("1992-07-01"),1e-3);
		assertEquals(140.5,er.getValue("1992-07-02",true),1e-3);
		assertEquals(140.5,er.getValue("1992-07-30",true),1e-3);
		assertEquals(217.251,er.getValue("2010-02-01"),1e-3);
		assertEquals(236.149,er.getValue("2014-12-01"),1e-3);
	}

}
