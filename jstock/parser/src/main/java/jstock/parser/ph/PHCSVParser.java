package jstock.parser.ph;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.BiFunction;

import jstock.domain.ph.PHTablet;
import jstock.domain.ph.PHTablet.PHTabletBuilder;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PHCSVParser implements BiFunction<String, InputStream, PHTablet> {
	private static final Logger logger = LoggerFactory
			.getLogger(PHCSVParser.class);

	private static final DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE;
//			.ofPattern("MMM dd, yyyy");

	public PHTablet parse(String tickerSymbol, InputStream in) {
		PHTabletBuilder tabletBuilder = null;
		try {
			List<String> lines = IOUtils.readLines(in);
			// must subtract one line for the header.
			tabletBuilder = PHTablet.start(tickerSymbol, lines.size()-1);
			// the first line is the header
			for (String line : lines.subList(1, lines.size())) {
				try {
					// fortunately I know that there are no quotes in this input
					// I remove all periods because it's an easy way to convert the
					// dollars into pennies.
					String[] parts = line.replaceAll("\\.", "").split(",");
					// Date Open High Low Close Volume Adj Close*
					// date is like: Jan 30, 2015
					LocalDate ld = LocalDate.parse(parts[0], dtf);
					tabletBuilder.add(ld, Integer.parseInt(parts[1]),
							Integer.parseInt(parts[2]), Integer.parseInt(parts[3]),
							Integer.parseInt(parts[4]), Integer.parseInt(parts[5]),
							Integer.parseInt(parts[6]));
				} catch (Exception e) {
					// don't stop, just skip lines that fail.
					logger.error("Failed to parse this line: "+line);
				}
			}
		} catch (IOException e) {
			logger.error("Failed to parse a PH tablet.", e);
		}

		return tabletBuilder.build();
	}

	@Override
	public PHTablet apply(String t, InputStream u) {
		return parse(t, u);
	}
}
