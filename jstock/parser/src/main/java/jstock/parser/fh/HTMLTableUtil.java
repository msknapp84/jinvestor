package jstock.parser.fh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTMLTableUtil {

	private HTMLTableUtil() {
		
	}
	
	public static String getTableId(String html,HtmlTable table) {
		String t = html.substring(table.getStart(),table.getCloseStart());
		String regex = ".*\\bid=['\"]([\\w_\\s]+)['\"].*";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(t);
		if (matcher.matches()) {
			return matcher.group(1);
		}
		return null;
	}
	
	public static String[] getTableClasses(String html,HtmlTable table) {
		String t = html.substring(table.getStart(),table.getCloseStart());
		String regex = ".*\\bclass=['\"]([\\w\\d_\\s]+)['\"].*";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(t);
		if (matcher.matches()) {
			return matcher.group(1).split("\\s+");
		}
		return null;
	}
	
	public static Map<String,HtmlTable> indexHtmlTablesById(List<HtmlTable> tables) {
		Map<String,HtmlTable> indexed = new HashMap<>();
		tables.stream().filter(t->t.getId()!=null).forEach(t->indexed.put(t.getId(),t));
		return indexed;
	}

	public static Map<String,List<HtmlTable>> indexHtmlTablesByClasses(List<HtmlTable> tables) {
		Map<String,List<HtmlTable>> indexed = new HashMap<>();
		for (HtmlTable t : tables) {
			if (t.getClasses() == null) {
				continue;
			}
			for (String clz : t.getClasses()) {
				List<HtmlTable> lst = indexed.get(clz);
				if (lst == null) {
					lst = new ArrayList<>();
					indexed.put(clz,lst);
				}
				lst.add(t);
			}
		}
		return indexed;
	}

	public static List<HtmlTable> findAllLevelTables(String html) {
		List<HtmlTable> tables = findTopLevelTables(html);
		return findAllLevelTables(tables);
	}
	
	public static List<HtmlTable> findAllLevelTables(List<HtmlTable> topLevels) {
		List<HtmlTable> allLevels = new ArrayList<>();
		for (HtmlTable t : topLevels) {
			allLevels.addAll(getAllDescendents(t));
			allLevels.add(t);
		}
		return allLevels;
	}
	
	private static List<HtmlTable> getAllDescendents(HtmlTable table) {
		List<HtmlTable> desc = new ArrayList<>();
		for (HtmlTable t : table.children) {
			desc.addAll(getAllDescendents(t));
			desc.add(t);
		}
		return desc;
	}
	
	public static List<HtmlTable> findTopLevelTables(String html) {
		int[] openIndices = getTableStartIndices(html);
		int[] closeIndices = getTableEndIndices(html);
		List<IndexEvent> events = new ArrayList<>();
		for (Integer i : openIndices) {
			events.add(new IndexEvent(i,true));
		}
		for (Integer i : closeIndices) {
			events.add(new IndexEvent(i,false));
		}
		Collections.sort(events);
		// now they to in order.
		int depth = 0;
		List<HtmlTable> topLevels = new ArrayList<>();
		HtmlTable current = null;
		for (IndexEvent event : events) {
			if (event.open) {
				HtmlTable table = new HtmlTable();
				table.depth=depth;
				table.start = event.index;
				table.setCloseStart(html.indexOf('>', table.start));
				table.setId(getTableId(html,table));
				table.setClasses(getTableClasses(html, table));
				if (current!=null) {
					current.children.add(table);
					table.parent = current;
				} else {
					topLevels.add(table);
				}
				current = table;
				depth++;
			} else {
				current.end = event.index+8;
				current = current.parent;
			}
		}
		return topLevels;
	}
	
	private static class IndexEvent implements Comparable<IndexEvent> {
		int index;
		boolean open;
		public IndexEvent() {
		}
		public IndexEvent(int index,boolean open) {
			this.index = index;
			this.open = open;
		}
		@Override
		public int compareTo(IndexEvent o) {
			return index - o.index;
		}
	}
	
	public static int[] getIndices(String html,String search,boolean end) {
		Pattern p = Pattern.compile(search);
		Matcher m = p.matcher(html);
		List<Integer> indices = new ArrayList<>();
		while (m.find()) {
			indices.add(end ? m.end() : m.start());
		}
		int[] x = new int[indices.size()];
		for (int i = 0;i<x.length;i++) {
			x[i] = indices.get(i);
		}
		return x;
	}
	
	public static int[] getTableStartIndices(String html) {
		return getIndices(html, "(?i)<table", false);
	}
	
	public static int[] getTableEndIndices(String html) {
		return getIndices(html, "(?i)</table>", false);
	}
}