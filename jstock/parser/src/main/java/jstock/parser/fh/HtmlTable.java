package jstock.parser.fh;

import java.util.ArrayList;
import java.util.List;

public class HtmlTable {
	HtmlTable parent = null;
	int start,end,depth;
	private String id;
	private String[] classes;
	private int closeStart;
	List<HtmlTable> children = new ArrayList<>();
	public HtmlTable getParent() {
		return parent;
	}
	public void setParent(HtmlTable parent) {
		this.parent = parent;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	public List<HtmlTable> getChildren() {
		return children;
	}
	public void setChildren(List<HtmlTable> children) {
		this.children = children;
	}
	public int getCloseStart() {
		return closeStart;
	}
	public void setCloseStart(int closeStart) {
		this.closeStart = closeStart;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String[] getClasses() {
		return classes;
	}
	public void setClasses(String[] classes) {
		this.classes = classes;
	}
}
