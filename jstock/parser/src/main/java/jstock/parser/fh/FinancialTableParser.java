package jstock.parser.fh;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import jstock.dao.FinancialLabelDAO;
import jstock.domain.fh.FinancialLabel;
import jstock.domain.fh.FinancialTable;
import jstock.domain.fh.StatementType;
import knapp.common.collections.time.DoubleTable;
import knapp.common.collections.time.DoubleTableImmutable;
import knapp.common.data.exceptions.DataException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class FinancialTableParser implements
		Function<InputStream, FinancialTable> {
	private static String MAIN_TABLE_CLASS = "yfnc_tabledata1";

	private FinancialLabelDAO financialLabelDAO;

	private double unsetValue = DoubleTableImmutable.SUGGESTED_UNSET_VALUE;

	public FinancialTableParser(FinancialLabelDAO financialLabelDAO) {
		this.financialLabelDAO = financialLabelDAO;
	}

	@Override
	public FinancialTable apply(InputStream t) {
		DoubleTable data = new DoubleTable();
		String ticker = null;
		StatementType type = null;
		boolean annual = false;
		String html = null;
		try {
			html = IOUtils.toString(t);
			List<HtmlTable> tables = HTMLTableUtil.findAllLevelTables(html);
			// which one has the data that I need?
			// it is the only child of the table with class 'yfnc_tabledata1'
			Map<String, List<HtmlTable>> indexedByClass = HTMLTableUtil
					.indexHtmlTablesByClasses(tables);
			ticker = findTicker(html, tables, indexedByClass);
			type = findType(html, tables, indexedByClass);
			int multiplier = findMultiplier(html, tables, indexedByClass);
			if (!indexedByClass.containsKey(MAIN_TABLE_CLASS)) {
				int i = 0;
				System.out
						.println(String
								.format("For the ticker %s, statement %s, "
										+ "there is no main table.  Here is what we do have: ",
										ticker, type.name()));
				for (HtmlTable tbl : tables) {
					if (tbl.getClasses() != null && tbl.getClasses().length > 0) {
						System.out.println(String.format(
								"These classes on tbl %d: %s", i, Arrays
										.asList(tbl.getClasses()).stream()
										.collect(Collectors.joining(","))));
					}
					if (tbl.getId() != null) {
						System.out.println(String.format(
								"This id on tbl %d: %s", i, tbl.getId()));
					}
					i++;
				}
				System.out.println("The financial table for "+ticker+" could not be parsed.");
				save(html, ticker);
				return null;
			}
			annual = extractData(data, html, indexedByClass, multiplier);
		} catch (IOException e) {
			save(html, ticker);
			e.printStackTrace();
		}
		FinancialTable financialTable = new FinancialTable(data, ticker, type,
				annual);
		return financialTable;
	}

	private void save(String html, String ticker) {
		File file = new File("src/test/resources/" + ticker + ".html");
		if (file.exists()) {
			file.delete();
		}
		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		try {
			FileUtils.writeStringToFile(file, html);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private int findMultiplier(String html, List<HtmlTable> tables,
			Map<String, List<HtmlTable>> indexedByClass) {
		String regex = "(?i)all numbers in (\\w+)";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(html);
		String t = "ones";
		if (m.find()) {
			t = m.group(1);
		}
		if ("ones".equals(t)) {
			return 1;
		} else if ("tens".equals(t)) {
			return 10;
		} else if ("hundreds".equals(t)) {
			return 100;
		} else if ("thousands".equals(t)) {
			return (int) 1E3;
		} else if ("millions".equals(t)) {
			return (int) 1e6;
		} else if ("billions".equals(t)) {
			return (int) 1e9;
		}
		return 1;
	}

	private StatementType findType(String html, List<HtmlTable> tables,
			Map<String, List<HtmlTable>> indexedByClass) {
		// there is one table that has the answer.
		String clz = "yfnc_modtitle1";
		List<HtmlTable> ts = indexedByClass.get(clz);
		// hopefully only one table has that class name.
		HtmlTable t = ts.get(0);
		String text = html.substring(t.getStart(), t.getEnd());
		if (text.contains("Cash Flow")) {
			return StatementType.CASHFLOW;
		} else if (text.contains("Income Statement")) {
			return StatementType.INCOME;
		}
		return StatementType.BALANCE;
	}

	private String findTicker(String html, List<HtmlTable> tables,
			Map<String, List<HtmlTable>> indexedByClass) {
		// they keep it in an h2 tag
		String regex = "<h2>.*?\\((.*?)\\).*?</h2>";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(html);
		String t = null;
		while (matcher.find()) {
			t = matcher.group(1);
			// there may be multiple of them.

			System.out.println("the ticker we found is '" + t + "'");
		}
		return t;
	}

	private boolean extractData(DoubleTable data, String html,
			Map<String, List<HtmlTable>> indexedByClass, final int multiplier) {
		List<HtmlTable> yfnc_tabledata1 = indexedByClass.get(MAIN_TABLE_CLASS);
		if (yfnc_tabledata1 == null) {
			throw new IllegalArgumentException("Missing: " + MAIN_TABLE_CLASS);
		}
		// System.out.println(yfnc_tabledata1.size());
		HtmlTable myTable = yfnc_tabledata1.get(0).getChildren().get(0);
		String text = html.substring(myTable.getStart(), myTable.getEnd());
		LocalDate[] dates = new LocalDate[4];
		doWithTd(text, (x) -> {
			// the first row has the dates, it is sort of special in that it
			// does not contain numbers.
				if ("Period Ending".equals(x.currentLabel)) {
					x.datesFound=true;
					if (x.td.matches("[A-Z][a-z]{2} \\d{1,2}, \\d{4}")) {
						// this is another date.
						LocalDate d = LocalDate.parse(x.td,
								DateTimeFormatter.ofPattern("MMM d, yyyy"));
						dates[x.dateIndex++] = d;
					} else {
						x.currentLabel = x.td;
						x.dateIndex = 0;
					}
				} else {
					if (x.datesFound && isNumeric(x.td)) {
						// another row for the current label
						LocalDate date = dates[x.dateIndex];
						if (date == null) {
							save(text,"bad_table");
							throw new IllegalStateException(
									"The dates should be the first "
											+ "thing to get set. Current label: "
											+ x.currentLabel);
						}
						int label = getOrCreateLabelOrdinal(x.currentLabel);
						double value = parseValue(x.td);
						if (Math.abs(value - unsetValue) > 1e-9) {
							// the value is truly set,
							// multiply by the factor
							value = multiplier * value;
						}
						data.add(date, label, value);
						x.dateIndex++;
					} else {
						// a new row.
						x.currentLabel = x.td;
						x.dateIndex = 0;
						if ("Period Ending".equals(x.currentLabel)) {
							System.out.println("We found The period ending row");
						} else {
							System.out.println("We found a label: "+x.currentLabel);
						}
					}
				}
				// System.out.println("here is our td: " + x.td);
			});
		// determine if it's annual
		// sometimes yahoo has no data, and these dates are null.
		long m = 3;
		if (dates != null && dates.length > 1 && dates[0] != null
				&& dates[1] != null) {
			m = ChronoUnit.MONTHS.between(dates[0], dates[1]);
		}
		// if there are more than 6 months between them, it's annual,
		// less and it's quarterly.
		return Math.abs(m) > 6;
	}

	private static class TD {
		String td;
		String currentLabel;
		int dateIndex;
		boolean datesFound = false;
	}

	public static void doWithTd(String text, Consumer<TD> callback) {
		// sometimes there are th cells that hold 
		// the dates that I need
		String regex = "(?i)<t[dh].*?>(.*?)</t[dh]>";
		Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
		Matcher matcher = pattern.matcher(text);
		TD x = new TD();
		while (matcher.find()) {
			String td = matcher.group(1);
			td = cleanTd(td);
			if (td.isEmpty()) {
				continue;
			}
			x.td = td;
			callback.accept(x);
		}
	}

	private int getOrCreateLabelOrdinal(String currentLabel) {
		int label = 0;
		if (this.financialLabelDAO != null) {
			try {
				Integer found = financialLabelDAO
						.getOrdinalForName(currentLabel);
				if (found == null || found < 0) {
					int count = financialLabelDAO.countAll();
					FinancialLabel nl = new FinancialLabel(currentLabel, count);
					financialLabelDAO.save(nl);
					found = count;
				}
				label = found;
			} catch (DataException e) {
				e.printStackTrace();
			}
		}
		return label;
	}

	private double parseValue(String td) {
		if (td.equals("-")) {
			return unsetValue;
		}
		td = td.replaceAll(",", "");
		boolean negative = false;
		if (td.contains("(")) {
			negative = true;
			td = td.replaceAll("\\(", "").replaceAll("\\)", "");
		}
		td = td.trim();
		if (td.startsWith("-")) {
			negative = true;
			td = td.substring(1);
		}
		return (negative ? -1 : 1) * Double.parseDouble(td);
	}

	private static boolean isNumeric(String td) {
		return td.matches("[\\(\\)\\d,\\-]*") && td.length() > 0;
	}

	public static String cleanTd(String td) {
		td = td.replaceAll("\r", "").replaceAll("\n", "")
				.replaceAll("\\&nbsp;", "").replaceAll("<strong>", "")
				.replaceAll("</strong>", "").replaceAll("<b>", "")
				.replaceAll("</b>", "").replaceAll("<small>", "")
				.replaceAll("</small>", "").replaceAll("</strong>", "");
		td = td.replaceAll("<span.*?>", "").replaceAll("</span>", "");
		td = td.trim();
		return td;
	}
}
