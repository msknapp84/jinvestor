package jstock.parser.company;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import org.apache.commons.io.IOUtils;

import jstock.domain.Company;
import knapp.common.util.ByteArray;
import knapp.common.util.StringHelp;

public class CompanyParser implements Function<InputStream,Collection<Company>> {

	@Override
	public Collection<Company> apply(InputStream t) {
		// I'm going to cheat here and do things the easy way.
		List<Company> companies = new ArrayList<>();
		try {
			List<String> lines = IOUtils.readLines(t);
			boolean first = true;
			for (String line : lines) {
				if (first) {
					// skip first since it's the header.
					first=false;
					continue;
				}
				// there may be quotes
				List<String> parts = StringHelp.splitConsideringQuotes(line, ',');
//				"Symbol","Name","LastSale","MarketCap","IPOyear","Sector","industry","Summary Quote",
				
				try {
					Company c = new Company();
					c.setTicker(parts.get(0).replaceAll("\"", "").trim());
					c.setName(parts.get(1).replaceAll("\"", "").trim());
					c.setSector(parts.get(5).replaceAll("\"", "").trim());
					c.setIndustry(parts.get(6).replaceAll("\"", "").trim());
					c.setId(new ByteArray(c.getTicker()));
					String s = parts.get(4).replaceAll("\"", "").trim();
					if (!"n/a".equals(s)) {
						try {
							Integer year = Integer.parseInt(s);
							c.setIpoYear(year);
						} catch (Exception e) {
							System.out.println("Failed to parse the input year: "+s);
						}
					}
					companies.add(c);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return companies;
	}

}
