package jstock.parser.er;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import jstock.domain.er.EconomicRecord;
import knapp.common.collections.time.DoublesOverTime;
import knapp.common.collections.time.DoublesOverTimeImmutable;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

public class SLFTextParser implements
		java.util.function.Function<InputStream, EconomicRecord> {

	// getting data from SLF is super easy:
	// http://research.stlouisfed.org/fred2/data/<series_id>.txt

	/**
	 * This information is available on SLF Title: Consumer Price Index for All
	 * Urban Consumers: All Items Series ID: CPIAUCSL Source: US. Bureau of
	 * Labor Statistics Release: Consumer Price Index Seasonal Adjustment:
	 * Seasonally Adjusted Frequency: Monthly Units: Index 1982-84=100 ... notes
	 * and all ...
	 * 
	 * DATE VALUE 1947-01-01 21.480 1947-02-01 21.620 1947-03-01 22.000
	 */

	@Override
	public EconomicRecord apply(InputStream t) {
		EconomicRecord economicRecord = new EconomicRecord();
		DoublesOverTime values = new DoublesOverTime();
		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(t))) {
			String line = null;
			boolean inData = false;
			while ((line = reader.readLine()) != null) {
				if (inData) {
					String[] parts = line.split("\\s+");
					LocalDate date = LocalDate.parse(parts[0],
							DateTimeFormatter.ISO_DATE);
					if (date.getYear()<DoublesOverTime.EPOCH_YEAR) {
						// don't need to record dates this far in the past.
						continue;
					}
					try {
						double value = Double.parseDouble(parts[1]);
						values.add(date, value);
					} catch (Exception e) {
						
					}
				} else {
					// in the header.
					if (line.startsWith("Title")) {
						economicRecord.setTitle(StringUtils.substringAfter(
								line, ":").trim());
					} else if (line.startsWith("Series ID")) {
						economicRecord.setSeriesId(StringUtils.substringAfter(
								line, ":").trim());
					} else if (line.startsWith("Source")) {
						economicRecord.setSource(StringUtils.substringAfter(
								line, ":").trim());
					} else if (line.startsWith("Release")) {
						economicRecord.setRelease(StringUtils.substringAfter(
								line, ":").trim());
					} else if (line.startsWith("Seasonal Adjustment")) {
						economicRecord
								.setSeasonallyAdjusted("Seasonally Adjusted"
										.equals(StringUtils.substringAfter(
												line, ":").trim()));
					} else if (line.startsWith("Frequency")) {
						String f = StringUtils.substringAfter(line, ":").trim();
						ERFrequency fr = parseChronoUnit(f);
						economicRecord.setFrequency(fr.unit);
						economicRecord.setPeriodicity(fr.periodicity);
					} else if (line.startsWith("Units")) {
						economicRecord.setUnits(StringUtils.substringAfter(
								line, ":").trim());
					} else if (line.matches("DATE\\s+VALUE.*")) {
						inData = true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(t);
		}
		DoublesOverTimeImmutable doti = new DoublesOverTimeImmutable(values);
		economicRecord.setValues(doti);
		return economicRecord;
	}
	
	private static class ERFrequency{
		ChronoUnit unit;
		int periodicity=1;
	}

	private ERFrequency parseChronoUnit(String f) {
		ERFrequency erf = new ERFrequency();
		if ("Monthly".equals(f)) {
			erf.unit =  ChronoUnit.MONTHS;
		} else if ("Biweekly".equals(f)) {
			erf.unit =  ChronoUnit.WEEKS;
			erf.periodicity=2;
		} else if ("Weekly".equals(f)) {
			erf.unit =  ChronoUnit.WEEKS;
		} else if ("Daily".equals(f)) {
			erf.unit =  ChronoUnit.DAYS;
		} else if ("Annually".equals(f)) {
			erf.unit =  ChronoUnit.YEARS;
		} else if ("Yearly".equals(f)) {
			erf.unit =  ChronoUnit.YEARS;
		} else if ("Quarterly".equals(f)) {
			erf.unit =  ChronoUnit.MONTHS;
			erf.periodicity=3;
		} else if ("SemiAnnually".equals(f)) {
			erf.unit =  ChronoUnit.MONTHS;
			erf.periodicity=6;
		} else {
			System.out.println("unknown chrono unit: "+f);
		}
		return erf;
	}
}
