package stock.download.financial;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.List;

import jstock.dao.FinancialDAO;
import jstock.domain.fh.Financial;
import jstock.domain.fh.FinancialTable;
import jstock.domain.fh.StatementType;
import jstock.download.di.JStockDownloadContext;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=JStockDownloadContext.class)
@ActiveProfiles("accumulo")
@Ignore("This test class connects to the internet so it's unreliable and inconsistent.")
public class FHDownloaderTest {

	@Autowired
	private FHDownloader downloader;
	
	@Autowired
	private FinancialDAO financialDAO;
	
//	@Test
	public void testDownloadAFew() throws DataException {
//		FinancialSource source = new FinancialSource();
		List<String> source = Arrays.asList("SLRA-ba","APT-iq","JONE-ia","CTSO-cq","PLUG-bq","IBIO-ca");
		int i = 0;
		for (String rowId : source) {
			testDownloading(rowId);
			// I trust the values are set at this point.
			// since I have separate tests for the parser and 
			// the DoublesTableImmutable.
			i++;
			if (i==3) {
				break;
				// you passed the test.
			}
		}
	}
	
	@Test
	public void testDownload_SLRA_ba() throws DataException {
		testDownloading("SLRA-ba");
	}
	
	@Test
	public void testDownload_PLUG_bq() throws DataException {
		testDownloading("PLUG-bq");
	}
	
	@Test
	public void testDownload_IBIO_ca() throws DataException {
		testDownloading("IBIO-ca");
	}
	
	@Test
	public void testDownload_CTSO_cq() throws DataException {
		testDownloading("CTSO-cq");
	}
	
	@Test
	public void testDownload_NILE_ia() throws DataException {
		testDownloading("NILE-ia");
	}
	
	@Test
	public void testDownload_APT_iq() throws DataException {
		testDownloading("APT-iq");
	}

	private void testDownloading(String rowId) throws DataException {
		System.out.println("Working on "+rowId);
		String ticker = FinancialTable.determineTicker(rowId);
		StatementType type = FinancialTable.determineType(rowId);
		boolean shouldBeAnnual = FinancialTable.determineAnnual(rowId);
		
		Financial financial = financialDAO.get(new ByteArray(ticker));
		assertNull(financial);
		downloader.accept(rowId);
		
		financial = financialDAO.get(new ByteArray(ticker));
		assertNotNull(financial);
		assertEquals(ticker,financial.getTicker());
		FinancialTable table = financial.getTable(type,shouldBeAnnual);
		assertEquals(ticker,table.getTicker());
		assertEquals(shouldBeAnnual,table.isAnnual());
		assertEquals(type,table.getType());
		assertTrue(table.getSetCellsSize()>6);
		assertTrue(table.getLabelsSize()>6);
		assertTrue(table.getDatesSize()>2);
	}

}
