package stock.download;

import jstock.dao.DownloadDAO;
import jstock.dao.PriceHistoryDAO;
import jstock.domain.ph.PHRowMutable;
import jstock.domain.ph.PHTablet;
import jstock.download.di.JStockDownloadContext;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import stock.download.ph.PHDownloader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={JStockDownloadContext.class})
//@ActiveProfiles(value={"accumulo","test_integration"})
@ActiveProfiles(value={"accumulo"})
public class DownloadTest {

	@Autowired
	private PHDownloader downloader;
	
	@Autowired
	private PriceHistoryDAO priceHistoryDAO;
	
	@Autowired
	private DownloadDAO downloadDAO;
	
	@Test
	public void testDownload() throws DataException {
		downloader.accept("GE");
		PHTablet ge = priceHistoryDAO.get(new ByteArray("GE"));
		Assert.assertNotNull(ge);
		int count = 0;
		for (PHRowMutable r : ge) {
			count++;
		}
		Assert.assertTrue(count>100);
	}
}