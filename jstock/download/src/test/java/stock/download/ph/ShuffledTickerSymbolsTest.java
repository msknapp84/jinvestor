package stock.download.ph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class ShuffledTickerSymbolsTest {
	
	@Test
	public void testShuffling() {
		ShuffledTickerSymbols shuffledTickerSymbols = new ShuffledTickerSymbols();
		List<String> found = new ArrayList<String>(20);
		for (int i = 0;i<20;i++) {
			String t = shuffledTickerSymbols.get();
			Assert.assertFalse(t.contains("\""));
			Assert.assertFalse(t.contains(" "));
			Assert.assertFalse(t.matches(".*[a-z].*"));
			found.add(t);
		}
		List<String> copy = new ArrayList<String>(found);
		Collections.sort(copy);
		Assert.assertNotSame(copy,found);
	}

}
