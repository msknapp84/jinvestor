package stock.download;

import jstock.dao.CompanyDAO;
import jstock.domain.Company;
import jstock.download.di.JStockDownloadContext;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.*;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=JStockDownloadContext.class)
@ActiveProfiles("accumulo")
public class CompanyIngesterTest {
	
	@Autowired
	private CompanyIngester companyIngester;
	
	@Autowired
	private CompanyDAO companyDAO;
	
	@Test
	public void testIngest() throws DataException {
		Company ge = companyDAO.get(new ByteArray("GE"));
		assertNull(ge);
		companyIngester.ingest();
		ge = companyDAO.get(new ByteArray("GE"));
		assertNotNull(ge);
		assertNotNull(ge.getIndustry());
		assertNotNull(ge.getSector());
		assertEquals("Consumer Electronics/Appliances",ge.getIndustry());
		assertEquals("Energy",ge.getSector());
		assertEquals("GE",ge.getTicker());
		assertEquals("GE",ge.getId().toString());
		
		Company bzm = companyDAO.get(new ByteArray("BZM"));
		assertEquals(2002,bzm.getIpoYear());
	}
}