package stock.download.er;

import java.time.LocalDate;

import jstock.dao.EconomicRecordDAO;
import jstock.domain.er.EconomicRecord;
import jstock.download.di.JStockDownloadContext;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=JStockDownloadContext.class)
@ActiveProfiles("accumulo")
public class SLFDownloaderTest {

	@Autowired
	private SLFDownloader downloader;
	
	@Autowired
	private EconomicRecordDAO economicRecordDAO;
	
	@Test
	public void testDownload() throws DataException {
		SLFSeriesSource source = new SLFSeriesSource();
		
		int i = 0;
		for (String seriesId : source) {
			EconomicRecord er = economicRecordDAO.get(new ByteArray(seriesId));
			assertNull(er);
			downloader.accept(seriesId);
			
			er = economicRecordDAO.get(new ByteArray(seriesId));
			assertNotNull(er);
			assertNotNull(er.getTitle());
			assertNotNull(er.getSeriesId());
			assertNotNull(er.getValue(LocalDate.now(), true));
			i++;
			if (i==3) {
				break;
				// you passed the test.
			}
		}
	}
	
	
}
