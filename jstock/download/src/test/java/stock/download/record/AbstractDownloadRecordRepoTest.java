package stock.download.record;

/*
 * #%L
 * jinvestor.parent
 * %%
 * Copyright (C) 2014 Michael Scott Knapp
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.text.ParseException;

import jstock.dao.DownloadDAO;
import jstock.domain.Download;
import jstock.domain.DownloadRecordQuery;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;
import knapp.common.util.TimeSpan;

import org.junit.Assert;
import org.junit.Test;

public abstract class AbstractDownloadRecordRepoTest {
	
	@Test
	public void create() throws ParseException, DataException {
		String uri = "http://localhost/foo";
		String tickerSymbol="GE";
		Download dr = new Download(uri, new ByteArray(tickerSymbol));
		dr.setAppVersion("1");
		dr.setDataTimeSpan(TimeSpan.fromThenToNow("2001-01-01"));
		dr.setMd5("foo");;
		dr.setResponseCode(202);
		dr.setResponseMessage("haha");
		dr.setSaveFilePath("/var/local/blah");
		dr.setServiceDenied(false);
		dr.setSystem("mine");
		
		DownloadDAO repo = getRepo();
		Assert.assertEquals(0,repo.count());
		repo.create(dr);
		Assert.assertEquals(1,repo.count());
		Download recovered1 = repo.getLatest(uri);
		Assert.assertEquals(dr,recovered1);
		Download recovered2 = repo.getLatestSuccess(uri);
		Assert.assertEquals(dr,recovered2);
		DownloadRecordQuery query = new DownloadRecordQuery();
		query.setUri(uri);
		Download rec = repo.get(query).get(0);
		Assert.assertEquals(dr,rec);
		Assert.assertEquals(dr.getMd5(),rec.getMd5());
		query.setUri("lkjew");
		Assert.assertTrue(repo.get(query).isEmpty());
		
		Download evil = new Download("lkjae", new ByteArray("EOIJ"));
		repo.create(evil);
		query.setUri(null);
		query.setTickerSymbol("EOIJ");
		Download wjer = repo.get(query).get(0);
		Assert.assertNotNull(wjer);
		
		Download cln = new Download(dr);
		cln.setMd5("jwrliwlijwe");
		repo.update(cln);
		Assert.assertEquals(2,repo.count());
		rec = repo.getLatest(uri);
		Assert.assertEquals("jwrliwlijwe",rec.getMd5());
		
		repo.delete(rec);
		Assert.assertEquals(1,repo.count());
		
	}

	@Test
	public void getLatest() {
	}

	@Test
	public void getLatestSuccess() {
	}

	@Test
	public void get() {
	}

	@Test
	public void update() {
	}

	@Test
	public void delete() {
	}

	@Test
	public void count() {
	}
	
	public abstract DownloadDAO getRepo();
}
