package stock.download;

/*
 * #%L
 * jinvestor.parent
 * %%
 * Copyright (C) 2014 Michael Scott Knapp
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import jstock.dao.DownloadDAO;
import jstock.domain.Download;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;
import knapp.common.util.TimeSpan;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stock.download.util.HttpResponseUtils;

public abstract class BaseDownloader<T> {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final DownloadDAO downloadRecordRepo;
	private final Supplier<CloseableHttpClient> clientSupplier;
	
	private TimeSpan downloadTimeRange;

	public BaseDownloader(
			final DownloadDAO repo,final Supplier<CloseableHttpClient> clientSupplier) {
		this.downloadRecordRepo = repo;
		this.clientSupplier = clientSupplier;
	}

	public void accept(T record,BiConsumer<Map<String,String>, InputStream> consumer) {
		if (record instanceof String) {
			record = (T) ((String)record).trim();
		}
		Collection<TimeSpan> missingTimeSpans = getMissingTimespans(record, downloadTimeRange);
		int failCount = 0;
		if (missingTimeSpans==null) {
			// the child class may not use time spans.
			// to keep things easy I just pretend they asked for ten years.
			missingTimeSpans = Collections.singleton(new TimeSpan(LocalDate.now().minus(10,ChronoUnit.YEARS), LocalDate.now()));
		}
		for (TimeSpan missing : missingTimeSpans) {
			// TODO don't count it as a failure if we decided not to 
			// download something just because we got it within the 
			// past 30 days.  In other words, the DOSPrevention code
			// forbode the download.
			if (!execute(record, missing,consumer)) {
				failCount++;
				if (failCount>2) {
					logger.warn("aborting download for "+getId(record));
					break;
				}
			}
		}
	}
	
	private boolean execute(final T record, final TimeSpan ts,
			BiConsumer<Map<String,String>, InputStream> consumer) {
		if (record == null) {
			throw new IllegalArgumentException("The record is null.");
		}
		if (ts == null) {
			throw new IllegalArgumentException("The timespan is null.");
		}
		InputStream in = null;
		boolean worked = false;
		boolean skipped = false;
		Download attemptRecord = null;
		HttpResponse response = null;
		CloseableHttpClient client = null;
		try {
			final String urlString = determineUrl(record,ts);
			URL url = new URL(urlString);
			if (isSafeToTry(url,record,ts,downloadRecordRepo)) {
				System.out.println("Downloading from: "+url);
				client = clientSupplier.get();
				// by now we are almost certainly going to attempt the download:
				attemptRecord = new Download(url,getId(record));
				attemptRecord.setQueryTimeSpan(ts);
				response = download(client,url);
				
				// TODO update the download record from the response.
//				attemptRecord.updateFromResponse(response);
				worked = HttpResponseUtils.passed(response);
				if (!worked) {
					handleFailure(record,
							ts, url);
				}
				Map<String,String> headers = getMapFromHeaders(response);
				in = response.getEntity().getContent();
				consumer.accept(headers, in);
			} else {
				// set worked to true so this doesn't count 
				// as a failure.
				skipped=true;
			}
		} catch (DataException | IOException e) {
			logger.error(e.getMessage(), e);
		} catch (IllegalStateException e) {
			logger.error(e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(in);
			if (client!=null) {
				try {
					client.close();
				} catch (IOException e) {
					logger.error("Failed to close the http client.",e);
				}
			}
			if (attemptRecord!=null) {
				try {
					downloadRecordRepo.create(attemptRecord);
				} catch (DataException e) {
					e.printStackTrace();
				}
			}
		}
		if (worked) {
			System.out.println("worked for "+record);
		}
		if (skipped) {
			System.out.println("Skipping "+record+" since we don't want to launch a DOS attack and be blacklisted.");
		}
		return worked || skipped;
	}

	protected boolean isSafeToTry(URL url, T record, TimeSpan ts,
			DownloadDAO downloadRecordRepo2) throws DataException {
		return DOSPrevention.shouldTry(downloadRecordRepo,url);
	}

	private void handleFailure(T record, TimeSpan ts, URL url) {
		// TODO Auto-generated method stub
		
	}

	public abstract ByteArray getId(T t);
	public abstract Collection<TimeSpan> getMissingTimespans(T t,TimeSpan within);
	public abstract String determineUrl(T t,TimeSpan timespan);

	public HttpResponse download(HttpClient client,URL url) {
		return DownloadHelper.get(client,url);
	}
	
	public static Map<String,String> getMapFromHeaders(HttpResponse resp) {
		Map<String,String> res = new HashMap<>();
		Arrays.asList(resp.getAllHeaders()).forEach(h->res.put(h.getName(),h.getValue()));
		return res;
	}
	
	public TimeSpan getDownloadTimeRange() {
		return downloadTimeRange;
	}

	public void setDownloadTimeRange(TimeSpan downloadTimeRange) {
		this.downloadTimeRange = downloadTimeRange;
	}
}