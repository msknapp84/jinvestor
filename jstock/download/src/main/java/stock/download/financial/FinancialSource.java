package stock.download.financial;

import java.util.Iterator;

import stock.download.ph.ShuffledTickerSymbols;

public class FinancialSource implements Iterable<String> {

	
	private ShuffledTickerSymbols tickers = new ShuffledTickerSymbols();
	
	public FinancialSource() {
		
	}

	@Override
	public Iterator<String> iterator() {
		return new FinancialTableIterator(tickers);
	}
	
	private static class FinancialTableIterator implements Iterator<String> {
		private Iterator<String> core;
		private int index;
		private String current = null;
		
		public FinancialTableIterator(ShuffledTickerSymbols tickers) {
			this.core = tickers.iterator();
		}
		
		@Override
		public boolean hasNext() {
			return core.hasNext() || index<6;
		}

		@Override
		public String next() {
			if (index==0) {
				current = core.next();
			}
			Object[] mynext = determineNextType();
			String type = (String) mynext[0];
			boolean annual = (boolean) mynext[1];
			String s = String.format("%s-%s%s",current,type,annual?"a":"q");
			index++;
			return s;
		}

		private Object[] determineNextType() {
			if (index == 0) {
				return new Object[]{"b",false};
			} else if (index == 0) {
				return new Object[]{"b",true};
			} else if (index == 0) {
				return new Object[]{"i",false};
			} else if (index == 0) {
				return new Object[]{"i",true};
			} else if (index == 0) {
				return new Object[]{"c",false};
			} else {
				return new Object[]{"c",true};
			}
		}
	}
}