package stock.download.financial;

/*
 * #%L
 * jinvestor.parent
 * %%
 * Copyright (C) 2014 Michael Scott Knapp
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

import jstock.dao.DownloadDAO;
import jstock.domain.Download;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;

import org.apache.commons.collections4.Closure;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stock.download.DOSPrevention;
import stock.download.DownloadHelper;
import stock.download.OutputWithMeta;
import stock.download.util.HttpResponseUtils;

public class FinancialDownloader implements Consumer<String> {
	private static final Logger logger = LoggerFactory
			.getLogger(FinancialDownloader.class);

	private static final FinancialCombo[] combos = new FinancialCombo[] {
			new FinancialCombo(FinancialType.BALANCE, FinancialPeriod.QUARTERLY),
			new FinancialCombo(FinancialType.CASHFLOW,
					FinancialPeriod.QUARTERLY),
			new FinancialCombo(FinancialType.INCOME, FinancialPeriod.QUARTERLY),
			new FinancialCombo(FinancialType.BALANCE, FinancialPeriod.ANNUALLY),
			new FinancialCombo(FinancialType.CASHFLOW, FinancialPeriod.ANNUALLY),
			new FinancialCombo(FinancialType.INCOME, FinancialPeriod.ANNUALLY), };

	private final DownloadHelper downloadHelper;
	private final DownloadDAO repo;
	private final Executor threadPool;
	private final FinancialOutputBuilder outputBuilder;
	private final FinancialExtractor responseTransformer;

	public FinancialDownloader(DownloadHelper downloadHelper,
			DownloadDAO repo, Executor threadPool,
			FinancialOutputBuilder outputBuilder,
			FinancialExtractor responseTransformer) {
		this.downloadHelper = downloadHelper;
		this.repo = repo;
		this.threadPool = threadPool;
		this.outputBuilder = outputBuilder;
		this.responseTransformer = responseTransformer;
	}

	@Override
	public void accept(String stockSymbol) {
		stockSymbol = stockSymbol.trim();
		for (final FinancialCombo combo : combos) {
			FinancialRunner runner = new FinancialRunner(combo, stockSymbol);
			if (threadPool != null) {
				// run in parallel
				threadPool.execute(runner);
			} else {
				// run in series
				runner.run();
			}
		}
	}

	private final class FinancialRunner implements Runnable {

		private final FinancialCombo combo;
		private final String stockSymbol;

		public FinancialRunner(final FinancialCombo combo,
				final String stockSymbol) {
			this.combo = combo;
			this.stockSymbol = stockSymbol;
		}

		@Override
		public void run() {
			OutputStream outputStream = null;
			Download attemptRecord = null;

			try {
				String url = outputBuilder.determineUrl(stockSymbol, combo);
				if (DOSPrevention.shouldTry(repo, url)) {
					OutputWithMeta output = outputBuilder.buildOutputStream(
							stockSymbol, combo, url);
					outputStream = output.getOutputStream();
					attemptRecord = new Download(url,
							new ByteArray(stockSymbol));
					HttpResponse response = null;
//					if (responseTransformer != null) {
//						// cannot simply write the entire page to file.
//						// this transformer takes the opportunity to 
//						// extract just that portion of text we want to save
//						ByteArrayOutputStream baos = new ByteArrayOutputStream();
//						try {
//							response = downloadHelper.download(url, baos);
//							String webResponse = new String(baos.toByteArray());
//							String usefulResponse = responseTransformer
//									.transform(webResponse,stockSymbol,combo);
//							IOUtils.write(usefulResponse, outputStream);
//						} finally {
//							IOUtils.closeQuietly(baos);
//						}
//					} else {
//						response = downloadHelper.download(url, outputStream);
//					}
//					attemptRecord.updateFromResponse(response);
//					attemptRecord.updateFromMeta(output);
					if (!HttpResponseUtils.passed(response)) {
						outputBuilder.handleFailure(stockSymbol, combo, url);
					}
				}
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			} catch (DataException e) {
				logger.error(e.getMessage(), e);
			} finally {
				IOUtils.closeQuietly(outputStream);
				if (attemptRecord != null) {
					try {
						repo.create(attemptRecord);
					} catch (DataException e) {
						logger.error(e.getMessage(), e);
					}
				}
			}
		}
	}

	public static final class FinancialCombo {
		private final FinancialType type;
		private final FinancialPeriod period;

		public FinancialCombo(FinancialType type, FinancialPeriod period) {
			this.type = type;
			this.period = period;
		}

		public FinancialType getType() {
			return type;
		}

		public FinancialPeriod getPeriod() {
			return period;
		}
	}

}
