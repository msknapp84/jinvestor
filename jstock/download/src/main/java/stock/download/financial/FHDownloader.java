package stock.download.financial;

import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import jstock.dao.DownloadDAO;
import jstock.dao.FinancialDAO;
import jstock.domain.Download;
import jstock.domain.fh.Financial;
import jstock.domain.fh.FinancialTable;
import jstock.domain.fh.StatementType;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;
import knapp.common.util.TimeSpan;

import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import stock.download.DAOBasedDownloader;
import stock.download.ph.PHDownloader;

public class FHDownloader extends DAOBasedDownloader<String, Financial> {
	private static final Logger logger = LoggerFactory
			.getLogger(PHDownloader.class);
	private final BiFunction<String, InputStream, Financial> parser;
	private final FinancialDAO dao;

	@Autowired
	public FHDownloader(DownloadDAO repo,
			final Supplier<CloseableHttpClient> clientSupplier, FinancialDAO dao,
			BiFunction<String, InputStream, Financial> parser) {
		super(repo, clientSupplier, dao::save);
		this.parser = parser;
		this.dao = dao;
	}

	@Override
	public ByteArray getId(String tickerSymbol) {
		return new ByteArray(tickerSymbol);
	}

	protected boolean isSafeToTry(URL url, String tickerSymbol, TimeSpan timespan,
			DownloadDAO downloadDAO) throws DataException {
		Download download = downloadDAO.getLatestByForeignId(url.getHost(), url.getPath(), "priceHistory", new ByteArray(tickerSymbol));
		if (download==null) {
			return true;
		}
		Calendar max = Calendar.getInstance();
		// if it was attempted in the past 30 days, don't attempt it again.
		max.add(Calendar.DAY_OF_MONTH, -30);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println("Max is: "+sdf.format(max.getTime()));
		System.out.println("Last download was on: "+sdf.format(download.getAttemptedOn().getTime()));
		return download.getAttemptedOn().before(max);
	}


	@Override
	public Collection<TimeSpan> getMissingTimespans(String rowId,
			TimeSpan within) {
		// this doesn't use time spans in the query.
		return null;
	}

	@Override
	public String determineUrl(String rowId, TimeSpan timespan) {
		return formUrl(rowId);
	}

	public static String formUrl(String rowId) {
		// it will come in like this:
		// GE-bq 
		// for quarterly balance sheet
		
		// http://finance.yahoo.com/q/<cf|is|bs>?s=FB[&annual]
		String ticker = FinancialTable.determineTicker(rowId);
		StatementType type = FinancialTable.determineType(rowId);
		boolean annual = FinancialTable.determineAnnual(rowId);
		String t = "bs";
		if (type==StatementType.CASHFLOW) {
			t = "cf";
		} else if (type == StatementType.INCOME) {
			t="is";
		}
		return String.format("http://finance.yahoo.com/q/%s?s=%s%s",t,ticker,annual?"&annual":"");
	}

	@Override
	public Collection<Financial> parse(String tickerSymbol,
			Map<String, String> attributes, InputStream in) {
		Financial t = parser.apply(tickerSymbol, in);
		return Collections.singleton(t);
	}
}