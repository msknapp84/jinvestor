package stock.download;

/*
 * #%L
 * jinvestor.parent
 * %%
 * Copyright (C) 2014 Michael Scott Knapp
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.function.Supplier;

import jstock.core.FileLineSource;
import jstock.download.di.JStockDownloadContext;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import stock.core.util.CloseableSourceUtil;
import stock.download.er.SLFDownloader;
import stock.download.er.SLFSeriesSource;
import stock.download.financial.FHDownloader;
import stock.download.financial.FinancialSource;
import stock.download.ph.PHDownloader;

public class DownloadCommandLine {

	public static void main(String[] args) {
		if (System.getProperty("env")==null) {
			// at the moment I have no real prod system, so this 
			// essentially means it's not a test, it's running locally
			// on a pseudo distributed cluster of one local node.
			System.setProperty("env", "prod");
		}
		Options options = new Options();
		options.addOption("y", false, "download yahoo price histories.");
		options.addOption("g", false, "download google price histories.");
		options.addOption("f", false, "download financial records from yahoo.");
		options.addOption("c", false, "ingest companies from the classpath");
		options.addOption("s", false, "download slf data");
		PosixParser parser = new PosixParser();
		try {
			final CommandLine commandLine = parser.parse(options, args);
			final ApplicationContext context = loadAppContext();
			if (commandLine.hasOption('y')) {
				downloadYahoo(context);
			}
			if (commandLine.hasOption('g')) {
				downloadGoogle(context);
			}
			if (commandLine.hasOption('s')) {
				downloadSlf(context);
			}
			if (commandLine.hasOption('f')) {
				downloadYahooFinancials(context);
			}
			if (commandLine.hasOption('c')) {
				ingestCompanies(context);
			}
		} catch (ParseException e) {
			HelpFormatter formatter = new HelpFormatter();
			String cmdLineSyntax = "downloads files";
			formatter.printHelp(cmdLineSyntax, options);
		}
	}

	private static void ingestCompanies(ApplicationContext context) {
		CompanyIngester ingester = context.getBean(CompanyIngester.class);
		ingester.ingest();
	}

	private static void downloadYahooFinancials(ApplicationContext context) {
		FHDownloader dl = context
				.getBean(FHDownloader.class);
		FinancialSource source = new FinancialSource();
		source.forEach(rowId->dl.accept(rowId));
	}

	private static void downloadSlf(ApplicationContext context) {
		SLFSeriesSource source = new SLFSeriesSource();
		SLFDownloader dl = context.getBean(SLFDownloader.class);
		source.forEach(s->dl.accept(s));
	}

	private static void downloadGoogle(ApplicationContext context) {
		PHDownloader dl = (PHDownloader) context.getBean("googlePH");
		FileLineSource symbolSource = (FileLineSource) context
				.getBean("tickerSymbolSource");
		CloseableSourceUtil.process(symbolSource, dl);
	}

	public static ApplicationContext loadAppContext() {
		final ApplicationContext context = new AnnotationConfigApplicationContext(
				JStockDownloadContext.class);
		return context;
	}

	public static void downloadYahoo(final ApplicationContext context) {
		PHDownloader dl = (PHDownloader) context.getBean("yahooPH");
		Supplier<String> symbolSource = (Supplier<String>) context
				.getBean("tickerSymbolSource");
		String ticker = symbolSource.get();
		while (ticker!=null) {
			dl.accept(ticker);
			ticker = symbolSource.get();
		}
//		CloseableSourceUtil.process(symbolSource, dl);
	}
}