package stock.download;

/*
 * #%L
 * jinvestor.parent
 * %%
 * Copyright (C) 2014 Michael Scott Knapp
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.InputStream;
import java.util.Collection;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

import jstock.dao.DownloadDAO;
import knapp.common.util.ByteArray;
import knapp.common.util.TimeSpan;

import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IndicatorDownloader extends BaseDownloader<String> implements Consumer<String> {

	@Autowired
	public IndicatorDownloader(DownloadDAO repo,Supplier<CloseableHttpClient> clientSupplier) {
		super(repo,clientSupplier);
	}

	@Override
	public ByteArray getId(String t) {
		return new ByteArray(t);
	}

	@Override
	public Collection<TimeSpan> getMissingTimespans(String t, TimeSpan within) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String determineUrl(String t, TimeSpan timespan) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void accept(String record) {
		BiConsumer<Map<String,String>,InputStream> consumer = (attributes,in) -> {
			
		};
		super.accept(record, consumer);
	}
}