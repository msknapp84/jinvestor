package stock.download;

import java.io.InputStream;
import java.util.Collection;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import jstock.dao.DownloadDAO;
import knapp.common.data.exceptions.DataException;

import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stock.core.function.ThrowingConsumer;

public abstract class DAOBasedDownloader<T,E> extends BaseDownloader<T> {
	private static final Logger logger = LoggerFactory.getLogger(DAOBasedDownloader.class);
	private ThrowingConsumer<Collection<E>,DataException> dao;
	
	public DAOBasedDownloader(DownloadDAO repo,
			Supplier<CloseableHttpClient> clientSupplier,ThrowingConsumer<Collection<E>,DataException> dao) {
		super(repo, clientSupplier);
		this.dao = dao;
	}


	public void accept(T record) {
		BiConsumer<Map<String,String>, InputStream> consumer = (attributes,in) -> {
			Collection<E> entities = parse(record,attributes,in);
			try {
				dao.accept(entities);
			} catch (Exception e) {
				logger.error("Failed to save entities",e);
			}
		};
		super.accept(record,consumer);
	}


	public abstract Collection<E> parse(T record,Map<String, String> attributes, InputStream in);
	

}
