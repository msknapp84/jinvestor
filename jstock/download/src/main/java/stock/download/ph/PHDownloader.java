package stock.download.ph;

/*
 * #%L
 * jinvestor.parent
 * %%
 * Copyright (C) 2014 Michael Scott Knapp
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;

import jstock.dao.DownloadDAO;
import jstock.dao.PriceHistoryDAO;
import jstock.domain.Download;
import jstock.domain.ph.PHTablet;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;
import knapp.common.util.TimeSpan;

import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import stock.download.DAOBasedDownloader;

@Component
public class PHDownloader extends DAOBasedDownloader<String, PHTablet>
		implements Consumer<String> {
	private static final Logger logger = LoggerFactory
			.getLogger(PHDownloader.class);
	private final BiFunction<String, InputStream, PHTablet> parser;
	private final PriceHistoryDAO dao;

	@Autowired
	public PHDownloader(DownloadDAO repo,
			final Supplier<CloseableHttpClient> clientSupplier, PriceHistoryDAO dao,
			BiFunction<String, InputStream, PHTablet> parser) {
		super(repo, clientSupplier, dao::save);
		this.parser = parser;
		this.dao = dao;
	}

	@Override
	public ByteArray getId(String tickerSymbol) {
		return new ByteArray(tickerSymbol);
	}
	

	protected boolean isSafeToTry(URL url, String tickerSymbol, TimeSpan timespan,
			DownloadDAO downloadDAO) throws DataException {
		Download download = downloadDAO.getLatestByForeignId(url.getHost(), url.getPath(), "priceHistory", new ByteArray(tickerSymbol));
		if (download==null) {
			return true;
		}
		Calendar max = Calendar.getInstance();
		// if it was attempted in the past 30 days, don't attempt it again.
		max.add(Calendar.DAY_OF_MONTH, -30);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println("Max is: "+sdf.format(max.getTime()));
		System.out.println("Last download was on: "+sdf.format(download.getAttemptedOn().getTime()));
		return download.getAttemptedOn().before(max);
	}


	@Override
	public Collection<TimeSpan> getMissingTimespans(String tickerSymbol,
			TimeSpan within) {
		if (within == null) {
			within = new TimeSpan(LocalDate.now().minus(5, ChronoUnit.YEARS),
					LocalDate.now());
		}
		Collection<TimeSpan> res = Collections.EMPTY_SET;
		try {
			res = dao.getMissingTimespans(new ByteArray(tickerSymbol), within);
		} catch (DataException e) {
			logger.error("Failed to get missing timespans", e);
		}
		return res;
	}

	@Override
	public String determineUrl(String tickerSymbol, TimeSpan timespan) {
		return formUrl(tickerSymbol, timespan);
	}

	
	public static String formUrl(String tickerSymbol, TimeSpan timespan) {
		// example:
		// http://real-chart.finance.yahoo.com/table.csv?s=%5EGSPC&d=1&e=1&f=2015&g=d&a=0&b=3&c=1950&ignore=.csv
		String url = null;
		try {
			LocalDate s = timespan.getStart();
			if (s == null) {
				s = LocalDate.ofEpochDay(0);
			}
			LocalDate e = timespan.getEnd() == null ? LocalDate.now()
					: timespan.getEnd();
			// http://real-chart.finance.yahoo.com/table.csv?s=YHOO&d=1&e=8&f=2015&g=d&a=3&b=12&c=1996&ignore=.csv

			// http://real-chart.finance.yahoo.com/table.csv?s=GE&d=2&e=8&f=2010&g=d&a=2&b=8&c=2015&ignore=.csv
			url = String
					.format("http://real-chart.finance.yahoo.com/table.csv?s=%s&d=%d&e=%d&f=%d&g=d&a=%d&b=%d&c=%d&ignore=.csv",
							URLEncoder.encode(tickerSymbol, "UTF8"),
							e.getMonthValue(), e.getDayOfMonth(), e.getYear(),
							s.getMonthValue(), s.getDayOfMonth(), s.getYear());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return url;
	}

	@Override
	public Collection<PHTablet> parse(String tickerSymbol,
			Map<String, String> attributes, InputStream in) {
		PHTablet t = parser.apply(tickerSymbol, in);
		return Collections.singleton(t);
	}
}