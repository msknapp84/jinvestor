package stock.download.ph;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * This just loads all of the ticker symbols into memory, and then shuffles
 * them. They are shuffled so that when you are inserting them into accumulo,
 * they do not all try to write to the same tablet at the same time, which would
 * create a bottleneck. If you did this in order, then one tablet would do all
 * the work while the others sat around doing nothing, it would be a slow
 * ingest.
 * 
 * @author michael
 */
public class ShuffledTickerSymbols implements Supplier<String>,
		Iterable<String> {

	private String[] classpathSources;

	private List<String> shuffledTickers;
	private int index;

	public ShuffledTickerSymbols() {
		this(new String[] { "/amex.csv", "/nasdaq.csv", "/nyse.csv" });
	}

	public ShuffledTickerSymbols(String[] classpathSources) {
		this.classpathSources = classpathSources;
	}

	@Override
	public String get() {
		lazyInit();
		if (index >= shuffledTickers.size()) {
			return null;
		}
		return shuffledTickers.get(index++);
	}

	private void lazyInit() {
		if (shuffledTickers != null) {
			return;
		}
		shuffledTickers = new ArrayList<>(4000);
		for (String cp : classpathSources) {
			InputStream input = null;
			try {
				input = getClass().getResourceAsStream(cp);
				List<String> lines = IOUtils.readLines(input);
				for (String line : lines) {
					if (StringUtils.isEmpty(line) || !line.contains(",")) {
						continue;
					}
					String ticker = line.split(",")[0].replaceAll("\"", "")
							.trim();
					if (StringUtils.isNotEmpty(ticker)) {
						shuffledTickers.add(ticker);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				IOUtils.closeQuietly(input);
			}
		}

		Collections.shuffle(shuffledTickers);
	}

	@Override
	public Iterator<String> iterator() {
		lazyInit();
		return shuffledTickers.iterator();
	}

}