package stock.download;

import java.io.InputStream;
import java.util.Collection;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jstock.dao.CompanyDAO;
import jstock.domain.Company;
import jstock.parser.company.CompanyParser;
import knapp.common.data.exceptions.DataException;

@Component
public class CompanyIngester {

	private CompanyParser parser;
	private CompanyDAO companyDAO;
	
	public CompanyIngester() {
		
	}
	
	public void ingest() {
		ingestFromClasspath("/nasdaq.csv");
		ingestFromClasspath("/nyse.csv");
		ingestFromClasspath("/amex.csv");
	}

	public void ingestFromClasspath(String classpath) {
		InputStream in = null;
		try {
			in = CompanyIngester.class.getResourceAsStream(classpath);
			ingest(in);
		} catch (DataException de) {
			de.printStackTrace();
		} finally {
			IOUtils.closeQuietly(in);
		}
	}
	public void ingest(InputStream in) throws DataException {
		Collection<Company> companies = parser.apply(in);
		companyDAO.save(companies);
	}

	public CompanyParser getParser() {
		return parser;
	}

	@Autowired
	public void setParser(CompanyParser parser) {
		this.parser = parser;
	}

	public CompanyDAO getCompanyDAO() {
		return companyDAO;
	}

	@Autowired
	public void setCompanyDAO(CompanyDAO companyDAO) {
		this.companyDAO = companyDAO;
	}
	
}
