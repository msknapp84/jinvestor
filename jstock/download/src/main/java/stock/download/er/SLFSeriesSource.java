package stock.download.er;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

public class SLFSeriesSource implements Supplier<String>, Iterable<String> {

	private List<String> series = new ArrayList<>();
	
	private int i = 0;

	public SLFSeriesSource() {
		try {
			String text = IOUtils.toString(SLFSeriesSource.class
					.getResourceAsStream("/SLFSeries.txt"));
			for (String line : text.split("\n")) {
				if (StringUtils.isEmpty(line)) {
					continue;
				}
				series.add(line.trim());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		Collections.shuffle(series);
	}

	@Override
	public Iterator<String> iterator() {
		return series.iterator();
	}

	@Override
	public String get() {
		if (i<series.size()) {
			return series.get(i++);
		}
		return null;
	}
}