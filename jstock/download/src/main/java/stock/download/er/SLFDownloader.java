package stock.download.er;

import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.function.Supplier;

import jstock.dao.DownloadDAO;
import jstock.dao.EconomicRecordDAO;
import jstock.domain.Download;
import jstock.domain.er.EconomicRecord;
import jstock.parser.er.SLFTextParser;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;
import knapp.common.util.TimeSpan;

import org.apache.http.impl.client.CloseableHttpClient;

import stock.download.DAOBasedDownloader;

public class SLFDownloader extends DAOBasedDownloader<String, EconomicRecord> {
	private SLFTextParser parser = new SLFTextParser();

	private EconomicRecordDAO dao;

	public SLFDownloader(DownloadDAO repo,
			Supplier<CloseableHttpClient> clientSupplier, EconomicRecordDAO dao) {
		super(repo, clientSupplier, (ers) -> {
			dao.save(ers);
		});
		this.dao = dao;
	}

	@Override
	public Collection<EconomicRecord> parse(String record,
			Map<String, String> attributes, InputStream in) {
		EconomicRecord er = parser.apply(in);
		er.setId(er.getSeriesId());
		return Collections.singleton(er);
	}

	@Override
	public ByteArray getId(String t) {
		return new ByteArray(t);
	}

	@Override
	public Collection<TimeSpan> getMissingTimespans(String t, TimeSpan within) {
		return null;
	}

	@Override
	public String determineUrl(String t, TimeSpan timespan) {
		return String.format(
				"http://research.stlouisfed.org/fred2/data/%s.txt", t);
	}

	@Override
	protected boolean isSafeToTry(URL url, String record, TimeSpan ts,
			DownloadDAO downloadRecordRepo2) throws DataException {
		Download download = downloadRecordRepo2.getLatestByForeignId(
				url.getHost(), url.getPath(), "economicRecords",
				new ByteArray(record));
		if (download==null) {
			return true;
		}
		Calendar now = Calendar.getInstance();
		Calendar min = download.getAttemptedOn();
		min.add(Calendar.DAY_OF_MONTH, 20);
		// if it is on or after the min, then ok.
		return !now.before(min);
	}
}