package jstock.download.di;

import java.util.Collection;

import knapp.common.util.TimeSpan;

public interface TimeSpanInfoService<T> {
	Collection<TimeSpan> getCoveredTimespans(T entity,TimeSpan within);
	Collection<TimeSpan> getMissingTimespans(T entity,TimeSpan within);
}