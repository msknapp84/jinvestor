package jstock.download.di;

import java.io.InputStream;
import java.util.concurrent.Executor;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import jstock.accumulo.di.JStockAccumuloContext;
import jstock.dao.CompanyDAO;
import jstock.dao.DownloadDAO;
import jstock.dao.EconomicRecordDAO;
import jstock.dao.FinancialDAO;
import jstock.dao.FinancialLabelDAO;
import jstock.dao.FinancialLabelMemDAO;
import jstock.dao.PriceHistoryDAO;
import jstock.domain.fh.Financial;
import jstock.domain.fh.FinancialTable;
import jstock.domain.fh.StatementType;
import jstock.domain.ph.PHTablet;
import jstock.parser.company.CompanyParser;
import jstock.parser.fh.FinancialTableParser;
import jstock.parser.ph.PHCSVParser;
import knapp.common.util.TimeSpan;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import stock.download.CompanyIngester;
import stock.download.er.SLFDownloader;
import stock.download.financial.FHDownloader;
import stock.download.financial.FinancialExtractor;
import stock.download.financial.FinancialOutputBuilder;
import stock.download.financial.XPathFinancialExtractor;
import stock.download.financial.YahooFOB;
import stock.download.ph.PHDownloader;
import stock.download.ph.ShuffledTickerSymbols;

@Configuration
@PropertySource(value={"classpath:download.properties"})
@Import(value=JStockAccumuloContext.class)
public class JStockDownloadContext {
	
	// note: get ticker symbol lists, and company information, here:
	// http://www.nasdaq.com/screening/company-list.aspx
	
	// this bean is found first by looking into the imported
	// JStockAccumuloContext, and from there it was component-scanned.
	// The other context component-scans anything in jstock.accumulo,
	// where the PHAccumuloDAO is.
	@Autowired
	private PriceHistoryDAO priceHistoryDAO;
	
	// likewise, this is found from the imported context.
	@Autowired
	private DownloadDAO downloadDAO;
	
	@Autowired
	private CompanyDAO companyDAO;
	
	@Autowired
	private EconomicRecordDAO economicRecordDAO;
	
	@Autowired
	private FinancialDAO financialDAO;

	@Autowired
	private FinancialLabelDAO financialLabelDAO;
	
	@Autowired
	private Environment environment;
	
	@Bean
	public BiFunction<String, InputStream, PHTablet> priceHistoryParser() {
		return new PHCSVParser();
	}

	@Bean
	public FinancialExtractor responseTransformer() {
		String xpathExpression = environment.getProperty("stock.download.financial.yahoo.extractor.xpath");
		XPathFinancialExtractor ex = new XPathFinancialExtractor(xpathExpression);
		return ex;
	}

	@Bean
	public FinancialOutputBuilder yahooFOB() {
		String pathFormat = environment.getProperty("stock.download.financial.yahoo.path.format");
		YahooFOB fob = new YahooFOB(pathFormat);
		return fob;
	}

	@Bean
	public Executor threadPool() {
		return null;
	}

	@Bean
	@Profile(value={"mem"})
	public DownloadDAO downloadDAO() {
		return null;
	}

	@Bean
	public HttpClientBuilder clientBuilder() {
		return HttpClientBuilder.create();
	}

	@Bean
	public Supplier<CloseableHttpClient> clientSupplier() {
		return clientBuilder()::build;
	}
	
//	@Bean
//	public FileLineSource tickerSymbolSource() {
//		String source = environment.getProperty("stock.download.data.tickerSymbols.list");
//		File sourceFile = new File(source);
//		return new FileLineSource(sourceFile, false);
//	}
	
	@Bean
	public Supplier<String> tickerSymbolSource() {
		// the no-arg constructor has the three classpaths 
		// for the three major stock exchange files.
		return new ShuffledTickerSymbols();
	}
	
	@Bean
	public PHDownloader yahooPH() {
		PHDownloader dl = new PHDownloader(downloadDAO, clientSupplier(), priceHistoryDAO, priceHistoryParser());
		return dl;
	}
	
	@Bean
	public TimeSpan yahooTimespan() {
		return TimeSpan.fromThenToNow("2003-01-01");
	}
	
	@Bean
	public CompanyParser companyParser() {
		return new CompanyParser();
	}
	
	@Bean
	public CompanyIngester companyIngester() {
		CompanyIngester ci = new CompanyIngester();
		ci.setParser(companyParser());
		ci.setCompanyDAO(companyDAO);
		return ci;
	}
	
	@Bean
	public SLFDownloader slfDownloader() {
		return new SLFDownloader(downloadDAO, clientSupplier(), economicRecordDAO);
	}
	
	@Bean
	public FHDownloader financialDownloader() {
		return new FHDownloader(downloadDAO,clientSupplier(),financialDAO,financialParser());
	}
	
	@Bean
	public BiFunction<String,InputStream,Financial> financialParser() {
		final FinancialTableParser parser = financialTableParser();
		return (rowId,inputStream) -> {
			FinancialTable table = parser.apply(inputStream);
			if (table==null) {
				// it could not be parsed.
				return null;
			}
			Financial financial = new Financial();
			financial.set(table);
			financial.setId(financial.getTicker());
			return financial;
		};
	}
	
	@Bean
	public FinancialTableParser financialTableParser() {
		return new FinancialTableParser(financialLabelCache());
	}
	
	@Bean 
	public FinancialLabelDAO financialLabelCache() {
		FinancialLabelMemDAO cache = new FinancialLabelMemDAO(financialLabelDAO);
		return cache;
	}
}