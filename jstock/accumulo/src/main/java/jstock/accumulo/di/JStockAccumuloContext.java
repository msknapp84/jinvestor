package jstock.accumulo.di;

import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.Instance;
import org.apache.accumulo.core.client.NamespaceExistsException;
import org.apache.accumulo.core.client.TableExistsException;
import org.apache.accumulo.core.client.ZooKeeperInstance;
import org.apache.accumulo.core.client.mock.MockConnector;
import org.apache.accumulo.core.client.mock.MockInstance;
import org.apache.accumulo.core.client.security.tokens.AuthenticationToken;
import org.apache.accumulo.core.client.security.tokens.PasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@ComponentScan(basePackages = "jstock.accumulo")
@PropertySource(value = { "classpath:/jstock/jstock.properties",
		"classpath:/jstock/accumulo.properties",
		"classpath:/jstock/sensitive.properties" })
// the sensitive properties file has the user name
// and password, it is ignored by git so other people will need
// to create it too, or provide those args using -D
public class JStockAccumuloContext {
	@Autowired
	Environment env;

	@Bean
	public Connector accumuloConnection() throws AccumuloException,
			AccumuloSecurityException {
		boolean mockMode = true;
		System.setProperty("jstock.accumulo.namespace","test_jstock");
		for (String p : env.getActiveProfiles()) {
			if ("test_integration".equals(p)) {
				mockMode=false;
			}
		}
		
		if (mockMode) {
			MockConnector connector = null;
			try {
				// use a test connection to a mock.
				MockInstance instance = new MockInstance();
				AuthenticationToken token = new PasswordToken();
				connector = (MockConnector) instance
						.getConnector("root", token);
				connector.namespaceOperations().create("test_jstock");
				connector.tableOperations().create("test_jstock.priceHistory");
				connector.tableOperations().create("test_jstock.download");
				connector.tableOperations().create("test_jstock.economicRecord");
				connector.tableOperations().create("test_jstock.company");
				connector.tableOperations().create("test_jstock.financialLabel");
				connector.tableOperations().create("test_jstock.financial");
			} catch (NamespaceExistsException e) {
				e.printStackTrace();
			} catch (TableExistsException e) {
				e.printStackTrace();
			}
			return connector;
		} else {
			String instanceName = env
					.getProperty("jstock.accumulo.instanceName");
			String zooServers = env.getProperty("jstock.accumulo.zooServers");// "zooserver-one,zooserver-two";
			Instance inst = new ZooKeeperInstance(instanceName, zooServers);
			String user = env.getProperty("jstock.accumulo.user");
			String pw = env.getProperty("jstock.accumulo.password");
			if (user == null || pw == null) {
				throw new IllegalStateException(
						"The user or password is null.  Make sure that the 'sensitive.properties' file is "
								+ "on your classpath, it is ignored by git so you won't have it unless you made it.  Also make sure that it "
								+ "works when you use it to log into accumulo.  If you're running this from eclipse, you may need to modify "
								+ "the runtime configuration to include folders from other projects on your classpath, like src/main/resources "
								+ "from the jstock.accumulo project.");
			}

			Connector conn = inst.getConnector(user, new PasswordToken(pw));
			return conn;
		}
	}
}
