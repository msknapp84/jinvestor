package jstock.accumulo;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import jstock.dao.EconomicRecordDAO;
import jstock.domain.er.EconomicRecord;
import jstock.domain.er.EconomicRecordRow;
import knapp.common.accumulo.dao.MultiRowAccumuloDAO;
import knapp.common.accumulo.dao.RowIterable;
import knapp.common.accumulo.dao.RowIterable.RowIterator;
import knapp.common.collections.time.DoublesOverTime;
import knapp.common.collections.time.DoublesOverTimeImmutable;
import knapp.common.collections.time.ValueOnDay;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;
import knapp.common.util.ByteUtils;
import knapp.common.util.IteratorWrapper;

import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static knapp.common.accumulo.dao.AccUtil.*;

@Component
public class ERAccumuloDAO extends
		MultiRowAccumuloDAO<EconomicRecord, EconomicRecordRow> implements
		EconomicRecordDAO {

	private static final String TABLE = "economicRecord";
	private static final String FAMILY = "f";
	private static final String DATE = "d";
	private static final String VALUE = "v";
	private static final String SERIESID = "s";
	private static final String TITLE = "t";
	private static final String SOURCE = "c";
	private static final String RELEASE = "r";
	private static final String SEASONALLY_ADJUSTED = "a";
	private static final String FREQUENCY = "fr";
	private static final String PERIODICITY = "p";
	private static final String UNITS = "u";

	@Autowired
	public ERAccumuloDAO(Connector connector) {
		super(connector);
	}

	@Override
	public String getTable() {
		return TABLE;
	}

	@Override
	public String getFamilyForCount() {
		return FAMILY;
	}

	@Override
	public ByteArray determineId(EconomicRecord entity) {
		return new ByteArray(entity.getSeriesId());
	}

	@Override
	public EconomicRecord loadRecords(String seriesId, LocalDate start,
			LocalDate end) throws DataException {
		return null;
	}

	@Override
	public void saveRecords(Collection<EconomicRecord> records)
			throws DataException {
		super.save(records);
	}

	@Override
	public Iterable<Mutation> toMutations(EconomicRecord entity) {
		return new ERMutationIterable(entity);
	}

	private static class ERMutationIterable implements Iterable<Mutation> {
		private final EconomicRecord entity;

		public ERMutationIterable(EconomicRecord entity) {
			this.entity = entity;
		}

		@Override
		public Iterator<Mutation> iterator() {
			Mutation basics = createERInfoMutation(entity);
			Iterator<Mutation> first = Collections.singleton(basics).iterator();
			Iterator<Mutation> second = new ERMutationIterator(
					entity.getSeriesId(), entity.getValues().iterator());
			IteratorWrapper<Mutation> wrap = new IteratorWrapper<Mutation>(
					first, second);
			return wrap;
		}
	}

	public static Mutation createERInfoMutation(EconomicRecord economicRecord) {
		if (economicRecord == null) {
			return null;
		}
		Mutation mutation = new Mutation(economicRecord.getSeriesId());
		put(mutation,FAMILY,SERIESID,economicRecord.getSeriesId());
		put(mutation,FAMILY,TITLE,economicRecord.getTitle());
		put(mutation,FAMILY,SOURCE,economicRecord.getSource());
		put(mutation,FAMILY,RELEASE,economicRecord.getRelease());
		put(mutation,FAMILY,SEASONALLY_ADJUSTED,economicRecord.isSeasonallyAdjusted());
		put(mutation,FAMILY,PERIODICITY,economicRecord.getPeriodicity());
		put(mutation,FAMILY,UNITS,economicRecord.getUnits());
		if (economicRecord.getFrequency() != null) {
			mutation.put(FAMILY.getBytes(), FREQUENCY.getBytes(),
					economicRecord.getFrequency().name().getBytes());
		}
		return mutation;
	}

	private static class ERMutationIterator implements Iterator<Mutation> {
		private final Iterator<ValueOnDay<Double>> core;
		private final String seriesId;

		public ERMutationIterator(final String seriesId,
				final Iterator<ValueOnDay<Double>> core) {
			this.seriesId = seriesId;
			this.core = core;
		}

		@Override
		public boolean hasNext() {
			return core.hasNext();
		}

		@Override
		public Mutation next() {
			ValueOnDay<Double> vod = core.next();
			Mutation md = new Mutation(EconomicRecordRow.determineRowId(
					seriesId, vod.getDate()));
			md.put(FAMILY.getBytes(), DATE.getBytes(),
					ByteUtils.dateToBytes(vod.getDate()));
			md.put(FAMILY.getBytes(), VALUE.getBytes(),
					ByteUtils.doubleToBytes(vod.getValue()));
			return md;
		}
	}

	@Override
	public Map<ByteArray, EconomicRecord> toEntities(
			Iterable<Entry<Key, Value>> scan) throws DataException {
		RowIterable rows = RowIterable.byRow(scan);
		RowIterator iter = rows.iterator();
		Map<ByteArray, EconomicRecord> finished = new HashMap<>();
		Map<String, DoublesOverTime> builders = new HashMap<>();
		Map<String, Value> row = null;
		while (iter.hasNext()) {
			row = iter.next();
			String seriesId = EconomicRecordRow.determineSeriesIdFromRowId(iter
					.getCurrentRowId());
			ByteArray sid = new ByteArray(seriesId);
			EconomicRecord er = finished.get(sid);
			if (er == null) {
				er = new EconomicRecord(seriesId);
				finished.put(sid, er);
			}
			if (row.containsKey("f.v")) {
				// this is a value row.
				DoublesOverTime dot = builders.get(seriesId);
				if (dot == null) {
					dot = new DoublesOverTime();
					builders.put(seriesId, dot);
				}
				LocalDate d = ByteUtils.bytesToDate(row.get("f.d").get());
				double v = ByteUtils.bytesToDouble(row.get("f.v").get());
				dot.add(d, v);
			} else {
				// this is an info row.
				final EconomicRecord e = er;
				setIfString(TITLE,row,s->e.setTitle(s));
				setIfString(SOURCE,row,s->e.setSource(s));
				setIfString(RELEASE,row,s->e.setRelease(s));
				setIfString(UNITS,row,s->e.setUnits(s));
				setIfBool(SEASONALLY_ADJUSTED,row,s->e.setSeasonallyAdjusted(s));
				setIfInt(PERIODICITY,row,s->e.setPeriodicity(s));
				if (row.containsKey("f." + FREQUENCY)) {
					String f = new String(row.get("f." + FREQUENCY).get());
					ChronoUnit u = ChronoUnit.valueOf(f);
					er.setFrequency(u);
				}
			}
		}
		for (String seriesId : builders.keySet()) {
			EconomicRecord er = finished.get(new ByteArray(seriesId));
			DoublesOverTimeImmutable doti = new DoublesOverTimeImmutable(
					builders.get(seriesId));
			er.setValues(doti);
		}
		return finished;
	}

	@Override
	public String getQualifierForCount() {
		return VALUE;
	}
}