package jstock.accumulo;

import java.util.Arrays;
import java.util.Collection;

import jstock.domain.Company;
import knapp.common.accumulo.dao.MutationMapping;
import knapp.common.accumulo.dao.SingleRowAccumuloDAO;

import org.apache.accumulo.core.client.Connector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("accumulo")
public class CompanyAccumuloDAO extends SingleRowAccumuloDAO<Company> implements
		jstock.dao.CompanyDAO {

	// Table layout:
	// key - company ticker symbol
	// column family - 'd'
	// column qualifier - either description or sector
	// value - based on qualifier.

	private static final String TABLE = "company";
	private static final String FAMILY = "f";
	private static final String TICKER = "t";
	private static final String NAME = "n";
	private static final String SECTOR = "s";
	private static final String INDUSTRY = "i";
	private static final String IPO = "y";

	@Override
	public Collection<MutationMapping<Company>> getMutationMappings() {
		MutationMapping<Company> m1 = new MutationMapping<Company>(FAMILY,
				TICKER, "ticker").entityType(Company.class).finish();
		MutationMapping<Company> m2 = new MutationMapping<Company>(FAMILY,
				NAME, "name").entityType(Company.class).finish();
		MutationMapping<Company> m3 = new MutationMapping<Company>(FAMILY,
				SECTOR, "sector").entityType(Company.class).finish();
		MutationMapping<Company> m4 = new MutationMapping<Company>(FAMILY,
				INDUSTRY, "industry").entityType(Company.class).finish();
		MutationMapping<Company> m5 = new MutationMapping<Company>(FAMILY,
				IPO, "ipoYearAfterEpoch").entityType(Company.class).finish();
		return Arrays.asList(m1,m2,m3,m4,m5);
	}

	// Key is the ticker symbol.

	@Autowired
	public CompanyAccumuloDAO(Connector connector) {
		super(connector);
	}

	@Override
	public String getFamilyForCount() {
		return FAMILY;
	}

	@Override
	public String getQualifierForCount() {
		return TICKER;
	}

	@Override
	public String getTable() {
		return TABLE;
	}

//	@Override
//	public Iterable<Mutation> toMutations(Company entity) {
//		Mutation mutation = new Mutation(entity.getTicker());
//		mutation.put(FAMILY.getBytes(), TICKER.getBytes(), entity.getTicker()
//				.getBytes());
//		mutation.put(FAMILY.getBytes(), SECTOR.getBytes(), entity.getSector()
//				.getBytes());
//		mutation.put(FAMILY.getBytes(), INDUSTRY.getBytes(), entity
//				.getIndustry().getBytes());
//		mutation.put(FAMILY.getBytes(), IPO.getBytes(),
//				new byte[] { entity.getIpoYearAfterEpoch() });
//		mutation.put(FAMILY.getBytes(), NAME.getBytes(), entity.getName()
//				.getBytes());
//		return Collections.singleton(mutation);
//	}
//
//	@Override
//	public Map<ByteArray, Company> toEntities(Iterable<Entry<Key, Value>> scan)
//			throws DataException {
//		Map<ByteArray, Company> companies = new HashMap<>();
//		for (Entry<Key, Value> kv : scan) {
//			ByteArray ticker = new ByteArray(kv.getKey().getRow().getBytes());
//			Company company = companies.get(ticker);
//			if (company == null) {
//				company = new Company();
//				companies.put(ticker, company);
//			}
//			if (kv.getKey().getColumnQualifier().toString().equals(SECTOR)) {
//				company.setSector(kv.getValue().toString());
//			}
//			if (kv.getKey().getColumnQualifier().toString().equals(INDUSTRY)) {
//				company.setIndustry(kv.getValue().toString());
//			}
//			if (kv.getKey().getColumnQualifier().toString().equals(IPO)) {
//				company.setIpoYearAfterEpoch(kv.getValue().get()[0]);
//			}
//			if (kv.getKey().getColumnQualifier().toString().equals(NAME)) {
//				company.setName(kv.getValue().toString());
//			}
//		}
//		return companies;
//	}

	@Override
	public Class<Company> getEntityClass() {
		return Company.class;
	}
}