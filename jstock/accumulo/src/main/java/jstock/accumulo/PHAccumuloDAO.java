package jstock.accumulo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import jstock.dao.PriceHistoryDAO;
import jstock.domain.ph.PHRowMutable;
import jstock.domain.ph.PHTablet;
import jstock.domain.ph.PHTablet.PHTabletGrowableBuilder;
import static knapp.common.accumulo.dao.AccUtil.*;
import knapp.common.accumulo.dao.MultiRowAccumuloDAO;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;
import knapp.common.util.TimeSpan;
import static knapp.common.util.ByteUtils.*;

import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.client.TableNotFoundException;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.hadoop.io.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component("priceHistoryDAO")
@Profile("accumulo")
public class PHAccumuloDAO extends MultiRowAccumuloDAO<PHTablet,PHRowMutable> implements PriceHistoryDAO {
	private static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");
	public static final String TABLE = "priceHistory";
	private static final String TICKER = "t";
	private static final String COLUMN_FAMILY = "f";
	private static final String OPEN = "o";
	private static final String HIGH = "h";
	private static final String LOW = "l";
	private static final String CLOSE = "c";
	private static final String ADJ_CLOSE = "a";
	private static final String VOLUME = "v";
	private static final String DATE = "d";
	
	@Autowired
	public PHAccumuloDAO(Connector connector) {
		super(connector);
	}

	@Override
	public String getTable() {
		return TABLE;
	}


	@Override
	public PHTablet load(ByteArray ticker, LocalDate from, LocalDate to)
			throws DataException {
		ByteArray id = new ByteArray(ticker);
		Map<ByteArray,PHTablet> m = loadRecords(Collections.singleton(id),from,to);
		return m.get(id);
	}

	@Override
	public PHTablet loadTablet(ByteArray ticker) throws DataException {
		return this.get(ticker);
	}

	@Override
	public Map<ByteArray, PHTablet> loadTablets(Collection<ByteArray> ticker)
			throws DataException {
		Set<Range> ranges = ticker.stream().map(Range::new).collect(Collectors.toSet()); 
		Map<ByteArray,PHTablet> ts = super.loadRanges(ranges,null);
		return ts;
	}

	@Override
	public Map<ByteArray, PHTablet> loadRecords(Collection<ByteArray> tickers, LocalDate start,
			LocalDate end) throws DataException {
		Collection<Range> ranges = new HashSet<Range>();
		for (ByteArray ticker : tickers) {
			ranges.add(new Range(PHTablet.determineId(ticker, start),
					incrementLexically(PHTablet.determineId(ticker, end))));
		}
		return super.loadRanges(ranges,null);
	}

	@Override
	public Collection<TimeSpan> getMissingTimespans(ByteArray tickerSymbol,
			TimeSpan within) throws DataException {
		if (within == null) {
			within = new TimeSpan(LocalDate.now().minus(5,ChronoUnit.YEARS),LocalDate.now());
		}
		Scanner scan = null;
		List<TimeSpan> timeSpansMissing = null;
		try {
			scan = getConnector().createScanner(getNameSpaceAndTable(), PUBLIC_AUTHORIZATIONS);
			scan.setRange(Range.prefix(tickerSymbol));
			
			// we only need the dates.
			scan.fetchColumn(new Text(COLUMN_FAMILY), new Text(DATE));
			
			List<LocalDate> dates = new ArrayList<LocalDate>();
			for (Entry<Key,Value> keyValue : scan) {
				LocalDate ld = bytesToDate(keyValue.getValue().get());
				dates.add(ld);
			}
			List<TimeSpan> timeSpansPresent = TimeSpan.getPresentTimeSpans(dates, 5);
			timeSpansMissing = TimeSpan.removeFrom(timeSpansPresent,within,5);
		} catch (TableNotFoundException e) {
			wrapException(null,getTable(),null, e);
		} finally {
			if (scan!=null) {
				scan.close();
			}
		}
		return timeSpansMissing;
	}
	
	@Override
	public int countAll() throws DataException {
		// when scanning the PH table for a count of entities, 
		// we need to only count by the uri prefix.
		return countAll(scan->{
			// I use the priority 20 so that this comes before the
			
			// the default no-option RowPrefixFilter will split on 
			// hyphens and underscores.  The row id for price histories
			// starts with a ticker and is followed by a hyphen.
			// so the no-option row prefix filter should successfully
			// filter things down to just one row/cell per entity.
			
			// CountIterator that the countAll(consumer) method adds for us.
			IteratorSetting s = new IteratorSetting(20,knapp.open.iterators.RowPrefixFilter.class);
			scan.addScanIterator(s);
	
		});
	}

	@Override
	public Iterable<Mutation> toMutations(PHTablet entity) {
		List<Mutation> ms = new ArrayList<>();
		for (PHRowMutable row : entity){
			String rowId = PHTablet.determineId(entity.getTicker(), row.getDate());
			Mutation m = new Mutation(rowId);
			put(m,COLUMN_FAMILY, TICKER, entity.getTicker().getBytes());
			put(m,COLUMN_FAMILY, OPEN, intToBytes(row.getOpenPennies()));
			put(m,COLUMN_FAMILY, HIGH, intToBytes(row.getHighPennies()));
			put(m,COLUMN_FAMILY, LOW, intToBytes(row.getLowPennies()));
			put(m,COLUMN_FAMILY, CLOSE, intToBytes(row.getClosePennies()));
			put(m,COLUMN_FAMILY, ADJ_CLOSE, intToBytes(row.getAdjClosePennies()));
			put(m,COLUMN_FAMILY, VOLUME, intToBytes(row.getVolume()));
			put(m,COLUMN_FAMILY,DATE,row.getDate());
			ms.add(m);
		}
		return ms;
	}

	@Override
	public Map<ByteArray,PHTablet> toEntities(
			Iterable<Entry<Key, Value>> scan) throws DataException {
		Map<ByteArray,PHTabletGrowableBuilder> res = new HashMap<>();
		
		PHRowMutable row = new PHRowMutable();
		String[] parts = null;
		String rowId = null;
		Key key = null;
		ByteArray ticker = null;
		String lastDateString = null;
		String dateString = null;
		ByteArray lastTicker = null;
		
		for (Entry<Key, Value> keyValues : scan) {
			key = keyValues.getKey();
			rowId = key.getRow().toString();
			parts = rowId.split("-");
			ticker = new ByteArray(parts[0]);
			dateString = parts[1];
			
			PHTabletGrowableBuilder lastBuilder = res.get(lastTicker);
			PHTabletGrowableBuilder currentBuilder = res.get(ticker);
			if (currentBuilder==null) {
				currentBuilder = PHTablet.growable(ticker);
				res.put(new ByteArray(ticker),currentBuilder);
			}
			
			if (lastBuilder!=null) {
				// nested if statement because it's easier to read.
				if ((lastDateString!=null && !dateString.equals(lastDateString)) || 
						(ticker!=null && !ticker.equals(lastTicker))){
					// the date or ticker has changed, so save 
					// the row before you lose it.
					lastBuilder.add(row);
				}
			}
			setIfInt(OPEN,keyValues,i->row.setOpenPennies(i));
			setIfInt(HIGH,keyValues,i->row.setHighPennies(i));
			setIfInt(LOW,keyValues,i->row.setLowPennies(i));
			setIfInt(CLOSE,keyValues,i->row.setClosePennies(i));
			setIfInt(ADJ_CLOSE,keyValues,i->row.setAdjClosePennies(i));
			setIfInt(VOLUME,keyValues,i->row.setVolume(i));
			setIfDate(DATE,keyValues,i->row.setDate(i));
			
			lastTicker = ticker;
			lastDateString = dateString;
		}
		// for the last record, the row won't have been saved, so do it now:
		if (ticker!=null && res.get(ticker)!=null) {
			res.get(ticker).add(row);
		}
		
		// build all of them.
		Map<ByteArray,PHTablet> done = new HashMap<>();
		res.keySet().forEach(t->done.put(t,res.get(t).build()));
		return done;
	}

	@Override
	public String getFamilyForCount() {
		return COLUMN_FAMILY;
	}

	@Override
	public String getQualifierForCount() {
		return ADJ_CLOSE;
	}
}