package jstock.accumulo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jstock.dao.FinancialDAO;
import jstock.domain.fh.Financial;
import jstock.domain.fh.StatementType;
import jstock.domain.fh.Financial.FinancialBuilder;
import jstock.domain.fh.FinancialTable;
import knapp.common.accumulo.dao.MultiRowAccumuloDAO;
import knapp.common.collections.time.DoubleTableCell;
import knapp.common.collections.time.DoubleTableImmutable;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;
import knapp.common.util.ByteUtils;

import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("accumulo")
public class FinancialAccumuloDAO extends
		MultiRowAccumuloDAO<Financial, DoubleTableImmutable> implements
		FinancialDAO {
	private static final String TABLE = "financial";
	private static final String FAMILY = "f";// 1 or 2 bytes, not 4 that an int needs.
	private static final String TICKER = "t";
	private static final String TYPE = "p";
	private static final String ANNUAL = "a";

	@Autowired
	public FinancialAccumuloDAO(Connector connector) {
		super(connector);
	}

	@Override
	public String getTable() {
		return TABLE;
	}

	@Override
	public String getFamilyForCount() {
		return FAMILY;
	}

	@Override
	public String getQualifierForCount() {
		return TICKER;
	}

	@Override
	public Map<ByteArray, Financial> toEntities(Iterable<Entry<Key, Value>> scan)
			throws DataException {
		Map<String, FinancialBuilder> builders = new HashMap<>();
		for (Entry<Key, Value> entry : scan) {
			if (entry.getKey().getColumnFamily().getBytes().length!=Integer.BYTES) {
				// this is for the FAMILY stuff, not data I need to use.
				continue;
			}
			String row = entry.getKey().getRow().toString();
			String ticker = FinancialTable.determineTicker(row);
			StatementType type = FinancialTable.determineType(row);
			boolean annual = FinancialTable.determineAnnual(row);
			int label = ByteUtils.bytesToInt(entry.getKey().getColumnFamily().getBytes());
			byte[] date = entry.getKey().getColumnQualifier().getBytes();
			double value = ByteUtils.bytesToDouble(entry.getValue().get());
			FinancialBuilder builder = builders.get(ticker);
			if (builder == null) {
				builder = new FinancialBuilder();
				builder.setTicker(ticker);
				builders.put(ticker,builder);
			}
			builder.add(type, annual, date, label, value);
		}

		Map<ByteArray, Financial> financials = new HashMap<>();
		for (String ticker : builders.keySet()) {
			ByteArray id = new ByteArray(ticker);
			Financial financial = builders.get(ticker).build();
			financial.setId(id);
			financial.setTicker(ticker);
			financials.put(id, financial);
		}
		return financials;
	}

	@Override
	public Iterable<Mutation> toMutations(Financial financial) {
		return new FinancialMutationIterable(financial);
	}

	private static class FinancialMutationIterable implements
			Iterable<Mutation> {
		private final Financial financial;

		public FinancialMutationIterable(final Financial financial) {
			this.financial = financial;
		}

		@Override
		public Iterator<Mutation> iterator() {
			return new FinancialMutationIterator(financial);
		}
	}

	private static class FinancialMutationIterator implements
			Iterator<Mutation> {
		private final List<FinancialTable> financialTables;

		private int i = 0;

		public FinancialMutationIterator(final Financial financial) {
			this.financialTables = financial.tablesList();
		}

		@Override
		public boolean hasNext() {
			return i < financialTables.size();
		}

		@Override
		public Mutation next() {
			if (i>=financialTables.size()) {
				return null;
			}
			FinancialTable table = financialTables.get(i++);
			Mutation mutation = new Mutation(table.calculateId());
			for (DoubleTableCell cell : table) {
				mutation.put(ByteUtils.intToBytes(cell.getLabel()),
						cell.getDate(),
						ByteUtils.doubleToBytes(cell.getValue()));
			}
			mutation.put(FAMILY.getBytes(), TICKER.getBytes(), table.getTicker().getBytes());
			mutation.put(FAMILY.getBytes(), TYPE.getBytes(), table.getType().name().getBytes());
			mutation.put(FAMILY.getBytes(), ANNUAL.getBytes(), ByteUtils.boolToBytes(table.isAnnual()));
			return mutation;
		}
	}
}