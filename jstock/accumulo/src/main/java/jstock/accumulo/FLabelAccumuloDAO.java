package jstock.accumulo;

import static knapp.common.accumulo.dao.AccUtil.put;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import jstock.dao.FinancialLabelDAO;
import jstock.domain.fh.FinancialLabel;
import knapp.common.accumulo.dao.MutationMapping;
import knapp.common.accumulo.dao.SingleRowAccumuloDAO;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;
import knapp.common.util.ByteUtils;

import org.apache.accumulo.core.client.BatchDeleter;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component("financialLabelDAO")
@Profile("accumulo")
public class FLabelAccumuloDAO extends SingleRowAccumuloDAO<FinancialLabel>
		implements FinancialLabelDAO {
	public static String TABLE = "financialLabel";
	public static String FAMILY = "f";
	public static String ORDINAL = "o";
	public static String NAME = "n";

	@Autowired
	public FLabelAccumuloDAO(Connector connector) {
		super(connector);
	}

	@Override
	public String getFamilyForCount() {
		return FAMILY;
	}

	@Override
	public String getQualifierForCount() {
		return NAME;
	}

	@Override
	public String getTable() {
		return TABLE;
	}

//	@Override
//	public Iterable<Mutation> toMutations(FinancialLabel entity) {
//		// pad the number with zeros so that lexical ordering is preserved
//		Mutation m = new Mutation(String.format("%04d", entity.getOrdinal()));
//		put(m, FAMILY, NAME, entity.getName());
//		put(m, FAMILY, ORDINAL, entity.getOrdinal());
//		// I keep a duplicate copy based on the name index,
//		// which is a hashed, truncated, and base 64d value derived
//		// from the nam.e
//		Mutation m2 = new Mutation(getNameIndex(entity));
//		put(m2, FAMILY, NAME, entity.getName());
//		put(m2, FAMILY, ORDINAL, entity.getOrdinal());
//		return Arrays.asList(m, m2);
//	}

	protected List<Mutation> customMutations(FinancialLabel entity,
			Mutation currentMutation) {
		// we have a copy of the entity under its name index
		// so it's easy to look it up by name too.
		Mutation m2 = new Mutation(getNameIndex(entity));
		put(m2, FAMILY, NAME, entity.getName());
		put(m2, FAMILY, ORDINAL, entity.getOrdinal());
		return Collections.singletonList(m2);
	}

	@Override
	public void delete(Collection<ByteArray> ids, boolean exact,
			Consumer<BatchDeleter> deleterConsumer) throws DataException {
		// before deleting these, be sure to also delete the indexes for them.
		Map<ByteArray, FinancialLabel> labels = get(ids);
		Set<Range> ranges = labels.values().stream()
				.map(l -> new Range(getNameIndex(l)))
				.collect(Collectors.toSet());
		
		// I delete the indexes.
		super.deleteRanges(ranges);
		// now I delete the real entity.
		super.delete(ids, exact, deleterConsumer);
	}

	public String getNameIndex(FinancialLabel entity) {
		return getNameIndex(entity.getName());
	}

	public String getNameIndex(String entityName) {
		return ByteUtils.toIndex4(entityName);
	}
//
//	@Override
//	public Map<ByteArray, FinancialLabel> toEntities(
//			Iterable<Entry<Key, Value>> scan) throws DataException {
//		Map<ByteArray, FinancialLabel> results = new HashMap<>();
//		for (Entry<Key, Value> entry : scan) {
//			int ordinal = Integer.valueOf(entry.getKey().getRow().toString());
//			String name = new String(entry.getValue().get());
//			results.put(new ByteArray(ByteUtils.intToBytes(ordinal)),
//					new FinancialLabel(name, ordinal));
//		}
//		return results;
//	}

	@Override
	public Map<Integer, String> loadLabels() throws DataException {
		Map<ByteArray, FinancialLabel> tmp = super.loadRanges(
				Collections.singleton(new Range()), null);
		Map<Integer, String> labels = new HashMap<>();
		tmp.values().forEach(l -> labels.put(l.getOrdinal(), l.getName()));
		return labels;
	}

	@Override
	public Integer getOrdinalForName(String name) throws DataException {
		FinancialLabel label = super.get(new ByteArray(getNameIndex(name)));
		return label == null ? null : label.getOrdinal();
	}

	@Override
	public String getNameForOrdinal(Integer ordinal) throws DataException {
		FinancialLabel label = super.get(new ByteArray(String.valueOf(ordinal)));
		return label == null ? null : label.getName();
	}

	@Override
	public Collection<MutationMapping<FinancialLabel>> getMutationMappings() {
		MutationMapping<FinancialLabel> m1 = new MutationMapping<FinancialLabel>(
				FAMILY, ORDINAL, "ordinal").entityType(FinancialLabel.class)
				.finish();
		MutationMapping<FinancialLabel> m2 = new MutationMapping<FinancialLabel>(
				FAMILY, NAME, "name").entityType(FinancialLabel.class).finish();
		return Arrays.asList(m1, m2);
	}

	@Override
	public Class<FinancialLabel> getEntityClass() {
		return FinancialLabel.class;
	}
}