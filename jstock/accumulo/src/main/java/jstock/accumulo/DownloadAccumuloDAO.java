package jstock.accumulo;

import static knapp.common.accumulo.dao.AccUtil.put;
import static knapp.common.accumulo.dao.AccUtil.valueToInt;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import jstock.dao.DownloadDAO;
import jstock.domain.Download;
import jstock.domain.DownloadRecordQuery;
import knapp.common.accumulo.dao.BaseAccumuloDAO;
import knapp.common.accumulo.dao.MutationMapping;
import knapp.common.accumulo.dao.MutationMapping.MutationMappingBuilder;
import knapp.common.accumulo.dao.SingleRowAccumuloDAO;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;
import knapp.common.util.ByteUtils;
import knapp.common.util.TimeSpan;
import knapp.open.iterators.CountIterator;
import knapp.open.iterators.NthRowFilter;
import knapp.open.iterators.RowValuesFilter;

import org.apache.accumulo.core.client.BatchScanner;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.client.TableNotFoundException;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.hadoop.io.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component("downloadDAO")
@Profile("accumulo")
public class DownloadAccumuloDAO extends SingleRowAccumuloDAO<Download>
		implements DownloadDAO {

	public static final String TABLE = "download";
	private static final String FAMILY = "f";
	private static final String URL = "u";
	private static final String URL_HOST_PORTION = "u.h";
	private static final String URL_PATH_PORTION = "u.p";
	private static final String URL_QUERY_PORTION = "u.q";
	private static final String FOREIGN_ENTITY_TABLE = "ft";
	private static final String FOREIGN_ENTITY_ID = "fi";
	private static final String WAS_SUCCESSFUL = "ws";
	private static final String ATTEMPTED_ON = "d";
	private static final String RESPONSE_CODE = "c";
	private static final String RESPONSE_MESSAGE = "m";
	private static final String SYSTEM = "s";
	private static final String QUERY_TIME_SPAN = "qts";
	private static final String DATA_TIME_SPAN = "dts";
	private static final String SAVE_FILE_PATH = "p";
	private static final String MD5 = "h";
	private static final String APP_VERSION = "v";
	private static final String SERVICE_DENIED = "sd";

	@Override
	public Collection<MutationMapping<Download>> getMutationMappings() {
		return new MutationMappingBuilder<Download>(Download.class,FAMILY)
				.add(URL,"uri")
				.add(FOREIGN_ENTITY_TABLE,"foreignTable")
				.add(FOREIGN_ENTITY_ID,"foreignId")
				.add(WAS_SUCCESSFUL,"wasSuccessful")
				.add(ATTEMPTED_ON,"attemptedOn")
				.add(RESPONSE_CODE,"responseCode")
				.add(RESPONSE_MESSAGE,"responseMessage")
				.add(SYSTEM,"system")
				.add(QUERY_TIME_SPAN,"queryTimeSpan")
				.add(DATA_TIME_SPAN,"dataTimeSpan")
				.add(SAVE_FILE_PATH,"saveFilePath")
				.add(MD5,"md5")
				.add(APP_VERSION,"appVersion")
				.add(SERVICE_DENIED,"serviceDenied")
				.getList();
	}

	@Override
	protected List<Mutation> customMutations(Download entity,Mutation currentMutation) {
		put(currentMutation,FAMILY,URL_HOST_PORTION,entity.getUri().getHost());
		put(currentMutation,FAMILY,URL_PATH_PORTION,entity.getUri().getHost());
		put(currentMutation,FAMILY,URL_QUERY_PORTION,entity.getUri().getHost());
		// don't need to return the list, since we just modified the current mutation.
		return null;
	}

	@Override
	protected void customToEntity(ByteArray ticker, Download entity, Entry<Key, Value> kv) {
		// here for children to do any extra processing.
	}

	@Autowired
	public DownloadAccumuloDAO(Connector connector) {
		super(connector);
	}

	@Override
	public void create(Download record) throws DataException {
		save(record);
	}

	@Override
	public void create(List<Download> records) throws DataException {
		save(records);
	}

	@Override
	public void update(Download record) throws DataException {
		save(record);
	}

	@Override
	public void delete(Download record) throws DataException {
		super.delete(record.getId());
	}

	@Override
	public void update(List<Download> records) throws DataException {
		save(records);
	}

	@Override
	public void delete(List<Download> records) throws DataException {
		super.delete(records.stream().map(r -> r.getId())
				.collect(Collectors.toSet()));
	}

	@Override
	public String getTable() {
		return TABLE;
	}

//	@Override
//	public Iterable<Mutation> toMutations(Download entity) {
//		// we only really have one mutation per entity.
//		Mutation m = new Mutation(entity.getId());
//		if (entity.getAttemptedOn() != null) {
//			m.put(FAMILY, ATTEMPTED_ON,
//					calendarToValue(entity.getAttemptedOn()));
//		}
//		if (entity.getUri() != null) {
//			m.put(FAMILY, URL, entity.getUri().toString());
//			m.put(FAMILY, URL_HOST_PORTION, entity.getUri().getHost()
//					.toString());
//			if (entity.getUri().getPath()!=null) {
//				m.put(FAMILY, URL_PATH_PORTION, entity.getUri().getPath()
//						.toString());
//			}
//			if (entity.getUri().getQuery()!=null) {
//				m.put(FAMILY, URL_QUERY_PORTION, entity.getUri().getQuery()
//						.toString());
//			}
//		}
//		if (entity.getForeignTable() != null) {
//			m.put(FAMILY, FOREIGN_ENTITY_TABLE, entity.getForeignTable());
//		}
//		if (entity.getForeignId() != null) {
//			m.put(FAMILY, FOREIGN_ENTITY_ID, entity.getForeignId());
//		}
//		entity.updateSuccess();
//		m.put(FAMILY, WAS_SUCCESSFUL,boolToValue(entity.wasSuccessful()));
//		m.put(FAMILY, RESPONSE_CODE, intToValue(entity.getResponseCode()));
//		if (entity.getResponseMessage() != null) {
//			m.put(FAMILY, RESPONSE_MESSAGE, entity.getResponseMessage());
//		}
//		if (entity.getSystem() != null) {
//			m.put(FAMILY, SYSTEM, entity.getSystem());
//		}
//		if (entity.getQueryTimeSpan() != null) {
//			m.put(FAMILY, QUERY_TIME_SPAN,
//					timeSpanToValue(entity.getQueryTimeSpan()));
//		}
//		if (entity.getDataTimeSpan() != null) {
//			m.put(FAMILY, DATA_TIME_SPAN,
//					timeSpanToValue(entity.getDataTimeSpan()));
//		}
//		if (entity.getSaveFilePath() != null) {
//			m.put(FAMILY, SAVE_FILE_PATH, entity.getSaveFilePath());
//		}
//		if (entity.getMd5() != null) {
//			m.put(FAMILY, MD5, entity.getMd5());
//		}
//		if (entity.getAppVersion() != null) {
//			m.put(FAMILY, APP_VERSION, entity.getAppVersion());
//		}
//		m.put(FAMILY, SERVICE_DENIED, boolToValue(entity.isServiceDenied()));
//		return Collections.singleton(m);
//	}

	public static Value timeSpanToValue(TimeSpan ts) {
		byte[] b1 = ByteUtils.dateToBytes(ts.getStart());
		byte[] b2 = ByteUtils.dateToBytes(ts.getEnd());
		byte[] b = new byte[b1.length + b2.length];
		System.arraycopy(b1, 0, b, 0, b1.length);
		System.arraycopy(b2, 0, b, b1.length, b2.length);
		return new Value(b);
	}

	public static TimeSpan valueToTimeSpan(Value v) {
		byte[] b = v.get();
		byte[] b1 = new byte[b.length / 2];
		byte[] b2 = new byte[b.length / 2];
		System.arraycopy(b, 0, b1, 0, b1.length);
		System.arraycopy(b, b1.length, b2, 0, b2.length);
		LocalDate start = ByteUtils.bytesToDate(b1);
		LocalDate end = ByteUtils.bytesToDate(b2);
		return new TimeSpan(start, end);
	}

//	@Override
//	public Map<ByteArray, Download> toEntities(Iterable<Entry<Key, Value>> scan)
//			throws DataException {
//		Map<ByteArray, Download> downloads = new HashMap<>();
//		ByteArray rowId = null;
//
//		for (Entry<Key, Value> keyValue : scan) {
//			rowId = new ByteArray(keyValue.getKey().getRow().getBytes());
//
//			Download download = downloads.get(rowId);
//			if (download == null) {
//				download = new Download();
//				downloads.put(rowId, download);
//			}
//			if (isCQ(keyValue, ATTEMPTED_ON)) {
//				download.setAttemptedOn(valueToCalendar(keyValue.getValue()));
//			}
//			if (isCQ(keyValue, URL)) {
//				download.setUriString(new String(keyValue.getValue().get()));
//			}
//			if (isCQ(keyValue, RESPONSE_CODE)) {
//				download.setResponseCode(valueToInt(keyValue.getValue()));
//			}
//			if (isCQ(keyValue, RESPONSE_MESSAGE)) {
//				download.setResponseMessage(new String(keyValue.getValue()
//						.get()));
//			}
//			if (isCQ(keyValue, SYSTEM)) {
//				download.setSystem(new String(keyValue.getValue().get()));
//			}
//			if (isCQ(keyValue, QUERY_TIME_SPAN)) {
//				download.setQueryTimeSpan(valueToTimeSpan(keyValue.getValue()));
//			}
//			if (isCQ(keyValue, DATA_TIME_SPAN)) {
//				download.setDataTimeSpan(valueToTimeSpan(keyValue.getValue()));
//			}
//			if (isCQ(keyValue, SAVE_FILE_PATH)) {
//				download.setSaveFilePath(new String(keyValue.getValue().get()));
//			}
//			if (isCQ(keyValue, MD5)) {
//				download.setMd5(new String(keyValue.getValue().get()));
//			}
//			if (isCQ(keyValue, APP_VERSION)) {
//				download.setAppVersion(String
//						.valueOf(keyValue.getValue().get()));
//			}
//			if (isCQ(keyValue, SERVICE_DENIED)) {
//				download.setServiceDenied(valueToBool(keyValue.getValue()));
//			}
//			if (isCQ(keyValue, FOREIGN_ENTITY_ID)) {
//				download.setForeignId(new ByteArray(keyValue.getValue().get()));
//			}
//			if (isCQ(keyValue, FOREIGN_ENTITY_TABLE)) {
//				download.setForeignTable(new String(keyValue.getValue().get()));
//			}
//			if (isCQ(keyValue, WAS_SUCCESSFUL)) {
//				download.setWasSuccessful(ByteUtils.bytesToBool(keyValue.getValue().get()));
//			}
//		}
//		return downloads;
//	}

	@Override
	public int count() throws DataException {
		BatchScanner scan = null;
		int count = 0;
		try {
			scan = getConnector()
					.createBatchScanner(getTable(), BaseAccumuloDAO.PUBLIC_AUTHORIZATIONS, THREADS);
			// I tell it to only fetch the url, so only one cell per row
			// is iterated over.
			scan.fetchColumn(new Text(FAMILY), new Text(URL));

			// I use the counting iterator so it just returns the count
			IteratorSetting s = new IteratorSetting(11, CountIterator.class);
			scan.addScanIterator(s);

			count = valueToInt(scan.iterator().next().getValue());
		} catch (TableNotFoundException e) {
			wrapException(null, getTable(), null, e);
		} finally {
			if (scan != null) {
				scan.close();
			}
		}
		return count;
	}

	@Override
	public Download getLatest(String uri) throws DataException {
		return getLatest(uri, false);
	}

	public Download getLatest(String uri, boolean mustBeSuccessful)
			throws DataException {
		// TODO you will have to form a range, do a range based query,
		// and then pick the download out of the results returned.
		String id = Download.determineIdForUri(uri);
		// need to span all of the timestamps.
		// it is not supposed to be an exact range.
		Range range = Range.prefix(id);
		Consumer<BatchScanner> consumer = (scan) -> {
			if (mustBeSuccessful) {
				// in this case, we want to filter out rows
				// that were not successful.

				Map<String, String> options = new HashMap<>();
				// this tells it to pick rows where the service was not denied.
				options.put(String.format("req:%s:%s:boolean", FAMILY,
						SERVICE_DENIED), "false");
				IteratorSetting iteratorSetting = new IteratorSetting(10,
						RowValuesFilter.class, options);
				scan.addScanIterator(iteratorSetting);
			}
			// this tells it to return only the first row:

			// we want it to just return the first result.
			// since the results are ordered by reverse time,
			IteratorSetting iteratorSetting = new IteratorSetting(11,
					NthRowFilter.class);
			scan.addScanIterator(iteratorSetting);
		};

		// since we used the iterators above, there should only be one result,
		// the one we wanted:
		return this.getOneByRange(range, consumer);
	}

	@Override
	public Download getLatestSuccess(String uri) throws DataException {
		return getLatest(uri, true);
	}

	@Override
	public List<Download> get(DownloadRecordQuery query) throws DataException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int count(DownloadRecordQuery query) throws DataException {
		return get(query).size();
	}

	@Override
	public String getFamilyForCount() {
		return FAMILY;
	}

	@Override
	public String getQualifierForCount() {
		return URL;
	}

	@Override
	public Download getLatestByForeignId(String host,String path,String foreignTable,
			ByteArray foreignId) throws DataException {
		return getLatestByForeignId(host, path, foreignTable, foreignId,false);
	}

	public Download getLatestByForeignId(String host,String path,String foreignTable,
			ByteArray foreignId,boolean mustBeSuccessful) throws DataException {
		Scanner scanner = null;
		Download download = null;
		try {
			scanner = getConnector().createScanner(getNameSpaceAndTable(), BaseAccumuloDAO.PUBLIC_AUTHORIZATIONS);
			String prefix = Download.determineIdPrefix(host, path);
			Range range = Range.prefix(prefix);
			scanner.setRange(range);
			// use all columns.
			
			// need two iterators to filter it to just the latest
			Map<String,String> options = new HashMap<>();
			options.put("req."+FAMILY+"."+FOREIGN_ENTITY_TABLE,foreignTable);
			options.put("req."+FAMILY+"."+FOREIGN_ENTITY_ID,foreignId.toString());
			IteratorSetting filter = new IteratorSetting(10,RowValuesFilter.class,options);
			
			// the Nth row filter returns the first row only if you provide it no arguments.
			// since the first row for a download prefix is the latest, that is what we want.
			// because this filter is outside of the row values filter.
			IteratorSetting latestFilter = new IteratorSetting(12,NthRowFilter.class);
			
			scanner.addScanIterator(filter);
			scanner.addScanIterator(latestFilter);
			if (mustBeSuccessful) {
				// this restricts it to rows where the download was successful.
				Map<String,String> successOptions = new HashMap<>();
				successOptions.put("req."+FAMILY+"."+WAS_SUCCESSFUL,"true");
				IteratorSetting successFilter = new IteratorSetting(11,RowValuesFilter.class,options);
				scanner.addScanIterator(successFilter);
			}
			
			// now scan it.
			Map<ByteArray,Download> downloads = toEntities(scanner);
			if (downloads!=null && !downloads.isEmpty()) {
				download = downloads.values().iterator().next(); 
			}
		} catch (Exception e) {
			wrapException("getting latest by foreign id", getNameSpaceAndTable(), null, e);
		} finally {
			scanner.close();
		}
		
		return download;
	}

	@Override
	public Download getLatestSuccessByForeignId(String host,String path,String foreignTable,
			ByteArray foreignId) throws DataException {
		return getLatestByForeignId(host, path, foreignTable, foreignId,true);
	}

	@Override
	public Class<Download> getEntityClass() {
		return Download.class;
	}
}