package jstock.accumulo;

import knapp.common.data.dao.DAO;
import knapp.common.data.dao.DAOTestBase;
import knapp.common.data.domain.Entity;

import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.mock.MockConnector;
import org.apache.accumulo.core.client.mock.MockInstance;
import org.apache.accumulo.core.client.security.tokens.AuthenticationToken;
import org.apache.accumulo.core.client.security.tokens.PasswordToken;

public abstract class AccumuloDAOTestBase<T extends Entity> extends DAOTestBase<T>{

	private DAO<T> dao;
	
	@Override
	public DAO<T> getDAO() {
		if (dao == null) {
			try {
				MockInstance instance = new MockInstance();
				AuthenticationToken token = new PasswordToken();
				MockConnector connector = (MockConnector) instance.getConnector("root", token);
				connector.namespaceOperations().create("test_jstock");
				connector.tableOperations().create("test_jstock."+getTableName());
				dao = createDAO(connector);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return dao;
	}

	public abstract DAO<T> createDAO(Connector connector);

	public abstract String getTableName();
}