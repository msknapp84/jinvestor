package jstock.accumulo;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import jstock.domain.ph.PHTablet;
import jstock.parser.ph.PHCSVParser;
import knapp.common.data.dao.DAO;

import org.apache.accumulo.core.client.Connector;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

//import org.junit.runner.RunWith;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import jstock.accumulo.di.JStockAccumuloContext;
//@RunWith(value=SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes={JStockAccumuloContext.class})
public class PHAccumuloDAOTest extends AccumuloDAOTestBase<PHTablet> {

//	@Test
//	public void checkArr() {
//		Byte[] b = new Byte[]{3,9};
//		Byte[] b2 = new Byte[]{3,9};
//		System.out.println(b.equals(b2));
//		System.out.println(b==b2);
//		System.out.println(b.hashCode()==b2.hashCode());
//	}
	
	@Override
	public List<PHTablet> getEntities() {
		// let's actually use our parser and some csv files
		// of real price histories.
		PHCSVParser parser = new PHCSVParser();
		InputStream in = PHAccumuloDAOTest.class.getResourceAsStream("/jstock/accumulo/ge_prices.csv");
		PHTablet ge = parser.apply("GE", in);
		IOUtils.closeQuietly(in);
		
		in = PHAccumuloDAOTest.class.getResourceAsStream("/jstock/accumulo/goog_prices.csv");
		PHTablet goog = parser.apply("GOOG", in);
		IOUtils.closeQuietly(in);
		
		in = PHAccumuloDAOTest.class.getResourceAsStream("/jstock/accumulo/amzn_prices.csv");
		PHTablet amzn = parser.apply("AMZN", in);
		IOUtils.closeQuietly(in);
		
		in = PHAccumuloDAOTest.class.getResourceAsStream("/jstock/accumulo/lmt_prices.csv");
		PHTablet lmt = parser.apply("LMT", in);
		IOUtils.closeQuietly(in);

		return Arrays.asList(ge,goog,amzn,lmt);
	}

	@Override
	public DAO<PHTablet> createDAO(Connector connector) {
		return new PHAccumuloDAO(connector);
	}

	@Override
	public String getTableName() {
		return PHAccumuloDAO.TABLE;
	}
}