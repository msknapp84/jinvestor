package jstock.accumulo;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import jstock.dao.DownloadDAO;
import jstock.domain.Download;
import jstock.domain.DownloadMutable;
import knapp.common.data.dao.DAO;
import knapp.common.data.exceptions.DataException;

import org.apache.accumulo.core.client.Connector;
import org.junit.Assert;
import org.junit.Test;

public class DownloadAccumuloDAOTest extends AccumuloDAOTestBase<Download> {

	@Override
	public DownloadDAO createDAO(Connector connector) {
		return new DownloadAccumuloDAO(connector);
	}

	@Override
	public String getTableName() {
		return DownloadAccumuloDAO.TABLE;
	}

	@Override
	public List<Download> getEntities() {
		Download dl = new DownloadMutable().appVersion(1).responseCode(200).saveFilePath("/foo/bar/zop")
				.system("yahoo").uriString("http://yahoo.com/whatever").toDownload();
		Download dl2 = new DownloadMutable().appVersion(3).responseCode(400).saveFilePath("/foo/lap/kola").serviceDenied(true)
				.system("google").uriString("http://google.com/meh").on("2010-07-21T08:14:32").toDownload();
		Download dl3 = new DownloadMutable().appVersion(3).responseCode(400).saveFilePath("/borg/picard")
				.system("star_trek").uriString("http://startrek.com/meh").toDownload();
		Download dl4 = new DownloadMutable().appVersion(2).responseCode(304).saveFilePath("/jedi/luke")
				.system("death_star").uriString("http://deathstar.com/meh").toDownload();
		Download dl5 = new DownloadMutable().appVersion(3).responseCode(400).saveFilePath("/foo/lap/kola").serviceDenied(true)
				.system("google").uriString("http://google.com/meh").on("2010-05-21T08:14:32").toDownload();
		Download dl6 = new DownloadMutable().appVersion(3).responseCode(200).serviceDenied(false).saveFilePath("/foo/lap/kola")
				.system("google").uriString("http://google.com/meh").on("2010-03-21T08:14:32").toDownload();
		return Arrays.asList(dl,dl2,dl3,dl4,dl5,dl6);
	}
	
	public DownloadDAO getDAO() {
		return (DownloadDAO)super.getDAO();
	}
	
	@Test
	public void latest() throws DataException {
		List<Download> entities = getEntities();
		getDAO().save(entities);
		Download d = getDAO().getLatest("http://google.com/meh");
		// month is zero based.
		Assert.assertEquals(6,d.getAttemptedOn().get(Calendar.MONTH));
		d = getDAO().getLatestSuccess("http://google.com/meh");
		Assert.assertEquals(2,d.getAttemptedOn().get(Calendar.MONTH));
		getDAO().delete(entities);
	}
}